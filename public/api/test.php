<?php
    $starttime = microtime (true);
    
    include "../../vendor/autoload.php";
    
    /** @var \Dotenv\Dotenv $defaultValues Loads the default values for the configuration */
    $defaultValues = new \Dotenv\Dotenv (realpath ("../../"), ".env.default");
    $defaultValues->load ();
    
    /** @var \Dotenv\Dotenv $overridenValues Loads the overriden values for the configuration */
    $overridenValues = new \Dotenv\Dotenv (realpath ("../../"), ".env");
    $overridenValues->overload ();
    
    $resolver = new \Atatiki\API\Controllers\Resolver ();
    $request = $resolver->resolve ($_SERVER ['PATH_INFO']);

    $request->getController ()->handle ($request)->printOutput ();
    
    $endtime = microtime (true);
    
    \Linkz\Logging\LoggerFactory::getPerformanceLogger ()->debug (
        ($_SERVER ['REQUEST_METHOD'] ?? '') . ' ' . ($_SERVER ['REQUEST_URI'] ?? '') . '?' . ($_SERVER ['QUERY_STRING'] ?? '') . ' took ' . floor ((($endtime - $starttime) * 1000)) . ' ms'
    );
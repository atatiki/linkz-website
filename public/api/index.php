<?php
    $starttime = microtime (true);
    
    include "../../vendor/autoload.php";
    
    $resolver = new \Atatiki\API\Controllers\Resolver ();
    $request = $resolver->resolve ($_SERVER ['PATH_INFO']);

    $request->getController ()->handle ($request)->printOutput ();
    
    $endtime = microtime (true);
    
    \Linkz\Logging\LoggerFactory::getPerformanceLogger ()->debug (
        ($_SERVER ['REQUEST_METHOD'] ?? '') . ' ' . ($_SERVER ['REQUEST_URI'] ?? '') . '?' . ($_SERVER ['QUERY_STRING'] ?? '') . ' took ' . floor ((($endtime - $starttime) * 1000)) . ' ms'
    );
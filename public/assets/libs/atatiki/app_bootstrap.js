'use strict';

/**
 * Small loader library for javascript code that ensures the files loaded are up to date
 * @author Alexis Maiquez Murcia <alexis.murcia@atatiki.es, almamu@almamu.com>
 */

(function ()
{
    /**
     * Performs synchronized file loading based on the modulemap and loadlist downloaded from the server
     *
     * @param {[]} loadlist The list of modules to load in order
     * @param {{}} modulesMap The module map to use as base for timestamps
     * @param {int} position The position where to start loading scripts
     * @param {boolean} iterate Whether to load the full array or only the specified element
     */
    function synchronizedScriptLoading (loadlist, modulesMap, position, iterate = true)
    {
        if (loadlist.length <= position || loadlist.hasOwnProperty (position) === false)
        {
            return; // console.log ("Modules load completed successful");
        }

        /** @type {string} The MD5 hash of the file's path */
        var md5val = md5 (loadlist [position]);
        var element;

        // md5 found, construct the script tag and add it to the head
        if (md5val in modulesMap)
        {
            if (loadlist [position].indexOf ('.css') === (loadlist [position].length - 4))
            {
                element = document.createElement ("link");
                element.href = window.location.protocol + "//" + window.location.host + "/" + loadlist [position] + "?" + modulesMap [md5val];
                element.type = "text/css";
                element.rel = "stylesheet";
            }
            else if (loadlist [position].indexOf ('.js') === (loadlist [position].length - 3))
            {
                element = document.createElement ("script");
                element.src = window.location.protocol + "//" + window.location.host + "/" + loadlist [position] + "?" + modulesMap [md5val];
                element.type = "text/javascript";
            }
        }
        else
        {
            if ("compiled" in modulesMap == false || modulesMap ['compiled'] == false)
                console.warn ("Loading file not present in the modules map, this is potentially unsafe");

            if (loadlist [position].indexOf ('.css') === (loadlist [position].length - 4))
            {
                element = document.createElement ("link");
                element.href = window.location.protocol + "//" + window.location.host + "/" + loadlist [position] + "?" + Math.abs (Math.random ());
                element.type = "text/css";
                element.rel = "stylesheet";
            }
            else if (loadlist [position].indexOf ('.js') === (loadlist [position].length - 3))
            {
                element = document.createElement ("script");
                element.src = window.location.protocol + "//" + window.location.host + "/" + loadlist [position] + "?" + Math.abs (Math.random ());
                element.type = "text/javascript";
            }
        }

        console.log ("Loading module " + loadlist [position]);
        element.onload = function ()
        {
            synchronizedScriptLoading (loadlist, modulesMap, position + 1, true);
        };

        element.onerror = function (e)
        {
            console.log (e);

            throw new Error ("Cannot load file " + loadlist [position]);
        };

        document.getElementsByTagName ("head") [0].appendChild (element);
    }

    /** @type {string} The url to load the autoload from */
    var autoloadUrl = window.location.protocol + "//" + window.location.host + "/app/config/autoload.json?" + Math.abs (Math.random ());
    /** @type {string} The url to load the loadlist from */
    var loadlistUrl = window.location.protocol + "//" + window.location.host + "/app/config/loadlist.json?" + Math.abs (Math.random ());
    /** @type {string} The url to load the compiled css from */
    var compiledCss = "app/config/compcss.css";
    /** @type {string} The url to load the compiled js from */
    var compiledJs = "app/config/compjs.js";
    /** @type {XMLHttpRequest} */
    var http = new XMLHttpRequest ();

    console.log ("Bootstrapping application...");
    console.log ("Downloading modules map information");

    http.open ('GET', autoloadUrl, true);
    http.onreadystatechange = function ()
    {
        if (http.readyState === 4)
        {
            if (http.status !== 200)
            {
                throw new Error ("Cannot download autoload file: " + http.statusText);
            }

            console.log ("Modules map downloaded, downloading autoload information");

            /** @type {object} The module map with the associated, last modified timestamp */
            var modulesMap = JSON.parse (http.responseText);

            if ("compiled" in modulesMap && modulesMap ['compiled'] == true)
            {
                // load both compiled files
                synchronizedScriptLoading ([compiledCss, compiledJs], modulesMap, 0, true);
            }
            else
            {
                http = new XMLHttpRequest ();

                http.open ('GET', loadlistUrl, true);
                http.onreadystatechange = function ()
                {
                    if (http.readyState === 4)
                    {
                        if (http.status !== 200)
                        {
                            throw new Error ("Cannot download load list for system: " + http.statusText);
                        }

                        console.log ("Autoload information downloaded, importing modules");

                        synchronizedScriptLoading (JSON.parse (http.responseText), modulesMap, 0, true);
                    }
                };

                http.send (null);
            }
        }
    };

    http.send (null);
}) ();

'use strict';

angular.module('linkz',
[
    'ng',
    'ngRoute',
    'ngStorage',
    'ngSanitize',
    'lr.upload',
    'pascalprecht.translate',
    'atatiki',
    'linkz.services',
    'linkz.api',
    'linkz.authentication',
    'linkz.dashboard',
    'linkz.messaging',
    'linkz.billing',
    'linkz.account',
    'linkz.ui',
    'linkz.parent',
    'linkz.student',
    'linkz.tutor',
    'linkz.notifications'
])
.config (['$compileProvider', '$locationProvider', '$routeProvider', '$sceDelegateProvider', 'LoggerProvider',
function ($compileProvider, $locationProvider, $routeProvider, $sceDelegateProvider, LoggerProvider)
{
    // initialize the logging system
    LoggerProvider.enableDebug ();

    $locationProvider.html5Mode(true);
    $routeProvider
    .when ('/error/', {
        template: 'TEMPORAL ERROR PAGE!',
        controller: 'ErrorController'
    })
    .when ('/', {
        controller: 'StartController',
        template: '',
        base: 'login'
    })
    .when ('/notfound/', {
        controller: 'NotFoundController',
        templateUrl: 'app/modules/linkz/notfound.html',
        base: 'login'
    })
    .otherwise({
        redirectTo: '/notfound/'
    });

    $compileProvider.aHrefSanitizationWhitelist (/^\s*(https?|ftp|mailto|tel|file|skype):/);
    $sceDelegateProvider.resourceUrlWhitelist (['self', 'skype:']);
}])
.filter ('customCurrency', ['$filter', function ($filter)
{
    return function (input)
    {
        if (input.indexOf (',') !== false)
        {
            input = input.replace (',', '.');
        }

        return $filter ('currency') (input);
    };
}])
.controller ('ErrorController', function ()
{

})
.controller ('StartController', ['$location', function ($location)
{
    $location.path ('/login/');
}])
.controller ('NotFoundController', function ()
{

});

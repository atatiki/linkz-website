/*eslint-disable no-console*/
/*eslint angular/module-name: [2,"atatiki"]*/
'use strict';
/**
 * Due to how AngularJS apps behave and the phases these have, the main configuration file has to be loaded first.
 * This is to prevent the app from running before any settings are ready to be used.
 */
angular.element (document).ready (function ()
{
    console.log ('Loading application settings');

    $.ajax ('app/config/configuration.json')
    .done (function (configurationData)
    {
        angular.module ('atatiki.configuration', ['atatiki.loader'])
        .config (['ApplicationConfigurationProvider', function (ApplicationConfigurationProvider)
        {
            ApplicationConfigurationProvider.set (configurationData);
        }]);

        console.log ('Configuration loaded properly, bootstrapping AngularJS application...');
        angular.bootstrap (document, ['linkz']);
    })
    .fail (function ()
    {
        console.log ('Configuration failed loading, please try again later');
    });
});


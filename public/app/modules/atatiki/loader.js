/*eslint angular/module-name: [2,"atatiki"]*/
'use strict';

angular.module ('atatiki.loader', [])
.service ('ConfigurationLoader', ['$http', '$q', 'Logger', function ($http, $q, Logger)
{
    const logger = Logger.get('ConfigurationLoader');

    return {
        load: function (name)
        {
            logger.debug ('Loading configuration file app/config/' + name);

            return $http.get ('app/config/' + name).then (
                function (content)
                {
                    return content.data;
                },
                function (reason)
                {
                    logger.error ('Cannot load configuration file \'' + name + '\': ' + reason);
                    return $q.reject ();
                }
            );
        }
    };
}])
/**
 * Simple factory that allows for configuration to be stored and readed
 */
.provider ('ApplicationConfiguration', function ()
{
    return {
        data: {},
        get: function ()
        {
            return this.data;
        },
        set: function (data)
        {
            this.data = angular.extend ({}, data, this.data);
        },
        $get: function ()
        {
            return this;
        }
    };
})
/**
 * Directive that takes care of displaying a nice loader for a section of the page
 */
.directive ('loader', function ()
{
    return {
        templateUrl: 'app/modules/atatiki/loader.html',
        restrict: 'E',
        transclude: true,
        scope: {
            'loader': '=',
            'errorOnNotFound': '=?',
            'ignoreNotFound': '=?',
            'notFoundMsg': '=?',
            'size': '@size',
            'loadingMessage': '=?'
        },
        link: function (scope)
        {
            if (angular.isUndefined (scope.errorOnNotFound) === true)
            {
                scope.errorOnNotFound = false;
            }

            if (angular.isUndefined (scope.ignoreNotFound) === true)
            {
                scope.ignoreNotFound = false;
            }

            if (angular.isUndefined (scope.notFoundMsg) === true)
            {
                scope.notFoundMsg = '';
            }

            if (angular.isUndefined (scope.size) === true || scope.size !== 'small')
            {
                scope.size = 'big';
            }

            if (angular.isUndefined (scope.loadingMessage) === true)
            {
                scope.loadingMessage = 'Loading...';
            }

            scope.showLoader = false;
            scope.showNotFound = false;
            scope.showContent = false;
            scope.showError = false;

            scope.updateStatus = function ()
            {
                if (angular.isUndefined (scope.loader))
                {
                    scope.showLoader = true;
                    scope.showNotFound = false;
                    scope.showContent = false;
                    scope.showError = false;
                    return;
                }

                scope.showLoader = scope.loader.status == 'loading' || scope.loader.status == 'waiting';
                scope.showNotFound = scope.loader.status == 'notfound' && scope.errorOnNotFound == false && scope.ignoreNotFound == false;
                scope.showContent = scope.loader.status == 'ok' || (scope.loader.status == 'notfound' && scope.ignoreNotFound == true);
                scope.showError = scope.loader.status == 'error' || (scope.loader.status == 'notfound' && scope.errorOnNotFound == true);
            };

            scope.$watch ('loader.status', function ()
            {
                scope.updateStatus ();
            });

            // fix for any tablist in the system
            angular.element ('[role="tablist"] > a[role="tab"]').off ('click').on ('click', function (e)
            {
                e.preventDefault ();
                angular.element (this).tab ('show');
            });
        }
    };
})
/**
 * Loader system designed to provide information to {@see: atatikiLoader}
 */
.service ('Loader', function ()
{
    return function (defaultData = [], maximum = 1)
    {
        return {
            /** @type int The maximum number of request to mark this loader as done */
            max: maximum,
            /** @type * The data the loader will be using */
            data: defaultData,
            /** @type int The number of failed requests */
            failed: 0,
            /** @type int The number of completed requests */
            completed: 0,
            /** @type int The number of queued requests */
            queued: 0,
            /** @type string Indicator of the loader status */
            status: 'waiting',
            /** @type function A callback for when the loader finishes loading the data */
            resolveCallback: function () {},
            /**
             * Queues up a promise to be handled by the loader, allowing for precise control
             * over the real status of the task
             *
             * @param {*|Promise|PromiseLike} promise The promise to queue
             */
            queue: function (promise)
            {
                var self = this;

                // first force the status to reflect the loading state
                this.updateStatus ();

                var queued = this.queued ++;

                promise.then (
                    function (data) { self.promiseResolved (data, queued); },
                    function () { self.promiseRejected (); }
                );
            },
            /**
             * Callback for when a promise is resolved
             *
             * @param {*} data The data the resolved promise sent
             */
            promiseResolved: function (data, order = 0)
            {
                // store loaded data
                if (this.max == 1)
                {
                    this.data = data;
                    this.completed = 1;
                }
                else
                {
                    this.data [order] = data;
                    this.completed ++;
                }

                if (angular.isFunction (this.resolveCallback))
                {
                    this.resolveCallback ();
                }

                this.updateStatus ();
            },
            /**
             * Callback for when a promise is rejected
             */
            promiseRejected: function ()
            {
                if (this.max == 1)
                {
                    this.failed = 1;
                }
                else
                {
                    this.failed ++;
                }

                this.updateStatus ();
            },
            /**
             * Keeps updated the status member with the correct values
             */
            updateStatus: function ()
            {
                if (this.completed == this.max)
                {
                    this.status = 'ok';
                }
                else if (this.failed > 0)
                {
                    if ( (this.completed + this.failed) == this.max)
                    {
                        this.status = 'error';
                    }
                    else
                    {
                        this.status = 'loading';
                    }
                }
                else
                {
                    this.status = 'loading';
                }
            },
            setResolveCallback: function (callback)
            {
                this.resolveCallback = callback;
            },
            isFinished: function ()
            {
                return this.failed + this.completed === this.max;
            },
            reset: function ()
            {
                this.failed = 0;
                this.completed = 0;
                this.queued = 0;
                this.updateStatus ();
            }
        };
    };
});

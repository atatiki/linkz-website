/*eslint angular/module-name: [2,"atatiki"]*/
'use strict';

angular.module ('atatiki.templating', [])
.constant ('ATATIKI_NGVIEW_TEMPLATE', 'atatikiNgView.html')
.run (['Templating', function (Templating)
{
    Templating.init ();
}])
.provider ('Templating', ['LoggerProvider', function (LoggerProvider)
{
    const logger = LoggerProvider.get ('TemplatingSystem');
    var templates = {};

    /**
     * Constructor for the templates object
     *
     * @param {string} name The template's name (id)
     * @param {string} url The template file's URL
     * @return {{name: *, url: *, childs: *, parent: *}}
     */
    function newTemplateEntry (name, url)
    {
        return {
            name: name,
            url: url
        };
    }

    /**
     * Registers a new template
     *
     * @param {string} name The template name
     * @param {string} url The template file's URL
     * @param {string} parent The template's parent (if any)
     *
     * @return {{name: *, url: *, childs: *, parent: *}}
     */
    this.registerTemplate = function (name, url, parent = '')
    {
        logger.debug ('Registering new template \'' + name + '\' for file \'' + url + '\'');
        let entry = newTemplateEntry (name, url);

        entry.childs = {};

        if (parent !== '')
        {
            entry.parent = this.lookupTemplate (parent);
            this.lookupTemplate (parent).childs [entry.name] = entry;
        }

        return templates [name] = entry;
    };

    this.lookupTemplate = function (name)
    {
        if (!templates [name])
        {
            logger.error ('Cannot find template \'' + name + '\'', true);
        }

        return templates [name];
    };

    /**
     * Search for the first parent template for the given template name
     *
     * @param {string} name
     */
    this.getParentTemplate = function (name)
    {
        let currentTemplate;

        // as long as the template has a parent we keep traversing the tree
        for (
            currentTemplate = this.lookupTemplate (name);
            currentTemplate.parent;
            currentTemplate = currentTemplate.parent
        );

        return currentTemplate;
    };

    /**
     * Checks the relationships of the given template and builds a stack of the templates to be loaded
     *
     * @param {string} name
     * @return {Array} The stack of templates to load
     */
    this.getLoadChainForTemplate = function (name)
    {
        let stack = [];
        let currentTemplate = this.lookupTemplate (name);

        do
        {
            stack.push (currentTemplate);
            currentTemplate = currentTemplate.parent;
        } while (currentTemplate);

        return stack;
    };

    this.$get = ['$rootScope', '$templateCache', 'ATATIKI_NGVIEW_TEMPLATE', function ($rootScope, $templateCache, ATATIKI_NGVIEW_TEMPLATE)
    {
        var self = this;

        return {
            lookupTemplate: self.lookupTemplate,
            registerTemplate: self.registerTemplate,
            getParentTemplate: self.getParentTemplate,
            getLoadChainForTemplate: self.getLoadChainForTemplate,
            init: function ()
            {
                /**
                 * Performs the template loading before the route is changed
                 *
                 * This is done by injecting a resolve on the route to force the router to wait until
                 * this request is completed
                 */
                //eslint-disable-next-line angular/on-watch
                $rootScope.$on ('$routeChangeStart', function (event, next)
                {
                    if (next && next.$$route && next.$$route.base)
                    {
                        // force promise on the needed routes
                        if (!next.$$route.resolve)
                        {
                            next.$$route.resolve = {};
                        }

                        // use some weird, random resolve name to prevent clashing
                        next.$$route.resolve ['atatikiTemplateResolve'] = ['$q', '$templateRequest', 'Templating', function ($q, $templateRequest, Templating)
                        {
                            let list = [];
                            let templates = Templating.getLoadChainForTemplate (next.$$route.base);

                            for (let template of templates)
                            {
                                list.push ($templateRequest (template.url));
                            }

                            return $q.all (list).then (
                                function ()
                                {
                                    logger.debug ('Pre-loaded template chain \'' + next.$$route.base + '\' for route ' + next.$$route.originalPath);
                                },
                                function ()
                                {
                                    logger.error ('Cannot pre-load template chain \'' + next.$$route.base + '\' for route ' + next.$$route.originalPath);
                                }
                            );
                        }];
                    }
                });

                // registers the ngView template for the templating system
                if (!$templateCache.get (ATATIKI_NGVIEW_TEMPLATE))
                {
                    $templateCache.put (ATATIKI_NGVIEW_TEMPLATE, '<div ng-view></div>');
                }
            }
        };
    }];
}])
/**
 * Root element for the templating system. Takes care of the template's HTML set up and preparation,
 * keeping the correct template at all times
 */
.directive ('atatikiTemplate', ['$rootScope', 'ATATIKI_NGVIEW_TEMPLATE', 'Logger', 'Templating', function ($rootScope, ATATIKI_NGVIEW_TEMPLATE, Logger, Templating)
{
    const logger = Logger.get ('TemplatingSystem');

    return {
        restrict: 'E',
        template: '<div ng-include="templateContentUrl"></div>',
        replace: true,
        link: function (scope)
        {
            var unregisterOnRouteChangeSuccess = $rootScope.$on ('$routeChangeSuccess', function (event, next, previous)
            {
                // if the template we're using is the same do not do any change to it whatsoever
                var previousTemplate = (previous && previous.$$route && previous.$$route.base) ? previous.$$route.base : '';
                var nextTemplate = (next && next.$$route && next.$$route.base) ? next.$$route.base : '';

                if (previousTemplate == nextTemplate) return;

                // load default ng-view template if no template is specified
                if (nextTemplate === '')
                {
                    scope.templateContentUrl = ATATIKI_NGVIEW_TEMPLATE;
                }
                else
                {
                    // prepare loadChain for the child templates
                    scope.loadChain = angular.extend ([], Templating.getLoadChainForTemplate (nextTemplate));
                    scope.current = scope.loadChain.pop ();

                    scope.templateContentUrl = scope.current.url;
                }

                logger.debug ('Updating template to \'' + scope.templateContentUrl + '\'');
            });

            scope.$on ('$destroy', function ()
            {
                unregisterOnRouteChangeSuccess ();
            });
        }
    };
}])
.directive ('atatikiTemplateTransclude', ['ATATIKI_NGVIEW_TEMPLATE', 'Logger', function (ATATIKI_NGVIEW_TEMPLATE, Logger)
{
    const logger = Logger.get ('TemplatingSystem');

    return {
        restrict: 'AE',
        replace: true,
        template: '<div ng-include="templateContentUrl"></div>',
        link: function (scope)
        {
            var unregisterWatch;

            function renderElementInside ()
            {
                scope.loadChain = angular.extend ([], scope.$parent.loadChain);
                scope.current = scope.loadChain.pop ();

                if (scope.current)
                {
                    scope.templateContentUrl = scope.current.url;
                    logger.debug ('Loading subtemplate \'' + scope.current.name + '\'');
                }
                else
                {
                    scope.templateContentUrl = ATATIKI_NGVIEW_TEMPLATE;
                }
            }

            renderElementInside ();

            unregisterWatch = scope.$parent.$watch ('loadChain', function ()
            {
                renderElementInside ();
            });

            //eslint-disable-next-line angular/on-watch
            scope.$on ('$destroy', function ()
            {
                unregisterWatch ();
            });
        },
    };
}]);

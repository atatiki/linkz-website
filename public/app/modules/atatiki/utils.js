/*eslint angular/module-name: [2,"atatiki"]*/
'use strict';

angular.module ('atatiki.utils', [])
/**
 * Displays a "None" text if the specified value is empty
 */
.directive ('noneText', function ()
{
    return {
        restrict: 'E',
        template: '<span><span class="responsive-none-text" ng-show="value == \'\'">{{ \'NONE\' | translate }}</span><span ng-hide="value == \'\'">{{value}}</span></span>',
        scope: {
            'value': '='
        }
    };
})
/**
 * Highlights the given input with strong tags based on the term specified
 */
.filter ('highlight', ['$sce', function ($sce)
{
    return function (input, term)
    {
        return $sce.trustAsHtml (
            input.replace (
                new RegExp ('(' + term + ')', 'gi'),
                '<strong>$&</strong>'
            )
        );
    };
}]);

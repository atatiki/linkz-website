/*eslint angular/module-name: [2,"atatiki"]*/
'use strict';

angular.module ('atatiki.localization', ['atatiki.loader', 'atatiki.logging', 'pascalprecht.translate'])
.config (['$translateProvider', function ($translateProvider)
{
    $translateProvider
        .preferredLanguage ('en')
        .fallbackLanguage ('en')
        .useSanitizeValueStrategy('sanitizeParameters')
        .useLoader ('$translateLocalizationLoader');
}])
.run (['Localization', function (Localization)
{
    Localization.init ();
}])
.service ('$translateLocalizationLoader', ['Localization', function (Localization)
{
    return function (options)
    {
        return Localization.loadLanguage (options.key);
    };
}])
.service ('Localization', ['$http', '$q', 'ConfigurationLoader', 'Logger', function ($http, $q, ConfigurationLoader, Logger)
{
    const logger = Logger.get ('Localization');
    var languageMap = [];

    var loadLanguage = function (language)
    {
        return $q(function (resolve, reject)
        {
            if (language in languageMap)
            {
                resolve (languageMap [language]);
            }
            else
            {
                $http.get ('app/langs/' + language + '.json').then (
                    function (content)
                    {
                        languageMap [language] = content.data;

                        resolve (languageMap [language]);
                    },
                    function (reason)
                    {
                        logger.error ('Cannot load language information for language \'' + language + '\': ' + reason.statusText);
                        reject ();
                    }
                );
            }
        });
    };

    return {
        init: function ()
        {
            return $q(function (resolve, reject)
            {
                ConfigurationLoader.load ('localization.json').then (
                    function (content)
                    {
                        if ('lang' in content === false)
                        {
                            logger.error ('Cannot load language information, aborting...');
                            reject ();
                        }

                        logger.debug ('Detected default language: ' + content.lang);

                        loadLanguage (content.lang).then (
                            function ()
                            {
                                resolve ();
                            },
                            function ()
                            {
                                reject ();
                            }
                        );
                    },
                    function ()
                    {
                        reject ();
                    }
                );
            });
        },
        loadLanguage: function (language)
        {
            return loadLanguage (language);
        }
    };
}]);

/*eslint angular/module-name: [2,"atatiki"]*/
'use strict';

angular.module ('atatiki.logging', ['angularMoment'])
/**
 * Application logging system to ease the development
 *
 * @author Alexis Maiquez Murcia <alexis.murcia@atatiki.es, almamu@almamu.com>
 */
.provider ('Logger', ['moment', function (moment)
{
    var self = this;
    /** @type {boolean} Whethever the logger has to output messages or not */
    this.debug = true;
    /** @type {Array} The current list of instances the logger is managing */
    this.instances = [];

    function createLogger (loggerName)
    {
        return {
            name: loggerName,
            isDebugMode: self.debug,
            prepareMessage: function (message)
            {
                return '[' + moment ().format () + '] - ' + this.name + ': ' + message;
            },
            message: function (message, callback)
            {
                if (this.isDebugMode === true)
                {
                    callback (message);
                }
            },
            log: function (message)
            {
                //eslint-disable-next-line no-console
                this.message (this.prepareMessage (message), console.log);
            },
            error: function (message, shouldThrow = false)
            {
                var preparedMessage = this.prepareMessage (message);

                //eslint-disable-next-line no-console
                this.message (preparedMessage, console.error);

                if (shouldThrow)
                {
                    throw new Error (preparedMessage);
                }
            },
            debug: function (message)
            {
                //eslint-disable-next-line no-console
                this.message (this.prepareMessage (message), console.debug);
            },
            warn: function (message)
            {
                //eslint-disable-next-line no-console
                this.message (this.prepareMessage (message), console.warn);
            },
            info: function (message)
            {
                this.message (this.prepareMessage (message), console.info);
            },
            enableDebug: function ()
            {
                this.isDebugMode = true;
            },
            disableDebug: function ()
            {
                this.isDebugMode = false;
            }
        };
    }

    /**
     * Enables the debug mode for the new logger instances
     */
    this.enableDebug = function ()
    {
        this.debug = true;
    };

    /**
     * Disables the debug mode for the new logger instances
     */
    this.disableDebug = function ()
    {
        this.debug = false;
    };

    /**
     * Searches for the given {loggerName} logger and creates it if it doesn't exist yet
     *
     * @param {string} loggerName The logger to search for
     *
     * @return {{}} The logger
     */
    this.get = function (loggerName)
    {
        if (loggerName in this.instances)
        {
            return this.instances [loggerName];
        }

        return this.instances [loggerName] = createLogger (loggerName);
    };

    /**
     * Angular magic element that makes the LoggerProvider a service known as Logger
     * @return {*}
     */
    this.$get = function ()
    {
        return this;
    };
}]);

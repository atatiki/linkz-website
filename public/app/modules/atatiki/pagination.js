/*eslint angular/module-name: [2,"atatiki"]*/
'use strict';

angular.module ('atatiki.pagination', [])
/**
 * Full-blown pagination directive that displays the common paginator on the specified web
 * and allows to navigate the pages
 */
.directive ('pagination', function ()
{
    return {
        restrict: 'E',
        scope: {
            'maxRecords': '=',
            'currentRecord': '=',
            'recordsPerPage': '=',
            'gotoPageCallback': '='
        },
        templateUrl: 'app/modules/atatiki/pagination.html',
        link: function (scope)
        {
            /**
             * Calculates the limits of the paginator
             */
            scope.calculateLimits = function ()
            {
                // get the page count first
                scope.maxPages = scope.maxRecords / scope.recordsPerPage;

                if (Math.floor (scope.maxPages) < scope.maxPages)
                {
                    scope.maxPages = Math.floor (scope.maxPages) + 1;
                }

                // get current page number (+1 because of 0 based index)
                scope.currentPage = (scope.currentRecord / scope.recordsPerPage) + 1;

                // enable/disable buttons based on position
                scope.previousEnabled = scope.currentPage > 1;
                scope.nextEnabled = scope.currentPage < scope.maxPages;
            };

            /**
             * Prepares the actual paginator display, organizing the numbers that are going ot be shown and
             * how many of them. It also dynamically changes the numbers displayed when there are too many records
             * allowing for a more dynamic paginator
             */
            scope.renderNumbers = function ()
            {
                scope.countVisible = true;

                // make the paginator visible based on how many pages there are available
                if (scope.maxPages == 0 || scope.maxPages == 1 || angular.isNumber (scope.maxPages) == false || Number.isNaN (scope.maxPages))
                {
                    scope.currentPage = 1;
                    scope.visible = false;

                    if (scope.maxRecords == 0)
                    {
                        scope.countVisible = false;
                    }

                    return;
                }

                scope.visible = true;
                // prepare pagination numbers to be displayed
                scope.pagesToDisplay = [];

                if (scope.maxPages < 5)
                {
                    for (let i = 0; i < scope.maxPages; i ++)
                    {
                        scope.pagesToDisplay.push (i + 1);
                    }

                    return;
                }

                // 10 pages of display
                let start = 1;

                if (scope.currentPage > 5)
                {
                    start = scope.currentPage - 5;
                }

                if (start < 1) start = 1;

                let end = start + 10;

                if (end > scope.maxPages)
                {
                    end = scope.maxPages;
                }

                for (; start <= end; start ++)
                {
                    scope.pagesToDisplay.push (start);
                }
            };

            /**
             * Callback from the layout, performs a callback to allow the controller to take care
             * of the actual data-fetching logic
             *
             * @param page int The page to change to
             */
            scope.gotoPage = function (page)
            {
                if (page < 1 || page > scope.maxPages || angular.isFunction (scope.gotoPageCallback) === false) return;

                // call the function to let the view know we want to change pages
                if (scope.gotoPageCallback (page) == true)
                {
                    // switch actives
                    scope.currentPage = page;
                    // enable/disable buttons based on position
                    scope.previousEnabled = scope.currentPage > 1;
                    scope.nextEnabled = scope.currentPage < scope.maxPages;
                }

                // also force the redraw of the control
                scope.renderNumbers ();
            };

            // prepare things
            scope.calculateLimits ();
            // render the numbers
            scope.renderNumbers ();
            // in case the maxRecords count is changed this makes sure the new data is calculated and rendered
            scope.$watch ('maxRecords', function ()
            {
                scope.calculateLimits ();
                scope.renderNumbers ();
            });
            scope.$watch ('currentRecord', function ()
            {
            });
        }
    };
})
.service ('PaginatorService', function ()
{
    //eslint-disable-next-line no-unused-vars
    return function (page = 1, count = 100)
    {
        return {

        };
    };
});

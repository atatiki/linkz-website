/*eslint angular/module-name: [2,"atatiki"]*/
'use strict';

angular.module ('atatiki.caching', ['atatiki.logging'])
/**
 * Caching service that allows for easy management of the cache's in the system
 *
 * @author Alexis Maiquez Murcia <alexis.murcia@atatiki.es, almamu@almamu.com>
 */
.provider ('Cache', ['LoggerProvider', 'moment', function (LoggerProvider, moment)
{
    /** @type {{}} The data the cache is storing */
    this.handlers = {};
    this.loaders = {};
    this.logger = LoggerProvider.get ('Cache');

    this.$get = ['$cacheFactory', '$localStorage', '$q', function ($cacheFactory, $localStorage, $q)
    {
        var self = this;

        function performLoad (key)
        {
            return $q(function (resolve, reject)
            {
                var cache = $cacheFactory (key);
                var self = this;

                self.loaders [key] ().then (
                    function (result)
                    {
                        cache.put ('value', result);
                        cache.put ('time', moment ());
                        cache.put ('status', 'ok');

                        // cache is persistent, save the results in localStorage
                        if (cache.get ('persistent') == true)
                        {
                            $localStorage.cache [key] = {
                                expiration: cache.get ('expiration'),
                                value: cache.get ('value'),
                                time: cache.get ('time'),
                                status: cache.get ('status')
                            };
                        }

                        if (key in self.handlers === false)
                        {
                            return self.logger.warn ('Cache loaded but no handlers are registered');
                        }

                        for (const i in self.handlers [key])
                        {
                            if (self.handlers [key].hasOwnProperty (i) === false) continue;
                            if ('success' in self.handlers [key] [i] === false) continue;

                            self.handlers [key] [i].success (result);
                        }

                        resolve (result);
                    },
                    function (error)
                    {
                        cache.put ('status', 'error');

                        if (key in self.handlers === false)
                        {
                            return self.logger.warn ('Cache loaded but no handlers are registered');
                        }

                        for (const i in self.handlers [key])
                        {
                            if (self.handlers [key].hasOwnProperty (i) === false) continue;
                            if ('fail' in self.handlers [key] [i] === false) continue;

                            self.handlers [key] [i].fail (error);
                        }

                        reject ();
                    }
                );
            });
        }

        /**
         * Loads cache information for $localStorage and saves it in $cacheFactory to allow usage
         */
        self.init = function ()
        {
            let cacheContent = $localStorage.cache;

            for (const key in cacheContent)
            {
                if (cacheContent.hasOwnProperty (key) === false) continue;

                let cache = $cacheFactory (key);

                cache.put ('time', cacheContent [key].time);
                cache.put ('value', cacheContent [key].value);
                cache.put ('expiration', cacheContent [key].expiration);
                cache.put ('persistent', true);
                cache.put ('status', cacheContent [key].status);
            }
        };

        /**
         * Loads the given cache key info
         *
         * @param {string} key The key info to load
         */
        self.load = function (key)
        {
            return $q (function (resolve, reject)
            {
                if (key in self.loaders === false)
                {
                    self.logger.error ('Trying to load a cache without any loader registered');
                    reject ();
                }
                else
                {
                    let cache = $cacheFactory (key);
                    let expiration = moment (cache.get ('time')).add (cache.get ('expiration'), 'seconds');

                    // if cache is ok and valid
                    if (cache.get ('status') == 'ok' && moment ().isAfter (expiration) == true)
                    {
                        resolve (cache.get ('value'));
                    }
                    else
                    {
                        return performLoad (key);
                    }
                }
            });
        };

        /**
         * Registers a loader for the given key
         *
         * @param {string} key The cache key to register the loader for
         * @param {function} callback The function to call when the cache key is being loaded
         * @param {{expiration, persistent}} options The configuration options for the cache
         *
         * Currently only the option "expiration" is supported. This indicates the time the
         * cache will be alive and will have to be requested to the server again
         */
        self.registerLoader = function (key, callback, options = {expiration: 60, persistent: false})
        {
            if (key in self.loaders)
            {
                return self.logger.error ('Cache \'' + key + '\' already has a loader registered');
            }

            if ('persistent' in options === false)
            {
                options.persistent = false;
            }

            if ('expiration' in options === false)
            {
                options.expiration = 60;
            }

            let cache = $cacheFactory (key);

            cache.put ('expiration', options.expiration);
            cache.put ('persistent', options.persistent);
            cache.put ('status', 'waiting');

            self.loaders [key] = callback;
        };

        /**
         * Registers a handler for when the cache loading fails or success
         *
         * @param {string} key The cache key to register the handler for
         * @param {function} successCallback The function to call when the cache load is successful
         * @param {function} failCallback The function to call when the cache load fails
         */
        self.registerHandler = function (key, successCallback = null, failCallback = null)
        {
            if (key in self.handlers === false)
            {
                self.handlers [key] = [];
            }

            let handlerData = {};

            if (successCallback !== null)
            {
                handlerData.success = successCallback;
            }

            if (failCallback !== null)
            {
                handlerData.fail = failCallback;
            }

            self.handlers [key].push (handlerData);
        };

        return self;
    }];
}])
.run (['Cache', function (Cache)
{
    Cache.init ();
}]);

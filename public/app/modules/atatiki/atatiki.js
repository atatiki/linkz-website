/*eslint angular/module-name: [2,"atatiki"]*/
'use strict';

angular.module ('atatiki', [
    'atatiki.caching',
    'atatiki.core',
    'atatiki.loader',
    'atatiki.localization',
    'atatiki.logging',
    'atatiki.pagination',
    'atatiki.templating',
    'atatiki.utils',
    'atatiki.configuration',
    'atatiki.bootstrap'
]);

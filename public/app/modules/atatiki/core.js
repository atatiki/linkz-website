/*eslint angular/module-name: [2,"atatiki"]*/
'use strict';

angular.module('atatiki.core', ['ngStorage', 'angularMoment', 'ui-notification', 'atatiki.utils', 'atatiki.caching', 'atatiki.logging']);

'use strict';

angular.module ('atatiki.bootstrap', [])
/**
 * Shortcut for bootstrap modals, also implementing the tab functionality to prevent UI bugs when those are loaded
 * dynamically
 */
.directive ('btModal', [function ()
{
    return {
        transclude: true,
        restrict: 'E',
        replace: true,
        scope: {
            'size': '=?'
        },
        templateUrl: 'app/modules/atatiki/modal.html',
        link: function (scope)
        {
            if (angular.isUndefined (scope.size) === true)
            {
                scope.size = "small";
            }

            scope.modal_class = {'modal-dialog': true, 'modal-lg': scope.size == 'big'};

            // fix for any tablist in the system
            $('[role="tablist"] > a[role="tab"]').off ('click').on ('click', function (e)
            {
                e.preventDefault ();
                $(this).tab ('show');
            });
        }
    };
}])
.directive ('btModalContent', [function ()
{
    return {
        template: '<div class="modal-body" ng-transclude></div>',
        replace: true,
        transclude: true,
        restrict: 'E'
    };
}])
.directive ('btModalFooter', [function ()
{
    return {
        template: '<div class="modal-footer" ng-transclude></div>',
        replace: true,
        transclude: true,
        restrict: 'E'
    }
}])
.directive ('btModalHeader', [function ()
{
    return {
        template: '\n' +
            '            <div class="modal-header">\n' +
            '                <h5 class="modal-title">{{ btTitle }}</h5>\n' +
            '                <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-show="btCanClose">\n' +
            '                    <span aria-hidden="true">&times;</span>\n' +
            '                </button>\n' +
            '            </div>',
        replace: true,
        scope: {
            'btCanClose': '=',
            'btTitle': '='
        },
        restrict: 'E'
    }
}]);

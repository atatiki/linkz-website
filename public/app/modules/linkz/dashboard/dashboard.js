'use strict';

angular.module ('linkz.dashboard', ['ui.bootstrap'])
.config (['$routeProvider', 'PERM_PARENT', 'PERM_STUDENT', 'PERM_TUTOR',
function ($routeProvider, PERM_PARENT, PERM_STUDENT, PERM_TUTOR)
{
    $routeProvider
    .when ('/dashboard/', {
        redirectTo: '/dashboard/home/feed/'
    })
    .when ('/dashboard/home/:mode/:page?', {
        base: 'dashboard',
        templateUrl: 'app/modules/linkz/dashboard/dashboard.html',
        permissions: PERM_STUDENT | PERM_PARENT | PERM_TUTOR,
        controller: 'DashboardController',
        menuEntry: 'home'
    })
    .when ('/dashboard/details/:id/', {
        base: 'dashboard',
        templateUrl: 'app/modules/linkz/dashboard/details.html',
        permissions: PERM_PARENT | PERM_STUDENT | PERM_TUTOR,
        controller: 'DashboardDetailsController',
        menuEntry: 'home'
    })
    .when ('/dashboard/schedule', {
        base: 'app',
        templateUrl: 'app/modules/linkz/dashboard/schedule.html',
        permissions: PERM_PARENT | PERM_TUTOR,
        controller: 'DashboardScheduleController',
        menuEntry: 'home'
    });
}])
.run (['$rootScope', 'NavigationBar', function ($rootScope, NavigationBar)
{
    /// TODO: MOVE THIS TO A DIFFERENT PLACE
    NavigationBar.registerMenu ('parent_dashboard');
    NavigationBar.registerMenuEntry (
        'parent_dashboard', 'dashboard',
        function ()
        {
            $rootScope.$broadcast ('CalendarGoToday');
        }, 'today', 'calendar'
    );
    NavigationBar.registerMenuEntry (
        'parent_dashboard', 'feed',
        function ()
        {
            $rootScope.$broadcast ('EnterFeedModePressed');
        },
        'dashboard', 'feed'
    );

    NavigationBar.registerMenu ('student_dashboard');
    NavigationBar.registerMenuEntry (
        'student_dashboard', 'dashboard',
        function ()
        {
            $rootScope.$broadcast ('CalendarGoToday');
        }, 'today', 'calendar'
    );
    NavigationBar.registerMenuEntry (
        'student_dashboard', 'feed',
        function ()
        {
            $rootScope.$broadcast ('EnterFeedModePressed');
        },
        'dashboard', 'feed'
    );

    NavigationBar.registerMenu ('tutor_dashboard');
    NavigationBar.registerMenuEntry (
        'tutor_dashboard', 'dashboard',
        function ()
        {
            $rootScope.$broadcast ('CalendarGoToday');
        }, 'today', 'calendar'
    );
    NavigationBar.registerMenuEntry (
        'tutor_dashboard', 'feed',
        function ()
        {
            $rootScope.$broadcast ('EnterFeedModePressed');
        },
        'dashboard', 'feed'
    );
}])
.directive ('dashboardCalendar', ['$rootScope', 'Event', function ($rootScope, Event)
{
    return {
        scope: {},
        replace: true,
        restrict: 'E',
        templateUrl: 'app/modules/linkz/dashboard/calendar.html',
        link: function (scope)
        {
            function fetchHeatmap (current)
            {
                let element = document.getElementById ('this-is-a-unique-identifier');

                if (element)
                {
                    let parent = element.parentNode;
                    parent.removeChild (element);
                }

                // add some days so we get the proper begining and end
                if (current.getDate () == 1) current.setDate (2);

                // empty the heatmap
                let keylist = Object.keys (scope.heatmap);

                for (let i = 0; i < keylist.length; i ++)
                {
                    delete scope.heatmap [keylist [i]];
                }

                Event.heatmap (moment (current)).then (
                    function (result)
                    {
                        for (let i = 0; i < result.data.data.length; i ++)
                        {
                            if (result.data.data [i] ['Ev105_Status_ac'] == 'Not confirmed' || result.data.data [i] ['Ev105_Status_ac'] == 'Canceled within 24h')
                                continue;

                            if (!(result.data.data [i] ['Ev100_Date'] in scope.heatmap))
                            {
                                scope.heatmap [result.data.data [i] ['Ev100_Date']] = 1;
                            }
                            else
                            {
                                scope.heatmap [result.data.data [i] ['Ev100_Date']] ++;
                            }
                        }

                        let style = '';
                        let keylist = Object.keys (scope.heatmap);

                        for (let i = 0; i < keylist.length; i ++)
                        {
                            let count = scope.heatmap [keylist [i]];

                            style += '.background-' + keylist [i].replace ('/', '-').replace ('/', '-') +' {';

                            if (count == 1)
                            {
                                style += 'background-color: rgba(0, 0, 0, .3) !important;';
                            }
                            else if (count == 2 || count == 3)
                            {
                                style += 'background-color: rgba(0, 0, 0, .4) !important;';
                            }
                            else if (count >= 4)
                            {
                                style += 'background-color: rgba(0, 0, 0, .5) !important;';
                            }

                            style += '}';
                        }

                        let sheet = document.createElement('style');
                        sheet.innerHTML = style;
                        sheet.id = 'this-is-a-unique-identifier';
                        document.body.appendChild(sheet);
                    }
                );
            }

            scope.options = {showWeeks: false, maxMode: 'day', customClass: function (info)
            {
                // WARNING: THIS SHOULD BE STANDARIZED AND THE TYPICAL TALK PEOPLE AGAINST JAVASCRIPT HAS
                // THIS IS NOT A GOOD SOLUTION, BUT GETS THE JOB DONE AND IS ONLY USED HERE, NOWHERE ELSE
                function strleftzero (value)
                {
                    let str = '' + value;

                    while (str.length < 2)
                    {
                        str = '0' + str;
                    }

                    return str;
                }

                let date = info.date;
                let formatdate = strleftzero (date.getMonth () + 1) + '-' + strleftzero (date.getDate ()) + '-' + date.getFullYear ();

                if (scope.date != null && date.getMonth () != scope.date.getMonth ()) return '';

                return 'background-' + formatdate;
            }};
            scope.date = null;
            scope.heatmap = {};

            let unregisterCalendarDateUpdateRequest = $rootScope.$on ('FetchCalendarDate', function ()
            {
                if (scope.date !== null)
                    $rootScope.$broadcast ('CalendarFilterChanged', scope.date);
                else // special situation, null value means feed mode
                    $rootScope.$broadcast ('EnterFeedModePressed');
            });

            let unregisterUpdateCalendarDate = $rootScope.$on ('UpdateCalendarDate', function (ev, date)
            {
                scope.date = date;
            });

            let unregisterOn = scope.$watch ('date', function (newVal, oldValue)
            {
                if (scope.date !== null)
                {
                    $rootScope.$broadcast ('CalendarFilterChanged', scope.date);
                }

                // months are not the same, fetch event heatmap
                if (oldValue == null || oldValue.getMonth () != newVal.getMonth () || oldValue.getFullYear () != newVal.getFullYear ())
                {
                    let current = newVal;

                    if (current == null)
                    {
                        current = new Date ();
                    }

                    fetchHeatmap (current);
                }
            });

            let unregisterFeed = $rootScope.$on ('EnterFeedMode', function ()
            {
                scope.date = null;
            });

            let unregisterChange = $rootScope.$on ('uib:datepicker.refresh', function (event, date)
            {
                fetchHeatmap (date);
            });

            scope.$on ('$destroy', function ()
            {
                unregisterOn ();
                unregisterFeed ();
                unregisterCalendarDateUpdateRequest ();
                unregisterChange ();
                unregisterUpdateCalendarDate ();
            });
        }
    };
}])
.directive ('newNote', ['$q', '$rootScope', 'Event', 'Loader', 'moment', function ($q, $rootScope, Event, Loader, moment)
{
    return {
        templateUrl: 'app/modules/linkz/dashboard/newnote.html',
        replace: true,
        scope: {},
        restrict: 'E',
        link: function (scope)
        {
            let unregisterOn = $rootScope.$on ('OpenCreateNewNote', function (event, eventInfo)
            {
                scope.noteCreated = false;
                scope.data = {note: '', visibleToParent: 0, visibleToStudent: 0};
                scope.eventInfo = eventInfo;
                angular.element ('#newnote-modal').modal ('show');
            });

            scope.time = moment ();
            scope.loader = Loader ();
            scope.loader.promiseResolved ({});

            scope.createNote = function ()
            {
                scope.loader.reset ();
                scope.loader.queue (
                    Event.createNote (scope.eventInfo ['_id'], scope.data.note, scope.data.visibleToParent, scope.data.visibleToStudent).then (
                        function (result)
                        {
                            scope.noteCreated = true;
                            $rootScope.$broadcast ('NewNoteCreated', {
                                'No101_Description': scope.data.note,
                                'No100_Date': moment ().format ('MM/DD/YYYY'),
                                'No300_VisibleToStudent': scope.data.visibleToStudent,
                                'No301_VisibleToParent': scope.data.visibleToParent
                            });
                            return result;
                        },
                        function ()
                        {
                            return $q.reject ();
                        }
                    )
                );
            };

            scope.$on ('$destroy', function ()
            {
                unregisterOn ();
            });
        }
    }
}])
.directive ('uploadNewFile', ['$rootScope', 'API', 'upload', function ($rootScope, API, upload)
{
    return {
        templateUrl: 'app/modules/linkz/dashboard/newfilemodal.html',
        replace: true,
        scope: {},
        restrict: 'E',
        link: function (scope)
        {
            scope.API = API;

            let unregisterOn = $rootScope.$on ('OpenFileUpload', function (event, eventInfo)
            {
                scope.eventInfo = eventInfo;
                scope.fileUpload = 'wait';
                angular.element ('#uploadfile-modal').modal ('show');
            });

            scope.performUpload = function ()
            {
                scope.fileUpload = 'uploading';

                upload ({
                    url: API ('event/' + scope.eventInfo ['_id'] + '/upload'),
                    method: 'POST',
                    data: {
                        file: angular.element ('#upload')
                    }
                }).then (
                    function (result)
                    {
                        $rootScope.$broadcast ('FileUploaded', result.data);
                        scope.fileUpload = 'uploaded';
                    },
                    function ()
                    {
                        scope.fileUpload = 'error';
                    }
                );
            };

            scope.isUploading = function ()
            {
                return scope.fileUpload == 'uploading';
            };

            scope.isUploaded = function ()
            {
                return scope.fileUpload == 'uploaded';
            };

            scope.isError = function ()
            {
                return scope.fileUpload == 'error';
            };

            scope.isWaiting = function ()
            {
                return scope.fileUpload == 'wait';
            };

            scope.$on ('$destroy', function ()
            {
                unregisterOn ();
            });
        }
    };
}])
.directive ('scheduleNewEvent', ['$rootScope', 'Account', 'Contracts', 'Event', 'Loader', function ($rootScope, Account, Contracts, Event, Loader)
{
    return {
        templateUrl: 'app/modules/linkz/dashboard/schedule.html',
        replace: true,
        scope: {},
        restrict: 'E',
        link: function (scope)
        {
            let unregisterDateUpdate = null;
            let unregisterEndUpdate = null;

            let unregisterOn = $rootScope.$on ('OpenCreateNewEvent', function ()
            {
                scope.eventCreationStepCount = 3;
                scope.step1 = {
                    loader: Loader (),
                    page: 1,
                    selectedContract: {},
                    goto: function (page)
                    {
                        this.page = page;

                        scope.step1.loader.reset ();
                        scope.step1.loader.queue (Contracts.get (this.page, 1));

                        return true;
                    }
                };
                scope.step2 = {
                    date: new Date (),
                    end: new Date ()
                };
                scope.step3 = {
                    subject: '',
                    description: '',
                    options: []
                };

                Account.getSubjectsList ().then (
                    function (result)
                    {
                        scope.step3.options = result.data;
                    }
                );

                scope.step2.date.setMinutes (0);
                scope.step2.date.setHours (scope.step2.date.getHours () + 1);
                scope.step2.end.setMinutes (0);
                scope.step2.end.setHours (scope.step2.date.getHours () + 1);

                // initialize event creation variables
                scope.eventCreationStep = 1;
                scope.isBefore = false;
                scope.isTimeCorrect = false;
                scope.step1.goto (1);

                if (unregisterDateUpdate !== null)
                    unregisterDateUpdate ();
                if (unregisterEndUpdate !== null)
                    unregisterEndUpdate ();

                unregisterDateUpdate = scope.$watch ('step2.date', function ()
                {
                    // set day month and year to be the same
                    scope.step2.end.setDate (scope.step2.date.getDate ());
                    scope.step2.end.setFullYear (scope.step2.date.getFullYear ());
                    scope.step2.end.setMonth (scope.step2.date.getMonth ());

                    scope.isTimeCorrect = moment (scope.step2.end).isAfter (moment (scope.step2.date));
                    scope.isBefore = moment (scope.step2.date).isBefore (moment ());
                });

                unregisterEndUpdate = scope.$watch ('step2.end', function ()
                {
                    // set day month and year to be the same
                    scope.step2.end.setDate (scope.step2.date.getDate ());
                    scope.step2.end.setFullYear (scope.step2.date.getFullYear ());
                    scope.step2.end.setMonth (scope.step2.date.getMonth ());

                    scope.isTimeCorrect = moment (scope.step2.end).isAfter (moment (scope.step2.date));
                    scope.isBefore = moment (scope.step2.date).isBefore (moment ());
                });

                angular.element ('#schedule-new-event').modal ('show');
            });

            scope.canGoNextStep = function ()
            {
                if (scope.eventCreationStep === 1 && angular.isUndefined (scope.step1.selectedContract ['_id']))
                {
                    return false;
                }

                if (scope.eventCreationStep === 2)
                {
                    // perform validation
                    if (scope.isBefore || scope.isTimeCorrect == false)
                    {
                        return false;
                    }
                }

                if (scope.eventCreationStep === 3)
                {
                    if (scope.step3.subject == '')
                        return false;
                }

                return true;
            };

            scope.nextStep = function ()
            {
                if (scope.canGoNextStep () === false)
                {
                    return;
                }

                if (scope.eventCreationStep === 3)
                {
                    scope.eventCreationStep = 4;

                    Event.create (scope.step2.date, scope.step2.end, scope.step1.selectedContract ['_id'], scope.step3.subject ['Ct100_Name'], scope.step3.description).then (
                        function ()
                        {
                            // update the calendar
                            $rootScope.$broadcast ('FetchCalendarDate');

                            scope.eventCreationStep = 6;
                        },
                        function ()
                        {
                            scope.eventCreationStep = 5;
                        }
                    );

                    return;
                }

                scope.eventCreationStep ++;
            };

            scope.previousStep = function ()
            {
                if (scope.eventCreationStep == 5)
                    scope.eventCreationStep = 3;
                else if (scope.eventCreationStep > 1)
                    scope.eventCreationStep --;
            };

            scope.$on ('$destroy', function ()
            {
                unregisterOn ();

                if (unregisterDateUpdate !== null)
                    unregisterDateUpdate ();

                if (unregisterEndUpdate !== null)
                    unregisterEndUpdate ();
            });
        }
    };
}])
.directive ('updateBooking', ['$rootScope', 'Contracts', 'Event', function ($rootScope, Contracts, Event)
{
    return {
        templateUrl: 'app/modules/linkz/dashboard/update-schedule.html',
        replace: true,
        scope: {},
        restrict: 'E',
        link: function (scope)
        {
            let unregisterDateUpdate = null;
            let unregisterEndUpdate = null;

            let unregisterOn = $rootScope.$on ('OpenScheduleUpdate', function (event, eventId)
            {
                scope.eventId = eventId;
                scope.eventCreationStep = 1;
                scope.step1 = {
                    date: new Date (),
                    end: new Date ()
                };

                scope.step1.date.setMinutes (0);
                scope.step1.date.setHours (scope.step1.date.getHours () + 1);
                scope.step1.end.setMinutes (0);
                scope.step1.end.setHours (scope.step1.date.getHours () + 1);

                // initialize event creation variables
                scope.eventCreationStep = 1;
                scope.isBefore = false;
                scope.isTimeCorrect = false;

                if (unregisterDateUpdate !== null)
                    unregisterDateUpdate ();
                if (unregisterEndUpdate !== null)
                    unregisterEndUpdate ();

                unregisterDateUpdate = scope.$watch ('step1.date', function ()
                {
                    // set day month and year to be the same
                    scope.step1.end.setDate (scope.step1.date.getDate ());
                    scope.step1.end.setFullYear (scope.step1.date.getFullYear ());
                    scope.step1.end.setMonth (scope.step1.date.getMonth ());

                    scope.isTimeCorrect = moment (scope.step1.end).isAfter (moment (scope.step1.date));
                    scope.isBefore = moment (scope.step1.date).isBefore (moment ());
                });

                unregisterEndUpdate = scope.$watch ('step1.end', function ()
                {
                    // set day month and year to be the same
                    scope.step1.end.setDate (scope.step1.date.getDate ());
                    scope.step1.end.setFullYear (scope.step1.date.getFullYear ());
                    scope.step1.end.setMonth (scope.step1.date.getMonth ());

                    scope.isTimeCorrect = moment (scope.step1.end).isAfter (moment (scope.step1.date));
                    scope.isBefore = moment (scope.step1.date).isBefore (moment ());
                });

                angular.element ('#schedule-update-event').modal ('show');
            });

            scope.canGoNextStep = function ()
            {
                if (scope.eventCreationStep === 1 && scope.isBefore || scope.isTimeCorrect == false)
                {
                    return false;
                }

                return true;
            };

            scope.nextStep = function ()
            {
                if (scope.canGoNextStep () === false)
                {
                    return;
                }

                if (scope.eventCreationStep === 1)
                {
                    scope.eventCreationStep = 2;

                    Event.update (scope.eventId, scope.step1.date, scope.step1.end).then (
                        function ()
                        {
                            scope.eventCreationStep = 4;
                        },
                        function ()
                        {
                            scope.eventCreationStep = 3;
                        }
                    );

                    return;
                }
            };

            scope.previousStep = function ()
            {
                if (scope.eventCreationStep == 3)
                    scope.eventCreationStep = 1;
            };

            scope.$on ('$destroy', function ()
            {
                unregisterOn ();

                if (unregisterDateUpdate !== null)
                    unregisterDateUpdate ();

                if (unregisterEndUpdate !== null)
                    unregisterEndUpdate ();
            });
        }
    };
}])
.controller ('DashboardController', ['$location', '$rootScope', '$routeParams', '$scope', '$timeout', 'Authentication', 'Loader', 'Event', 'moment', 'PERM_PARENT', 'PERM_STUDENT', 'PERM_TUTOR',
function ($location, $rootScope, $routeParams, $scope, $timeout, Authentication, Loader, Event, moment)
{
    $scope.ignoreStudents = [];
    $scope.loader = Loader ();
    $scope.mode = $routeParams.mode;
    $scope.date = null;
    $scope.page = angular.isDefined ($routeParams.page) ? $routeParams.page : 1;
    $scope.currentRecord = 0;
    $scope.showCancelled = false;
    let fetchTimeout = -1;

    function fetchData ()
    {
        // we need to have at least a date to know what to request
        if ($scope.mode === 'date' && (angular.isUndefined ($scope.date) || $scope.date == null))
        {
            return;
        }

        if ($scope.mode === 'date')
        {
            $scope.loader.queue (Event.get ($scope.date, $scope.page, $scope.ignoreStudents, $scope.showCancelled));
        }
        else
        {
            $scope.loader.queue (Event.feed ($scope.page, $scope.ignoreStudents, $scope.showCancelled));
        }
    }

    function queueFetchData ()
    {
        $scope.loader.reset ();

        if (fetchTimeout)
            $timeout.cancel (fetchTimeout);

        fetchTimeout = $timeout (fetchData, 1000);
    }

    let unregisterStudentChangeListener = $rootScope.$on ('StudentFilterChanged', function (event, students)
    {
        $scope.ignoreStudents = [];

        angular.forEach (students, function (value)
        {
            if (!value.active)
            {
                $scope.ignoreStudents.push (value);
            }
        });

        queueFetchData ();
    });

    let unregisterCancelledChangeListener = $rootScope.$on ('CancelledFilterChanged', function (event, showCancelled)
    {
        $scope.showCancelled = showCancelled;

        queueFetchData ();
    });

    let unregisterCalendarChangeListener = $rootScope.$on ('CalendarFilterChanged', function (event, newDate)
    {
        $location.path ('/dashboard/home/date/');
        $scope.page = 1;
        $scope.date = moment (newDate);
        $scope.mode = 'date';
        queueFetchData ();
    });

    // this is using precise
    let unregisterFeedModeListener = $rootScope.$on ('EnterFeedModePressed', function ()
    {
        $location.path ('/dashboard/home/feed/');
        $scope.mode = 'feed';
        queueFetchData ();
    });

    let unregisterGoToToday = $rootScope.$on ('CalendarGoToday', function ()
    {
        $location.path ('/dashboard/home/date/');
        $scope.page = 1;
        $scope.date = moment ();
        $scope.mode = 'date';
        queueFetchData ();
        $rootScope.$broadcast ('UpdateCalendarDate', $scope.date);
    });

    if ($scope.mode == 'feed')
    {
        $rootScope.$broadcast ('EnterFeedMode');
    }
    else
    {
        $scope.date = moment (new Date ());
        $rootScope.$broadcast ('FetchCalendarDate');
    }

    $scope.loader.setResolveCallback (function ()
    {
        let lastDate = 0;

        $scope.loader.data.feed = [];

        angular.forEach ($scope.loader.data.data.data, function (value)
        {
            if (value ['Ev100_Date'] !== lastDate)
            {
                lastDate = value ['Ev100_Date'];

                $scope.loader.data.feed.push ({type: 'date', moment: moment (value ['Ev100_Date'], 'MM/DD/YYYY')});
            }

            $scope.loader.data.feed.push ({type: 'entry', data: value});
        });
    });

    $scope.$on ('$destroy', function ()
    {
        unregisterStudentChangeListener ();
        unregisterCalendarChangeListener ();
        unregisterFeedModeListener ();
        unregisterCancelledChangeListener ();
        unregisterGoToToday ();
    });

    $scope.details = function (entry)
    {
        $location.path ('/dashboard/details/' + entry ['_id'] + '/');
    };

    $scope.gotoPage = function (page)
    {
        $scope.page = page;

        queueFetchData ();

        return true;
    };

    queueFetchData ();

    $scope.openScheduleNewEvent = function ()
    {
        $rootScope.$broadcast ('OpenCreateNewEvent');
    };
}])
.controller ('DashboardDetailsController', ['$location', '$rootScope', '$routeParams', '$scope', '$q', 'Authentication', 'Loader', 'Event',
function ($location, $rootScope, $routeParams, $scope, $q, Authentication, Loader, Event)
{
    $scope.reject = {reason: '', diff: 0};

    let unregisterListenToNewNote = $rootScope.$on ('NewNoteCreated', function (event, noteInfo)
    {
        $scope.loader.data.data.notes.push (noteInfo);
    });

    let unregisterEnterFeedMode = $rootScope.$on ('EnterFeedModePressed', function ()
    {
        $location.path ('/dashboard/home/feed/');
    });

    let unregisterCalendarFilterChanged = $rootScope.$on ('CalendarFilterChanged', function ()
    {
        $location.path ('/dashboard/home/date/');
    });

    let unregisterFileUploaded = $rootScope.$on ('FileUploaded', function (event, fileInformation)
    {
        $scope.loader.data.data.files.push (fileInformation);
    });

    $scope.loader = Loader ();
    $scope.cancellation = {reasons: []};

    function fetchEventData ()
    {
        $scope.loader.queue (Event.getById ($routeParams.id));
        Event.getCancellationReasons ().then (
            function (result)
            {
                $scope.cancellation.reasons = result.data;
            }
        );
    }

    fetchEventData ();

    $scope.goback = function ()
    {
        $location.path ('/dashboard/home/date/');
    };

    $scope.gotoContract = function ()
    {
        $location.path ('/contracts/' + $scope.loader.data.data.event ['_id_Contract'] + '/details/');
    };

    $scope.getClassForNote = function (note)
    {
        return {
            'alert-info':    $rootScope.isParent () || $rootScope.isStudent () || ($rootScope.isTutor () && note ['No300_VisibleToStudent'] == 1 && note ['No301_VisibleToParent'] != 1),
            'alert-danger':  $rootScope.isTutor () && (note ['No300_VisibleToStudent'] != 1 && note ['No301_VisibleToParent'] != 1),
            'alert-success': $rootScope.isTutor () && (note ['No300_VisibleToStudent'] == 1 && note ['No301_VisibleToParent'] == 1),
            'alert-warning': $rootScope.isTutor () && (note ['No301_VisibleToParent'] == 1 && note ['No300_VisibleToStudent'] != 1)
        };
    };

    $scope.confirmSchedule = function ()
    {
        $scope.confirmStep = 2;

        function performEventConfirmation ()
        {
            $scope.loader.reset ();
            Event.confirm ($scope.loader.data.data.event ['_id']).then (
                function ()
                {
                    $scope.confirmStep = 3;
                    fetchEventData ();
                },
                function ()
                {
                    $scope.confirmStep = 4;
                    $scope.loader.promiseRejected ();
                }
            );
        }

        if ($rootScope.isTutor () && $scope.loader.data.data.event ['Ev105_Status_ac'] == 'Waiting tutor\'s confirmation')
        {
            performEventConfirmation ();
        }
        else if ($rootScope.isParent () && $scope.loader.data.data.event ['Ev105_Status_ac'] == 'Waiting parent\'s confirmation')
        {
            performEventConfirmation ();
        }
    };

    $scope.cancelSchedule = function ()
    {
        $scope.cancelStep = 2;

        function performEventCancellation ()
        {
            $scope.loader.reset ();
            Event.cancel ($scope.loader.data.data.event ['_id'], $scope.reject.reason ['Cnr100_Name']).then (
                function ()
                {
                    $scope.cancelStep = 3;
                    fetchEventData ();
                },
                function ()
                {
                    $scope.cancelStep = 4;
                    $scope.loader.promiseRejected ();
                }
            );
        }

        if ($rootScope.isTutor ())
        {
            performEventCancellation ();
        }
        else if ($rootScope.isParent ())
        {
            performEventCancellation ();
        }
    };

    $scope.rescheduleEvent = function ()
    {
        $rootScope.$broadcast ('OpenScheduleUpdate', $scope.loader.data.data.event ['_id']);
    };

    $scope.openCancel = function ()
    {
        let start = $scope.getUnixTime ($scope.loader.data.data.event ['Ev100_Date'], $scope.loader.data.data.event ['Ev101_StartTime']);
        $scope.reject.diff = start - moment ().utc ().unix ();
        $scope.reject.reason = '';
        $scope.cancelStep = 1;
        angular.element ('#cancel-booking').modal ('show');
    };

    $scope.openConfirm = function ()
    {
        $scope.confirmStep = 1;
        angular.element ('#accept-booking').modal ('show');
    };

    $scope.removeFile = function (file)
    {
        $scope.fileToRemove = file;
        angular.element ('#question-file').modal ('show');
    };

    $scope.doRemoveFile = function ()
    {
        $scope.loader.reset ();

        Event.removeFile ($scope.loader.data.data.event ['_id'], $scope.fileToRemove ['_id']).then (
            function ()
            {
                $scope.loader.queue (Event.getById ($routeParams.id));
            },
            function ()
            {
                $scope.loader.promiseRejected ();
            }
        );
    };

    $scope.topActionsSize = function ()
    {
        if (angular.isUndefined ($scope.loader.data.data) || angular.isUndefined ($scope.loader.data.data.event))
        {
            return {};
        }

        let classes = {};

        if (
            $scope.loader.data.data.event ['Ev105_Status_ac'] == 'Not confirmed' ||
            $scope.loader.data.data.event ['Ev105_Status_ac'] == 'Canceled within 24h'
        )
        {
            classes ['d-none'] = true;
        }
        else if ($rootScope.isParent ())
        {
            if ($scope.loader.data.data.event ['_id_Contract'] != '')
            {
                if ($scope.loader.data.data.event ['Ev105_Status_ac'] == 'Waiting parent\'s confirmation')
                {
                    classes ['col-md-3'] = true;
                }
                else
                {
                    classes ['col-md-4'] = true;
                }
            }
            else
            {
                if ($scope.loader.data.data.event ['Ev105_Status_ac'] != 'Waiting parent\'s confirmation')
                {
                    classes ['col-md-4'] = true;
                }
                else
                {
                    classes ['col-md-3'] = true;
                }
            }
        }
        else if ($rootScope.isTutor ())
        {
            if ($scope.loader.data.data.event ['Ev105_Status_ac'] != 'Waiting tutor\'s confirmation')
            {
                classes ['col-md-6'] = true;
            }
            else
            {
                classes ['col-md-4'] = true;
            }
        }

        return classes;
    };

    $scope.openNewNoteModal = function ()
    {
        $rootScope.$broadcast ('OpenCreateNewNote', $scope.loader.data.data.event);
    };

    $scope.openNewFileModal = function ()
    {
        $rootScope.$broadcast ('OpenFileUpload', $scope.loader.data.data.event);
    };

    /**
     * @param {string} timestring
     */
    function extractHour (timestring)
    {
        return parseInt (timestring.substring (0, timestring.indexOf (':')));
    }

    /**
     * @param {string} timestring
     */
    function extractMinutes (timestring)
    {
        return parseInt (timestring.substring (timestring.indexOf (':') + 1, timestring.indexOf (' ')));
    }

    /**
     * @param {string} date
     * @param {string} time
     */
    $scope.getUnixTime = function (date, time)
    {
        if (!time) return;
        if (!date) return;

        let timestring = time.toLowerCase ();
        let minutes = extractMinutes (timestring);

        if (timestring.indexOf ('am') !== false)
        {
            let am = extractHour (timestring);

            if (am == 12)
            {
                am = 0;
            }

            minutes += am * 60;
        }
        else if (timestring.indexOf ('pm') !== false)
        {
            let pm = extractHour (timestring);

            if (pm == 12)
            {
                pm = 0;
            }

            pm += 12;
            minutes += pm * 60;
        }
        else
        {
            minutes += extractHour (timestring) * 60;
        }

        // get date unix time
        return moment.utc (date, 'MM/DD/YYYY').unix () + (minutes * 60);
    };

    $scope.$on ('$destroy', function ()
    {
        unregisterListenToNewNote ();
        unregisterEnterFeedMode ();
        unregisterFileUploaded ();
        unregisterCalendarFilterChanged ();
    });
}]);

'use strict';

angular.module ('linkz.notifications', ['ui-notification'])
.config ([
'$routeProvider', 'NotificationProvider', 'PERM_PARENT', 'PERM_STUDENT', 'PERM_TUTOR',
function ($routeProvider, NotificationProvider, PERM_STUDENT, PERM_PARENT, PERM_TUTOR)
{
    NotificationProvider.setOptions ({
        delay: 10000,
        startTop: 20,
        startRight: 10,
        verticalSpacing: 20,
        horizontalSpacing: 20,
        positionX: 'right',
        positionY: 'top',
        templateUrl: 'app/modules/linkz/notifications/popup.html'
    });

    $routeProvider
    .when ('/notifications/', {
        base: 'app',
        templateUrl: 'app/modules/linkz/notifications/list.html',
        permissions: PERM_STUDENT | PERM_PARENT | PERM_TUTOR,
        controller: 'NotificationsController',
        menuEntry: 'notifications'
    });
}])
.directive ('notificationCount', [
'$interval', '$location', '$rootScope', 'Loader', 'Notification', 'Notifications',
function ($interval, $location, $rootScope, Loader, Notification, Notifications)
{
    return {
        restrict: 'E',
        template: '<span class="badge badge-danger" ng-show="count > 0"><loader size="small" loader="loader">{{ count }}</loader></span>',
        replace: false,
        link: function (scope)
        {
            scope.count = 0;
            scope.loader = Loader ();

            scope.loadPage = function ()
            {
                $location.path ('/notifications/');
            };

            function notifyFetching ()
            {
                $rootScope.$broadcast ('NotificationsFetched', scope.loader.data);
            }

            function fetchNotifications (force = false)
            {
                if (force == false && angular.isDefined (scope.loader.data.data))
                {
                    notifyFetching ();
                    return;
                }

                scope.loader.reset ();

                Notifications.get ().then (
                    function (result)
                    {
                        let count = 0;
                        let difference = 0;

                        angular.forEach (result.data, function (value)
                        {
                            if (value ['status'] == 'new') count ++;
                        });

                        difference = count - scope.count;

                        scope.loader.promiseResolved (result);
                        scope.count = count;

                        if (difference > 0 && $location.path () != '/notifications/')
                        {
                            // display a user notification top-right to give them a heads up
                            Notification ({
                                title: '<span class="fa fa-calendar"></span> You have ' + difference + ' new notification(s)',
                                message: 'Click here to go to your notifications page',
                                replaceMessage: true,
                                scope: scope
                            });
                        }

                        notifyFetching ();
                    },
                    function ()
                    {
                        scope.loader.promiseResolved ([]);
                        scope.count = 0;
                    }
                );
            }

            fetchNotifications ();

            let unregisterTimeout = $interval (function () { fetchNotifications (true); }, 1000 * 60);
            let unregisterScope = $rootScope.$on ('NotificationsUpdated', function ()
            {
                fetchNotifications (true);
            });
            let unregisterNotificationRequest = $rootScope.$on ('RequestNotifications', function ()
            {
                $rootScope.$broadcast ('NotificationsFetched', scope.loader.data);
            });

            scope.$on ('$destroy', function ()
            {
                unregisterTimeout ();
                unregisterScope ();
                unregisterNotificationRequest ();
            });
        }
    };
}])
.controller ('NotificationsController', [
'$location', '$rootScope', '$scope', 'Loader', 'Notifications',
function ($location, $rootScope, $scope, Loader, Notifications)
{
    $scope.loader = Loader ();

    $scope.gotoEvent = function (eventId, notificationId)
    {
        Notifications.markAsRead (notificationId).then (
            function ()
            {
                $rootScope.$broadcast ('NotificationsUpdated');
            }
        );

        $location.path ('/dashboard/details/' + eventId);
    };

    let unregisterFetch = $rootScope.$on ('NotificationsFetched', function (event, newNotifications)
    {
        $scope.loader.promiseResolved (newNotifications);
    });

    $rootScope.$broadcast ('RequestNotifications');

    $scope.$on ('$destroy', function ()
    {
        unregisterFetch ();
    });

    $scope.background = function (entry, $index)
    {
        let classes = {'col-md-6': $index != ($scope.loader.data.data.length - 1) || $index % 2 != 0};

        switch (entry ['message'])
        {
            case 'EVENT_CONFIRMED':
                classes ['bg-success text-light'] = entry ['status'] == 'new';
                classes ['text-dark'] = entry ['status'] != 'new';
                break;

            case 'EVENT_NOT_CONFIRMED':
                classes ['bg-danger text-light'] =  entry ['status'] == 'new';
                classes ['text-dark'] = entry ['status'] != 'new';
                break;

            case 'EVENT_WAITING_CONFIRMATION':
                classes ['bg-dark text-light'] = entry ['status'] == 'new';
                classes ['text-dark'] = entry ['status'] != 'new';
                break;

            case 'EVENT_UPDATE_REQUEST':
                classes ['bg-warning text-light'] = entry ['status'] == 'new';
                classes ['text-dark'] = entry ['status'] != 'new';
                break;
        }

        return classes;
    };
}]);

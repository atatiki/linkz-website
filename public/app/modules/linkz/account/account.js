'use strict';

angular.module ('linkz.account', [])
.config (['$routeProvider', 'PERM_PARENT', 'PERM_STUDENT', 'PERM_TUTOR', function ($routeProvider, PERM_PARENT, PERM_STUDENT, PERM_TUTOR)
{
    $routeProvider
    .when ('/account/', {
        controller: 'AccountController',
        templateUrl: 'app/modules/linkz/account/details.html',
        base: 'app',
        permissions: PERM_STUDENT | PERM_PARENT | PERM_TUTOR,
        menuEntry: 'account'
    });
}])
.controller ('AccountController', ['$rootScope', '$scope', 'Account', 'Authentication', 'Loader',
function ($rootScope, $scope, Account, Authentication, Loader)
{
    $scope.loader = Loader ([], 2);
    $scope.mode = 'show';
    $scope.tmp = {};

    function fetchData ()
    {
        $scope.loader.reset ();
        $scope.loader.queue (Account.getStatesList ());
        $scope.loader.queue (Account.get ());
    }

    fetchData ();

    $scope.switchMode = function ()
    {
        if ($scope.mode == 'show')
        {
            angular.extend ($scope.tmp, $scope.loader.data [1].data);
            $scope.mode = 'edit';
        }
        else
        {
            $scope.mode = 'show';
        }
    };

    $scope.isEdit = function ()
    {
        return $scope.mode == 'edit';
    };

    $scope.isShow = function ()
    {
        return $scope.mode == 'show';
    };

    $scope.saveChanges = function ()
    {
        angular.extend ($scope.loader.data [1].data, $scope.tmp);

        if ($rootScope.isTutor () === true)
        {
            let visitingStateName = '';
            let invoicingStateName = '';

            angular.forEach ($scope.loader.data [0].data, function (value)
            {
                if (value ['_id'] == $scope.tmp ['_id_StateVisiting'])
                {
                    visitingStateName = value ['Sa100_Name'];
                }

                if (value ['_id'] == $scope.tmp ['_id_StateInvoice'])
                {
                    invoicingStateName = value ['Sa100_Name'];
                }
            });

            $scope.loader.data [1].data ['Tu124_VisitingState'] = visitingStateName;
            $scope.loader.data [1].data ['Tu134_InvoiceState'] = invoicingStateName;
        }
        else if ($rootScope.isParent () === true)
        {
            let stateName = '';

            angular.forEach ($scope.loader.data [0].data, function (value)
            {
                if (value ['_id'] == $scope.tmp ['_id_StateVisiting'])
                {
                    stateName = value ['Sa100_Name'];
                }
            });

            $scope.loader.data [1].data ['Pe114_State'] = stateName;
        }

        $scope.loader.reset ();

        Account.update ($scope.tmp).then (
            function ()
            {
                fetchData ();
            },
            function ()
            {
                $scope.loader.promiseRejected ();
                $scope.loader.promiseRejected ();
            }
        );

        $scope.mode = 'show';
    };

    $scope.discardChanges = function ()
    {
        $scope.mode = 'show';
    };
}]);

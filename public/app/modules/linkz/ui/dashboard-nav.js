'use strict';

angular.module ('linkz.ui.dashboardnav', [])
.directive ('dashboardNav', ['$location', '$rootScope', '$sce', 'Authentication', 'Family', 'NavigationBar', 'PERM_PARENT', 'PERM_STUDENT', 'PERM_TUTOR',
function ($location, $rootScope, $sce, Authentication, Family, NavigationBar, PERM_PARENT, PERM_STUDENT, PERM_TUTOR)
{
    return {
        templateUrl: 'app/modules/linkz/ui/dashboard-nav.html',
        restrict: 'E',
        replace: true,
        link: function (scope)
        {
            scope.menu = {};
            scope.students = [];
            scope.checkedStudents = [];
            scope.filter = {cancelled: false};

            if (Authentication.perms.has (PERM_PARENT) === true)
            {
                scope.menu = angular.extend ({}, NavigationBar.get ('parent_dashboard'));

                // also prepare the extra options for the menu
                Family.get ().then (
                    function (result)
                    {
                        angular.forEach (result.data.students, function (value)
                        {
                            scope.students.push ({
                                _id: value ['_id'],
                                name: value ['St100_FirstName'] + ' ' + value ['St101_LastName'],
                                active: true,
                                custom: true
                            });
                        });
                    }
                );
            }
            else if (Authentication.perms.has (PERM_STUDENT) === true)
            {
                scope.menu = angular.extend ({}, NavigationBar.get ('student_dashboard'));
            }
            else if (Authentication.perms.has (PERM_TUTOR) === true)
            {
                scope.menu = angular.extend ({}, NavigationBar.get ('tutor_dashboard'));
            }

            scope.navigate = function (entry)
            {
                if (angular.isFunction (entry.link))
                {
                    entry.link ();
                }
                else
                {
                    $location.path (entry.link);
                }
            };

            scope.change = function (entry)
            {
                entry.active = !entry.active;

                $rootScope.$broadcast ('StudentFilterChanged', scope.students);
            };

            scope.switchCancelledFilter = function ()
            {
                scope.filter.cancelled = !scope.filter.cancelled;

                $rootScope.$broadcast ('CancelledFilterChanged', scope.filter.cancelled);
            };

            $rootScope.$on ('EnterFeedMode', function ()
            {
                $rootScope.$broadcast ('CancelledFilterChanged', scope.filter.cancelled);

                if (scope.students.length > 0)
                    $rootScope.$broadcast ('StudentFilterChanged', scope.students);
            });

            $rootScope.$on ('FetchCalendarDate', function ()
            {
                $rootScope.$broadcast ('CancelledFilterChanged', scope.filter.cancelled);

                if (scope.students.length > 0)
                    $rootScope.$broadcast ('StudentFilterChanged', scope.students);
            });

            scope.trust = function (html)
            {
                return $sce.trustAsHtml (html);
            };

            angular.element ('#studentsNavigation').on ('show.bs.collapse', function ()
            {
                angular.element ('#displayArrow').addClass ('active');
            });

            angular.element ('#studentsNavigation').on ('hide.bs.collapse', function ()
            {
                angular.element ('#displayArrow').removeClass ('active');
            });
        }
    };
}]);

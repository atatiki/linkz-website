'use strict';

angular.module ('linkz.ui.topbar', [])
.directive ('topNavBar', ['$location', '$sce', 'Authentication', 'NavigationBar', 'PERM_PARENT', 'PERM_STUDENT', 'PERM_TUTOR',
function ($location, $sce, Authentication, NavigationBar, PERM_PARENT, PERM_STUDENT, PERM_TUTOR)
{
    return {
        templateUrl: 'app/modules/linkz/ui/topbar.html',
        restrict: 'E',
        replace: true,
        link: function (scope)
        {
            scope.menu = {};

            if (Authentication.perms.has (PERM_PARENT) === true)
            {
                scope.menu = NavigationBar.get ('parent');
            }
            else if (Authentication.perms.has (PERM_STUDENT) === true)
            {
                scope.menu = NavigationBar.get ('student');
            }
            else if (Authentication.perms.has (PERM_TUTOR) === true)
            {
                scope.menu = NavigationBar.get ('tutor');
            }

            scope.navigate = function (entry)
            {
                $location.path (entry.link);
            };

            scope.trust = function (html)
            {
                return $sce.trustAsHtml (html);
            };
        }
    };
}]);

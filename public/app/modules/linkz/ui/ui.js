'use strict';

angular.module ('linkz.ui', ['linkz.ui.leftbar', 'linkz.ui.dashboardnav', 'linkz.ui.templating', 'linkz.ui.topbar'])
.directive ('sectionTitle', [function ()
{
    return {
        template: '<div class="row"><div class="col-12 text-center"><h2 class="border-bottom" ng-transclude></h2></div></div>',
        restrict: 'E',
        replace: true,
        transclude: true
    };
}])
.directive ('sectionContent', [function ()
{
    return {
        template: '<div class="row" ng-transclude></div>',
        restrict: 'E',
        replace: true,
        transclude: true
    };
}])
.directive ('sectionContainer', [function ()
{
    return {
        template: '<div class="container container-top mt-2 p-0 pb-2" ng-transclude></div>',
        restrict: 'E',
        replace: true,
        transclude: true
    };
}]);

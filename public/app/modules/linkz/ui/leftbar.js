'use strict';

angular.module ('linkz.ui.leftbar', ['atatiki.logging'])
.run (['NavigationBar', function (NavigationBar)
{
    NavigationBar.init ();
}])
.provider ('NavigationBar', ['LoggerProvider', function (LoggerProvider)
{
    const logger = LoggerProvider.get ('NavigationBar');
    var registeredMenus = {};

    return {
        $get: ['$rootScope', function ($rootScope)
        {
            var self = this;

            this.init = function ()
            {
                //eslint-disable-next-line angular/on-watch
                $rootScope.$on ('$routeChangeSuccess', function (event, current)
                {
                    if (current && current.$$route && current.$$route.menuEntry)
                    {
                        angular.forEach (registeredMenus, function (menuData, menuName)
                        {
                            if (angular.isDefined (registeredMenus [menuName] [current.$$route.menuEntry]))
                                self.setMenuEntryActive (menuName, current.$$route.menuEntry);
                        });
                    }
                });
            };

            return this;
        }],
        /**
         * Registers a new menu to be used by the application
         *
         * @param {string} menuName The internal name for the menu
         */
        registerMenu: function (menuName)
        {
            registeredMenus [menuName] = {};
        },
        /**
         * Registers a new menu entry for the application
         *
         * @param {string} menuName The name of the menu to register the entry in
         * @param {string} entryName The internal name used to refer to this entry
         * @param {string|function} entryUrl The url to change when clicked
         * @param {string} entryText Text to be translated by the system
         * @param {string} entryIcon The FontAwesome icon (without the fa-) to display
         * @param {string} entryHtml The entry HTML to display
         * @return {*}
         */
        registerMenuEntry: function (menuName, entryName, entryUrl, entryText, entryIcon = '', entryHtml = '')
        {
            if (!registeredMenus [menuName])
            {
                return logger.error ('Registering a menu item without a registered menu first...');
            }

            if (registeredMenus [menuName] [entryName])
            {
                logger.info ('Replacing menu entry ' + entryName + ' on menu ' + menuName);
            }

            entryIcon = 'fa-' + entryIcon;
            entryText = 'MENU.' + entryText.toUpperCase ();

            registeredMenus [menuName] [entryName] = {
                name: entryName,
                html: entryHtml,
                text: entryText,
                icon: ['fa', entryIcon],
                link: entryUrl,
                active: false
            };
        },
        setMenuEntryActive: function (menuName, entryName)
        {
            if (!registeredMenus [menuName])
            {
                return logger.error ('Marking as active an entry in a menu that doesn\'t exist');
            }

            if (!registeredMenus [menuName] [entryName])
            {
                return logger.error ('Marking as active an entry that doesn\'t exist');
            }

            angular.forEach (registeredMenus [menuName], function (entry)
            {
                if (entry.name == entryName)
                {
                    entry.active = true;
                }
                else
                {
                    entry.active = false;
                }
            });
        },
        /**
         * Returns a reference to the given menu
         *
         * @param {string} menuName
         */
        get: function (menuName)
        {
            if (!registeredMenus [menuName])
            {
                logger.error ('Trying to obtain an unregistered menu: ' + menuName);
                return null;
            }

            return registeredMenus [menuName];
        }
    };
}])
.directive ('leftNavBar', [
'$location', '$sce', 'Authentication', 'NavigationBar', 'PERM_PARENT', 'PERM_STUDENT', 'PERM_TUTOR',
function ($location, $sce, Authentication, NavigationBar, PERM_PARENT, PERM_STUDENT, PERM_TUTOR)
{
    return {
        replace: true,
        restrict: 'E',
        templateUrl: 'app/modules/linkz/ui/leftbar.html',
        link: function (scope)
        {
            scope.menu = {};

            if (Authentication.perms.has (PERM_PARENT) === true)
            {
                scope.menu = NavigationBar.get ('parent');
            }
            else if (Authentication.perms.has (PERM_STUDENT) === true)
            {
                scope.menu = NavigationBar.get ('student');
            }
            else if (Authentication.perms.has (PERM_TUTOR) === true)
            {
                scope.menu = NavigationBar.get ('tutor');
            }

            scope.navigate = function (entry)
            {
                if (angular.isFunction (entry.link))
                {
                    entry.link ();
                }
                else
                {
                    $location.path (entry.link);
                }
            };

            scope.trust = function (html)
            {
                return $sce.trustAsHtml (html);
            };
        }
    };
}])
.directive('initBind', ['$compile', function ($compile)
{
    return {
        restrict: 'A',
        link : function (scope, element, attr)
        {
            attr.$observe ('ngBindHtml',function ()
            {
                if (attr.ngBindHtml)
                {
                    $compile (element [0].children) (scope);
                }
            });
        }
    };
}]);

'use strict';

angular.module ('linkz.ui.templating', [])
.run (['Templating', function (Templating)
{
    Templating.registerTemplate ('app', 'app/template/app.html', '');
    Templating.registerTemplate ('login', 'app/template/login.html', '');
    Templating.registerTemplate ('dashboard', 'app/template/dashboard.html', 'app');
}]);

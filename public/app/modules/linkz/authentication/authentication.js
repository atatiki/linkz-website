'use strict';

angular.module ('linkz.authentication', [])
.constant ('PERM_GUEST', 1)
.constant ('PERM_STUDENT', 1 << 1)
.constant ('PERM_TUTOR', 1 << 2)
.constant ('PERM_PARENT', 1 << 3)
.service ('Authentication', [
'$http', '$injector', '$localStorage', '$location', '$q', '$rootScope', 'API', 'Logger', 'PERM_GUEST', 'PERM_PARENT', 'PERM_STUDENT', 'PERM_TUTOR', 'moment',
function ($http, $injector, $localStorage, $location, $q, $rootScope, API, Logger, PERM_GUEST, PERM_PARENT, PERM_STUDENT, PERM_TUTOR, moment)
{
    const logger = Logger.get ('AuthenticationSystem');
    /** @type {int} Permission map for the current authentication level */
    var permissions = PERM_GUEST; // all users are guest by default

    if ($localStorage.auth && $localStorage.auth.permissions && $localStorage.auth.token)
    {
        permissions = $localStorage.auth.permissions;
        // set http header too
        $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.auth.token;
    }

    return {
        init: function ()
        {
            //eslint-disable-next-line angular/on-watch
            $rootScope.$on ('AuthenticationError', function ()
            {
                $location.path ('/login/');
            });

            //eslint-disable-next-line angular/on-watch
            $rootScope.$on ('$routeChangeStart', function (event, next)
            {
                if ('$$route' in next && 'permissions' in next.$$route)
                {
                    // force promise on the needed routes
                    if ('resolve' in next.$$route === false)
                    {
                        next.$$route.resolve = {};
                    }

                    // use some weird, random resolve name to prevent clashing
                    next.$$route.resolve ['linkzAuthenticationResolve'] = ['$q', 'Authentication', function ($q, Authentication)
                    {
                        return $q (function (resolve, reject)
                        {
                            if (Authentication.perms.has (next.$$route.permissions) === false)
                            {
                                logger.error ('User tried to navigate to a protected area');
                                reject ('AuthNotAllowed');
                            }
                            else
                            {
                                logger.debug ('User has enough permissions to navigate to ' + next.$$route.originalPath);
                                resolve ();
                            }
                        });
                    }];
                }
            });

            //eslint-disable-next-line angular/on-watch
            $rootScope.$on ('$routeChangeError', function (event, current, previous, rejection)
            {
                const logger = Logger.get ('RouteChange');

                // main route change error handler
                if (rejection === 'AuthNotAllowed')
                {
                    $location.path ('/login/');
                }
                else if (rejection === 'AlreadyLoggedIn')
                {
                    $location.path ('/dashboard/');
                }
                else
                {
                    logger.error ('Unknown route change error ' + rejection);
                    $location.path ('/error/');
                }
            });

            // registers functions for the views to change based on the kind of user
            // we are
            $rootScope.isTutor = function ()
            {
                return $injector.invoke (['Authentication', function (Authentication)
                {
                    return Authentication.perms.has (PERM_TUTOR);
                }]);
            };

            $rootScope.isParent = function ()
            {
                return $injector.invoke (['Authentication', function (Authentication)
                {
                    return Authentication.perms.has (PERM_PARENT);
                }]);
            };

            $rootScope.isStudent = function ()
            {
                return $injector.invoke (['Authentication', function (Authentication)
                {
                    return Authentication.perms.has (PERM_STUDENT);
                }]);
            };
        },
        perms: {
            has: function (permissionToCheck)
            {
                return (permissions & permissionToCheck) !== 0;
            },
            set: function (permissionToSet)
            {
                permissions |= permissionToSet;
            },
            clear: function (permissionToClear)
            {
                permissions &= ~permissionToClear;
            },
            get: function ()
            {
                return permissions;
            }
        },
        login: function (username, password)
        {
            let self = this;
            let authHeader = 'Basic ' + btoa (encodeURIComponent (username) + ':' + encodeURIComponent(password));

            // make sure the permission flags are cleared
            self.perms.clear (PERM_PARENT);
            self.perms.clear (PERM_STUDENT);
            self.perms.clear (PERM_TUTOR);
            self.perms.set (PERM_GUEST);

            return $http.post (API ('login'), {}, {headers: {'Authorization': authHeader}}).then (
                function (response)
                {
                    switch (response.data.type)
                    {
                        case 'parent':
                            self.perms.set (PERM_PARENT);
                            break;

                        case 'student':
                            self.perms.set (PERM_STUDENT);
                            break;

                        case 'tutor':
                            self.perms.set (PERM_TUTOR);
                            break;

                        default:
                            logger.error ('Unknown user type ' + response.data.type);
                            return $q.reject ();
                    }

                    // store the session data (so we can restore it later)
                    $localStorage.auth = {expiretime: response.data.expiretime, permissions: self.perms.get (), token: response.data.token};

                    // add the common authorization header
                    $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.auth.token;

                    // emit login event
                    $rootScope.$broadcast ('LoginPerformed');
                },
                function ()
                {
                    // emit login failed event
                    $rootScope.$broadcast ('LoginFailed');
                    return $q.reject ();
                }
            );
        },
        logout: function ()
        {
            var self = this;

            return $http.get (API ('logout')).then (
                function ()
                {
                    // remove default authorization header
                    $http.defaults.headers.common.Authorization = undefined;

                    self.perms.clear (PERM_PARENT);
                    self.perms.clear (PERM_STUDENT);
                    self.perms.clear (PERM_TUTOR);
                    self.perms.set (PERM_GUEST);

                    $localStorage.auth = {permissions: self.perms.get (), token: '', expiretime: 0};
                    $rootScope.$broadcast ('LogoutPerformed');
                },
                function ()
                {
                    $rootScope.$broadcast ('LogoutFailed');
                }
            );
        },
        isLoggedIn: function ()
        {
            if (angular.isDefined ($localStorage.auth) == false)
                return false;

            if (angular.isDefined ($localStorage.auth.expiretime) == false)
                return false;

            if (angular.isDefined ($localStorage.auth.token) == false)
                return false;

            if ($localStorage.auth.token == '')
                return false;

            if (moment.unix ($localStorage.auth.expiretime).isAfter (moment ()) == false)
                return false;

            return true;
        },
        getToken: function ()
        {
            if (this.isLoggedIn ())
            {
                return $localStorage.auth.token;
            }
            else
            {
                return '';
            }
        }
    };
}])
.config ([
'$routeProvider', 'PERM_GUEST', 'PERM_PARENT', 'PERM_STUDENT', 'PERM_TUTOR',
function ($routeProvider, PERM_GUEST, PERM_PARENT, PERM_STUDENT, PERM_TUTOR)
{
    $routeProvider
    .when ('/login/', {
        base: 'login',
        templateUrl: 'app/modules/linkz/authentication/login.html',
        controller: 'LoginController',
        permissions: PERM_GUEST,
        resolve: {
            isLoggedIn: function ($q, Authentication)
            {
                return $q (function (resolve, reject)
                {
                    if (Authentication.isLoggedIn () === true)
                    {
                        reject ('AlreadyLoggedIn');
                    }
                    else
                    {
                        resolve ();
                    }
                });
            }
        }
    })
    .when ('/logout/', {
        base: 'login',
        templateUrl: 'app/modules/linkz/authentication/logout.html',
        controller: 'LogoutController',
        permissions: PERM_TUTOR | PERM_STUDENT | PERM_PARENT
    });
}])
.run (['Authentication', function (Authentication)
{
    Authentication.init ();
}])
.controller ('LoginController', ['$location', '$scope', 'Authentication', 'Logger', function ($location, $scope, Authentication, Logger)
{
    const logger = Logger.get ('LoginPage');

    //eslint-disable-next-line no-console
    console.dir ('LOGIN!');

    $scope.errorOccurred = false;
    $scope.userinfo = {username: '', password: ''};

    $scope.doLogin = function ()
    {
        Authentication.login ($scope.userinfo.username, $scope.userinfo.password).then (
            function ()
            {
                logger.debug ('User ' + $scope.userinfo.username + ' logged in correctly');
                $location.path ('/dashboard/');
            },
            function ()
            {
                logger.error ('Error logging in user ' + $scope.userinfo.username);
                $scope.errorOccurred = true;
            }
        );
    };
}])
.controller ('LogoutController', ['$location', 'Authentication', function ($location, Authentication)
{
    //eslint-disable-next-line no-console
    console.dir ('LOGOUT!');

    Authentication.logout ().then (
        function ()
        {
            $location.path ('/login/');
        }
    );
}]);

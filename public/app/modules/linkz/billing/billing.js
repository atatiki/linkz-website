'use strict';

angular.module ('linkz.billing', [])
.config (['$routeProvider', 'PERM_PARENT', function ($routeProvider, PERM_PARENT)
{
    $routeProvider
    .when ('/contracts/', {
        controller: 'ContractController',
        templateUrl: 'app/modules/linkz/billing/contracts/list.html',
        base: 'app',
        menuEntry: 'billing',
        permissions: PERM_PARENT
    })
    .when ('/contracts/:contractId/details/', {
        controller: 'ContractDetailsController',
        templateUrl: 'app/modules/linkz/billing/contracts/details.html',
        base: 'app',
        menuEntry: 'billing',
        permissions: PERM_PARENT
    })
    .when ('/contracts/:contractId/invoice/:invoiceId/', {
        controller: 'InvoiceDetailsController',
        templateUrl: 'app/modules/linkz/billing/contracts/invoice.html',
        base: 'app',
        permissions: PERM_PARENT
    });
}])
.controller ('ContractController', ['$location', '$scope', 'Contracts', 'Loader',
function ($location, $scope, Contracts, Loader)
{
    $scope.loader = Loader ();
    $scope.page = 1;

    function request ()
    {
        $scope.loader.reset ();
        $scope.loader.queue (Contracts.get ($scope.page));
    }

    request ();

    $scope.goto = function (page)
    {
        $scope.page = page;
        request ();

        return true;
    };

    $scope.details = function (entry)
    {
        $location.path ('/contracts/' + entry ['_id'] + '/details/');
    };
}])
.controller ('ContractDetailsController', ['$location', '$routeParams', '$scope', 'Contracts', 'Loader',
function ($location, $routeParams, $scope, Contracts, Loader)
{
    $scope.loader = Loader ();
    $scope.loader.queue (Contracts.getById ($routeParams.contractId));
    $scope.invoiceDetails = function (entry)
    {
        $location.path ('/contracts/' + $routeParams.contractId + '/invoice/' + entry ['_id'] + '/');
    };
    $scope.goback = function ()
    {
        $location.path ('/contracts/');
    };
}])
.controller ('InvoiceDetailsController', ['$location', '$routeParams', '$scope', 'API', 'Authentication', 'Invoices', 'Loader',
function ($location, $routeParams, $scope, API, Authentication, Invoices, Loader)
{
    $scope.loader = Loader ();
    $scope.loader.queue (Invoices.getRows ($routeParams.contractId, $routeParams.invoiceId));
    $scope.downloadUrl = API ('invoices/' + $routeParams.invoiceId + '/download') + '?token=' + encodeURIComponent (Authentication.getToken ());

    $scope.goback = function ()
    {
        $location.path ('/contracts/' + $routeParams.contractId + '/details/');
    };
}]);

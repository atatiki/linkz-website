'use strict';

angular.module ('linkz.api', [])
.service ('API', ['ApplicationConfiguration', function (ApplicationConfiguration)
{
    return function (url)
    {
        // build the URL
        let config = ApplicationConfiguration.get ().api;

        return config.protocol + '://' + config.server + ':' + config.port + '/' + config.env + '/' + url;
    };
}])
.filter ('bridge', ['API', 'Authentication', function (API, Authentication)
{
    return function (input)
    {
        return API ('bridge') + '?url=' + encodeURIComponent (input) + '&token=' + encodeURIComponent (Authentication.getToken ());
    };
}]);

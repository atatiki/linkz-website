'use strict';

angular.module ('linkz.services.family', [])
.service ('Family', ['$http', '$q', 'API', function ($http, $q, API)
{
    return {
        get: function ()
        {
            return $http.get (API ('family')).then (
                function (result)
                {
                    return result;
                },
                function ()
                {
                    return $q.reject ();
                }
            );
        }
    };
}]);

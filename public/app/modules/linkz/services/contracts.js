'use strict';

angular.module ('linkz.services.contracts', [])
.service ('Contracts', ['$http', '$q', 'API', function ($http, $q, API)
{
    return {
        get: function (page = 1, ignoreEmpty = 0)
        {
            return $http.get (API('contracts') + '?page=' + page + '&ignoreempty=' + ignoreEmpty).then (
                function (result)
                {
                    return result;
                },
                function ()
                {
                    return $q.reject ();
                }
            );
        },
        getById: function (contractId)
        {
            return $http.get (API ('contracts/' + contractId)).then (
                function (result)
                {
                    return result;
                },
                function ()
                {
                    return $q.reject ();
                }
            );
        }
    };
}])
.service ('Invoices', ['$http', '$q', 'API', function ($http, $q, API)
{
    return {
        get: function (contractId)
        {
            return $http.get (API ('contracts/' + contractId + '/invoices/')).then (
                function (result)
                {
                    return result;
                },
                function ()
                {
                    return $q.reject ();
                }
            );
        },
        getRows: function (contractId, invoiceId)
        {
            return $http.get (API ('contracts/' + contractId + '/invoice/' + invoiceId));
        }
    };
}]);

'use strict';

angular.module ('linkz.services.account', [])
.service ('Account', ['$http', '$q', 'API', function ($http, $q, API)
{
    return {
        get: function ()
        {
            return $http.get (API ('user')).then (
                function (result)
                {
                    return result;
                },
                function ()
                {
                    return $q.reject ();
                }
            );
        },
        getStatesList: function ()
        {
            return $http.get (API ('states')).then (
                function (result)
                {
                    return result;
                },
                function ()
                {
                    return $q.reject ();
                }
            );
        },
        getSubjectsList: function ()
        {
            return $http.get (API ('subjects')).then (
                function (result)
                {
                    return result;
                },
                function ()
                {
                    return $q.reject ();
                }
            );
        },
        update: function (data)
        {
            return $http.post (API ('user'), data).then (
                function (result)
                {
                    return result;
                },
                function ()
                {
                    return $q.reject ();
                }
            );
        }
    };
}]);

'use strict';

angular.module ('linkz.services.event', [])
.service ('Event', ['$http', '$q', 'API', 'moment', function ($http, $q, API, moment)
{
    return {
        get: function (date = moment (), page = 1, ignoreStudents = [], showCancelled = false)
        {
            let url = API ('events') + '?date=' + encodeURIComponent (date.format ());

            angular.forEach (ignoreStudents, function (value)
            {
                url += '&ignoreStudents[]=' + value ['_id'];
            });

            url += '&page=' + page;
            url += '&showCancelled=' + showCancelled;

            return $http.get (url).then (
                function (result)
                {
                    return result;
                },
                function ()
                {
                    return $q.reject ();
                }
            );
        },
        feed: function (page = 1, ignoreStudents = [], showCancelled = false)
        {
            let url = API ('events/feed') + '?page=' + page;

            angular.forEach (ignoreStudents, function (value)
            {
                url += '&ignoreStudents[]=' + value ['_id'];
            });

            url += '&showCancelled=' + showCancelled;

            return $http.get (url).then (
                function (result)
                {
                    return result;
                },
                function ()
                {
                    return $q.reject ();
                }
            );
        },
        getById: function (eventId)
        {
            return $http.get (API ('event/' + eventId + '/details')).then (
                function (result)
                {
                    return result;
                },
                function ()
                {
                    return $q.reject ();
                }
            );
        },
        createNote: function (eventId, note, visibleToParent = 1, visibleToStudent = 1)
        {
            return $http.post (API ('notes/create/' + eventId), {
                'note': note,
                'visibleToParent': visibleToParent,
                'visibleToStudent': visibleToStudent
            }).then (
                function (result)
                {
                    return result;
                },
                function ()
                {
                    return $q.reject ();
                }
            );
        },
        confirm: function (eventId)
        {
            return $http.post (API ('event/' + eventId + '/confirm')).then (
                function (result)
                {
                    return result;
                },
                function ()
                {
                    return $q.reject ();
                }
            );
        },
        cancel: function (eventId, reason)
        {
            return $http.post (API ('event/' + eventId + '/cancel'), {'reason': reason}).then (
                function (result)
                {
                    return result;
                },
                function ()
                {
                    return $q.reject ();
                }
            );
        },
        create: function (eventStart, eventEnd, contractId, subject, description)
        {
            // WARNING: THIS SHOULD BE STANDARIZED AND THE TYPICAL TALK PEOPLE AGAINST JAVASCRIPT HAS
            // THIS IS NOT A GOOD SOLUTION, BUT GETS THE JOB DONE AND IS ONLY USED HERE, NOWHERE ELSE
            function strleftzero (value)
            {
                let str = '' + value;

                while (str.length < 2)
                {
                    str = '0' + str;
                }

                return str;
            }

            return $http.post (API ('events'), {
                'date': strleftzero (eventStart.getMonth () + 1) + '/' + strleftzero (eventStart.getDate ()) + '/' + eventStart.getFullYear (),
                'contractId': contractId,
                'starttime': strleftzero (eventStart.getHours ()) + ':' + strleftzero (eventStart.getMinutes ()),
                'endtime': strleftzero (eventEnd.getHours ()) + ':' + strleftzero (eventEnd.getMinutes ()),
                'subject': subject,
                'description': description
            }).then (
                function (result)
                {
                    return result;
                },
                function ()
                {
                    return $q.reject ();
                }
            );
        },
        update: function (eventId, eventStart, eventEnd)
        {
            // WARNING: THIS SHOULD BE STANDARIZED AND THE TYPICAL TALK PEOPLE AGAINST JAVASCRIPT HAS
            // THIS IS NOT A GOOD SOLUTION, BUT GETS THE JOB DONE AND IS ONLY USED HERE, NOWHERE ELSE
            function strleftzero (value)
            {
                let str = '' + value;

                while (str.length < 2)
                {
                    str = '0' + str;
                }

                return str;
            }

            return $http.patch (API ('events'), {
                'date': strleftzero (eventStart.getMonth () + 1) + '/' + strleftzero (eventStart.getDate ()) + '/' + eventStart.getFullYear (),
                'eventId': eventId,
                'starttime': strleftzero (eventStart.getHours ()) + ':' + strleftzero (eventStart.getMinutes ()),
                'endtime': strleftzero (eventEnd.getHours ()) + ':' + strleftzero (eventEnd.getMinutes ())
            }).then (
                function (result)
                {
                    return result;
                },
                function ()
                {
                    return $q.reject ();
                }
            );
        },
        removeFile: function (eventId, fileId)
        {
            return $http.post (API ('event/' + eventId + '/file/' + fileId + '/remove')).then (
                function (result)
                {
                    return result;
                },
                function ()
                {
                    return $q.reject ();
                }
            );
        },
        heatmap: function (date)
        {
            return $http.get (API ('events/heatmap') + '?date=' + encodeURIComponent (date.format ())).then (
                function (result)
                {
                    return result;
                },
                function ()
                {
                    return $q.reject ();
                }
            )
        },
        getCancellationReasons: function ()
        {
            return $http.get (API ('reasons')).then (
                function (result)
                {
                    return result;
                },
                function ()
                {
                    return $q.reject ();
                }
            );
        }
    };
}]);

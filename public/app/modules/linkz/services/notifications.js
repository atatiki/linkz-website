'use strict';

angular.module ('linkz.services.notifications', [])
.service ('Notifications', ['$http', '$q', 'API', function ($http, $q, API)
{
    return {
        get: function ()
        {
            return $http.get (API ('notifications')).then (
                function (result)
                {
                    return result;
                },
                function ()
                {
                    return $q.reject ();
                }
            );
        },
        markAsRead: function (notificationId)
        {
            return $http.post (API ('notifications/' + notificationId + '/markAsRead')).then (
                function (result)
                {
                    return result;
                },
                function ()
                {
                    return $q.reject ();
                }
            );
        }
    };
}]);

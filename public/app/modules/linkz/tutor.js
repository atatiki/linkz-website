'use strict';

angular.module ('linkz.tutor', [])
.config (['NavigationBarProvider', function (NavigationBarProvider)
{
    NavigationBarProvider.registerMenu ('tutor');
    NavigationBarProvider.registerMenuEntry ('tutor', 'home', '/dashboard/home/feed/', 'home', 'home');
    NavigationBarProvider.registerMenuEntry ('tutor', 'notifications', '/notifications/', 'notifications', 'bell', '<notification-count></notification-count>');
    // NavigationBarProvider.registerMenuEntry ('tutor', 'messages', '/messages/', 'messages', 'envelope', '<message-count></message-count>');
    NavigationBarProvider.registerMenuEntry ('tutor', 'account', '/account/', 'account', 'user');
    NavigationBarProvider.registerMenuEntry ('tutor', 'singout', '/logout/', 'logout', 'sign-out-alt');
}]);

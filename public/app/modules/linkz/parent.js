'use strict';

angular.module ('linkz.parent', [])
.config (['NavigationBarProvider', function (NavigationBarProvider)
{
    NavigationBarProvider.registerMenu ('parent');
    NavigationBarProvider.registerMenuEntry ('parent', 'home', '/dashboard/home/feed/', 'home', 'home');
    NavigationBarProvider.registerMenuEntry ('parent', 'billing', '/contracts/', 'billing', 'file-invoice');
    NavigationBarProvider.registerMenuEntry ('parent', 'notifications', '/notifications/', 'notifications', 'bell', '<notification-count></notification-count>');
    // NavigationBarProvider.registerMenuEntry ('parent', 'messages', '/messages/', 'messages', 'envelope', '<message-count></message-count>');
    NavigationBarProvider.registerMenuEntry ('parent', 'account', '/account/', 'account', 'user');
    NavigationBarProvider.registerMenuEntry ('parent', 'singout', '/logout/', 'logout', 'sign-out-alt');
}]);

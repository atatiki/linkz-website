'use strict';

angular.module ('linkz.services', [
    'linkz.services.event',
    'linkz.services.family',
    'linkz.services.contracts',
    'linkz.services.account',
    'linkz.services.notifications'
]);

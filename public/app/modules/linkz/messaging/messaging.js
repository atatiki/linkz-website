'use strict';

angular.module ('linkz.messaging', [])
.config (['$routeProvider', 'PERM_PARENT', 'PERM_STUDENT', 'PERM_TUTOR', function ($routeProvider, PERM_PARENT, PERM_STUDENT, PERM_TUTOR)
{
    $routeProvider
    .when ('/messages/', {
        base: 'app',
        templateUrl: 'app/modules/linkz/messaging/list.html',
        permissions: PERM_STUDENT | PERM_PARENT | PERM_TUTOR,
        controller: 'MessagingController',
        menuEntry: 'messages'
    });
}])
.directive ('messageCount', ['$timeout', 'Loader', function ($timeout, Loader)
{
    return {
        restrict: 'E',
        template: '<span class="badge badge-danger" ng-show="count > 0"><loader size="small" loader="loader">{{ count }}</loader></span>',
        replace: false,
        link: function (scope)
        {
            scope.count = 1;
            scope.loader = Loader ();
            scope.loader.updateStatus ();

            // delay loading of the quantity as an example
            $timeout (function ()
            {
                scope.loader.promiseResolved ('0');
            }, 2000);
        }
    };
}])
.directive ('contactList', function ()
{
    return {
        replace: true,
        restrict: 'E',
        templateUrl: 'app/modules/linkz/messaging/contactlist.html',
        link: function (scope)
        {

        }
    };
})
.directive ('chatContent', function ()
{
    return {
        replace: true,
        restrict: 'E',
        templateUrl: 'app/modules/linkz/messaging/chat.html',
        link: function (scope)
        {

        }
    };
})
.controller ('MessagingController', ['$scope', 'Loader', function ($scope, Loader)
{
    $scope.loader = Loader ();
    $scope.loader.status = 'ok';
}]);

/// TODO: IMPLEMENT HANDLING LoginPerformed and LogoutPerformed EVENTS HERE TO PERFORM DATA FETCHING

'use strict';

angular.module ('linkz.student', [])
.config (['NavigationBarProvider', function (NavigationBarProvider)
{
    NavigationBarProvider.registerMenu ('student');
    NavigationBarProvider.registerMenuEntry ('student', 'home', '/dashboard/home/feed/', 'home', 'home');
    NavigationBarProvider.registerMenuEntry ('student', 'notifications', '/notifications/', 'notifications', 'bell', '<notification-count></notification-count>');
    // NavigationBarProvider.registerMenuEntry ('student', 'messages', '/messages/', 'messages', 'envelope', '<message-count></message-count>');
    NavigationBarProvider.registerMenuEntry ('student', 'account', '/account/', 'account', 'user');
    NavigationBarProvider.registerMenuEntry ('student', 'singout', '/logout/', 'logout', 'sign-out-alt');
}]);

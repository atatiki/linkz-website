# Deploying system

## Pre-requirements
 - PHP 7.3 or greater
 - Redis server

## Setting up configuration
In order for the system to properly work we need to set some environment variables for the system to run. There is a
simple approach that just needs a file named ```.env``` to be created on the project root but it is discouraged
as It can be a security concern in the long run. The best option is to set the environment variables in the server
itself, either in the PHP configuration or on the environment itself (like Windows allows).

These are the environment variables needed for the system to work properly:
```dotenv
API_SERVER=online.linkzedu.com
API_PORT=443
API_ENV=api/test.php
API_PROTOCOL=https
PROJECT_PATH=/home/almamu/Development/linkz/linkz-website/
RESTFM_SERVER=dev1fms16.atatiki.com
RESTFM_DATABASE=Linkz_Web
RESTFM_KEY=FeL3w4EuRJBhBF7GK6s7feDH8jC5m5rH
FM_USERNAME=utvecklare
FM_PASSWORD=Temp1
FM_SERVER=dev1fms16.atatiki.com
FM_PROTOCOL=http
FILE_URL_BASE=http://office.atatiki.es:8080/tmpdata/
FILE_URL_PATH=${PROJECT_PATH}/public/tmpdata/
LOGS_PATH=${PROJECT_PATH}/logs/
```
**NOTE: IF THE ```.env``` APPROACH IS USED THE API_ENV VARIABLE MUST HAVE THE VALUE ```api/test.php``` for the file to be
properly loaded**

###### API_SERVER
Hostname where the application is going to be stored
###### API_PORT
Port to use for web requests
###### API_ENV
The API endpoint to use, possible values:

```api/test.php```: used for development and .env file loading

```api/index.php```: used for production
###### API_PROTOCOL
The protocol to use for the web requests, https is advised
###### PROJECT_PATH
Full path to the root of the project
###### RESTFM_SERVER
Hostname where the RESTfm api is stored to access the FileMaker Database
###### RESTFM_DATABASE
The name of the FileMaker database to connect to trough RESTfm
###### RESTFM_KEY
The RESTfm key to connect to the FileMaker database
###### FM_USERNAME
The username to use for fetching files from the Linkz database
###### FM_PASSWORD
The password to use for fetching files from the Linkz database
###### FM_SERVER
The server where the FileMaker database is stored
###### FM_PROTOCOL
The protocol used to connect to the FileMaker database trough web publishing
###### FILE_URL_BASE
The base URL to access the uploaded files by the users to be later uploaded to the FileMaker database
###### FILE_URL_PATH
Filesystem path to the folder where uploaded files are stored

## Server configuration
The public web directory the server has to be serving is the public folder. This is the only required configuration on
the server side.

The application depends on a redis server to store session information for the logged-in users. The server must be on
the same machine as the application is going to be run off from.

## Application initialization
In order for the application to properly work the dependencies for the PHP application have to be fetched first
using the composer package manager. For installation instructions please check https://getcomposer.org/doc/00-intro.md

Once composer is installed It's as easy as running
```
composer install
```

Take in mind that depending on the installation method you selected, you might need to change the ```composer``` part to
match your environment.

## Application configuration
The next step is to configure the web application. This process is completely automated by embedded tools in the system.
In order to run these tools first open a console and ```cd``` into the public folder of the project.

The first step is to generate the application configuration for the application, this can be achieved with
the ```config``` tool. This tool will output any error found in the configuration like missing variables or 
non-valid values.

```
../bin/config
```

Once the configuration is generated the only step left is to generate the loadlist for the required files for
the web application:

```
../bin/autoloader
``` 

This should leave the application ready to be used if you point your browser to the public hostname.
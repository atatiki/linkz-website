#!/bin/bash

echo "Watching directory for js changes"

while [ true ]; do
	inotifywait -r public -e modify -qq
	cd public
	../bin/autoloader
	cd ..
done

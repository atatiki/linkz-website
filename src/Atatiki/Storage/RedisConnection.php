<?php declare(strict_types=1);
    namespace Atatiki\Storage;
    
    use Redis;
    
    class RedisConnection
    {
        /** @var RedisConnection The redis connection object */
        private static $static = null;
    
        /**
         * @var Redis The connection to the redis server to be used
         */
        private $redisSessionConnection = null;
    
        /**
         * Makes a connection to Redis server if the connection is not established yet
         *
         * @return \Atatiki\Storage\RedisConnection
         */
        static function makeConnection ()
        {
            if (self::$static == null)
            {
                self::$static = new RedisConnection ();
            }
            
            return self::$static;
        }
        
        protected function __construct ()
        {
            $this->redisSessionConnection = new Redis ();
            $this->redisSessionConnection->connect (getenv ('REDIS_CLUSTER'));
            $this->redisSessionConnection->select ((int) getenv ('REDIS_DATABASE'));
        }
        
        function getConnection ()
        {
            return $this->redisSessionConnection;
        }
    };
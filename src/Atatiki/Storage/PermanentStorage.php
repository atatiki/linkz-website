<?php declare (strict_types=1);
    namespace Atatiki\Storage;
    
    use Redis;

    /**
     * Allows to store somewhat permanent (but unimportant) data on redis
     *
     * @package Atatiki\Storage
     */
    class PermanentStorage
    {
        /**
         * The prefix to use for the session data keys in redis
         */
        const PERMANENT_STORAGE_KEY_PREFIX = 'permanent:';

        /**
         * @var string The key to the session data
         */
        private $redisKey = "";

        /**
         * @var bool Indicates if we can use igbinary serialization or PHP's
         */
        private $igbinary = false;

        /**
         * Constructs a new session storage based on the given authentication key
         *
         * @param string $authKey The authkey this SessionStorage handles
         */
        function __construct (string $authKey)
        {
            $this->redisKey = self::PERMANENT_STORAGE_KEY_PREFIX . $authKey;

            if (function_exists ('igbinary_serialize') === true)
            {
                $this->igbinary = true;
            }
        }

        /**
         * Searches for the given key in the session storage and returns it's value
         *
         * @param string $key The key to get
         *
         * @return mixed The value stored in that key
         */
        public function get (string $key)
        {
            $value = \Atatiki\Storage\RedisConnection::makeConnection ()->getConnection ()->hGet ($this->redisKey, $key);

            if ($this->igbinary === true)
            {
                return igbinary_unserialize ($value);
            }
            else
            {
                return unserialize ($value);
            }
        }
        
        public function has (string $key)
        {
            return \Atatiki\Storage\RedisConnection::makeConnection ()->getConnection ()->hExists ($this->redisKey, $key);
        }

        public function set (string $key, $value)
        {
            if ($this->igbinary === true)
            {
                $value = igbinary_serialize ($value);
            }
            else
            {
                $value = serialize ($value);
            }
    
            \Atatiki\Storage\RedisConnection::makeConnection ()->getConnection ()->hSet ($this->redisKey, $key, $value);

            return $this;
        }

        /**
         * Deletes this session storage and all it's data
         */
        public function delete ()
        {
            \Atatiki\Storage\RedisConnection::makeConnection ()->getConnection ()->delete ($this->redisKey);
        }
    };
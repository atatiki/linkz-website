<?php declare (strict_types=1);
    namespace Atatiki\Storage;
    
    use Ramsey\Uuid\Uuid;
    use Redis;

    class SessionStorage
    {
        /**
         * The prefix to use for the session data keys in redis
         */
        const SESSION_KEY_PREFIX = 'sessiondata:';

        /**
         * @var string The key to the session data
         */
        private $redisKey = "";
    
        /**
         * @var string The authentication key used by the session storage
         */
        private $authKey = '';

        /**
         * @var bool Indicates if we can use igbinary serialization or PHP's
         */
        private $igbinary = false;

        /**
         * Constructs a new session storage based on the given authentication key
         *
         * @param string $authKey The authkey this SessionStorage handles
         */
        private function __construct (string $authKey)
        {
            $this->redisKey = self::SESSION_KEY_PREFIX . $authKey;

            if (function_exists ('igbinary_serialize') === true)
            {
                $this->igbinary = true;
            }
            
            $this->authKey = $authKey;
        }

        /**
         * Searches for the given key in the session storage and returns it's value
         *
         * @param string $key The key to get
         *
         * @return mixed The value stored in that key
         */
        public function get (string $key)
        {
            $value = \Atatiki\Storage\RedisConnection::makeConnection ()->getConnection ()->hGet ($this->redisKey, $key);

            if ($this->igbinary === true)
            {
                return igbinary_unserialize ($value);
            }
            else
            {
                return unserialize ($value);
            }
        }

        public function set (string $key, $value)
        {
            if ($this->igbinary === true)
            {
                $value = igbinary_serialize ($value);
            }
            else
            {
                $value = serialize ($value);
            }
    
            \Atatiki\Storage\RedisConnection::makeConnection ()->getConnection ()->hSet ($this->redisKey, $key, $value);

            return $this;
        }
        
        public function getAuthKey ()
        {
            return $this->authKey;
        }
        
        /**
         * Deletes this session storage and all it's data
         */
        public function delete ()
        {
            \Atatiki\Storage\RedisConnection::makeConnection ()->getConnection ()->delete ($this->redisKey);
        }

        /**
         * Generates a new auth key that can be used in the system (uuid)
         *
         * @return string The new auth key to send to the user
         * @throws \Exception
         */
        public static function generateAuthKey (): string
        {
            return base64_encode (Uuid::uuid4 ()->getBytes ());
        }

        /**
         * Factory method, creates a new SessionStorage to handle session data for the given AuthKey
         *
         * @param string $authKey The authkey to read session data for
         *
         * @return self
         */
        public static function makeStorage (string $authKey): self
        {
            return new self ($authKey);
        }
    };
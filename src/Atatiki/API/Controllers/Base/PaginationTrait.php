<?php declare(strict_types=1);
    namespace Atatiki\API\Controllers\Base;

    /**
     * Small trait for controllers to allow for pagination-based responses without comprising the controller's logic
     *
     * @package Atatiki\API\Controllers\Base
     */
    trait PaginationTrait
    {
        /**
         * @param int                           $page The page to request
         * @param \Atatiki\Filemaker\Connection $connection The Filemaker connection to use for performing the SQL query
         * @param string                        $layout The layout to perform the SQL query on
         * @param string                        $sqlQuery The SQL query to run
         * @param int                           $recordsPerPage The amount of records to retrieve on each page
         *
         * @return array                        The paginated result
         * @throws \Exception If the query failed for some reason
         */
        protected static function performPaginatedSQL (
            int $page,
            \Atatiki\Filemaker\Connection $connection,
            string $layout,
            string $sqlQuery,
            int $recordsPerPage
        )
        {
            $page --;
            $skip = $page * $recordsPerPage;
            $result = $connection->layoutSQL ($layout, $sqlQuery, array (), $skip, $recordsPerPage);
            
            if ($result->isError () === true)
            {
                $result->raiseError ();
            }
            
            if ($result->isEmpty () === true)
            {
                return self::emptyPaginationResult ($recordsPerPage);
            }
            
            return array (
                'count' => $result->getFoundCount (),
                'pages' => round ($result->getFoundCount () / $recordsPerPage) + 1,
                'recordsPerPage' => $recordsPerPage,
                'data' => $result->getData () ?? []
            );
        }
        
        protected static function emptyPaginationResult (int $recordsPerPage)
        {
            return array ('count' => 0, 'pages' => 0, 'data' => array (), 'recordsPerPage' => $recordsPerPage);
        }
    };
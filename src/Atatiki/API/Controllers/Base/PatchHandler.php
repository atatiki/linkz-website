<?php declare(strict_types=1);
    namespace Atatiki\API\Controllers\Base;
    
    interface PatchHandler
    {
        function patch (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void;
    };
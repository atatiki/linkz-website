<?php declare (strict_types=1);
    namespace Atatiki\API\Controllers\Base;
    
    class BaseController
    {
        /**
         * Handles incoming requests for this controller
         *
         * @param \Atatiki\HTTP\Request $request Information of the request
         *
         * @return \Atatiki\HTTP\Response Response data (headers, status, etc)
         *
         * @throws \Exception If the endpoint doesn't support the given request method
         */
        public function handle (\Atatiki\HTTP\Request $request): \Atatiki\HTTP\Response
        {
            $response = $request->makeResponse ();

            $this->prepare ($request, $response);

            switch ($request->getMethod ())
            {
                case 'GET':
                    if ($this instanceof GetHandler == false)
                    {
                        throw new \Exception ("This endpoint doesn't allow GET requests");
                    }

                    $this->get ($request, $response);
                    break;
                    
                case 'POST':
                    if ($this instanceof PostHandler == false)
                    {
                        throw new \Exception ("This endpoint doesn't allow POST requests");
                    }

                    $this->post ($request, $response);
                    break;

                case 'PATCH':
                    if ($this instanceof PatchHandler == false)
                    {
                        throw new \Exception ("This endpoint doesn't allow PATCH requests");
                    }
                    
                    $this->patch ($request, $response);
                    break;
                    
                default:
                    throw new \Exception ("This endpoing doesn't allow " . $_SERVER ['REQUEST_METHOD'] . " requests");
            }

            return $response;
        }

        /**
         * Prepares the controller to handle the call
         *
         * @param \Atatiki\HTTP\Request $request
         * @param \Atatiki\HTTP\Response $response
         * @throws \Exception If any error on the preparation happened
         */
        public function prepare (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
        
        }
    };
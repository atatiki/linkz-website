<?php declare(strict_types=1);
    namespace Atatiki\API\Controllers\Base;

    use Atatiki\Storage\SessionStorage;

    class SecuredController extends BaseController
    {
        const TYPE_PARENT = 'parent';
        const TYPE_STUDENT = 'student';
        const TYPE_TUTOR = 'tutor';

        /**
         * @var \Atatiki\Storage\SessionStorage The session storage to read data from
         */
        protected $storage = null;

        public function prepare(\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $authkey = '';

            // token can be sent trough querystring parameters too
            if ($request->hasQueryStringParameter ('token') === true)
            {
                $authkey = $request->getQueryStringParameter ('token');
            }
            elseif ($request->hasHeader ('Authorization') == true)
            {
                $value = $request->getHeader ('Authorization');

                if (strncmp ($value, 'Bearer ', strlen ('Bearer ')) !== 0)
                {
                    throw new \Exception ("Expected Bearer OAuth token", \Atatiki\HTTP\Response::ERROR_AUTH);
                }

                $authkey = substr ($value, strlen ('Bearer '));
            }
            else
            {
                throw new \Exception ("Expected OAuth token", \Atatiki\HTTP\Response::ERROR_AUTH);
            }

            $this->storage = SessionStorage::makeStorage ($authkey);

            // check that token is still valid
            if ($this->storage->get ('expire_time') < time ())
            {
                throw new \Exception ("The specified OAuth token is not valid or expired", \Atatiki\HTTP\Response::ERROR_AUTH);
            }

            // everything looks okay, allow the user to continue
        }
    };
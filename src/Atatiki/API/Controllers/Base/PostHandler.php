<?php declare (strict_types=1);
    namespace Atatiki\API\Controllers\Base;
    
    interface PostHandler
    {
        function post (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void;
    };
<?php declare (strict_types=1);
    namespace Atatiki\Filemaker;

    class ResultSet implements \Iterator
    {
        /** @var ?array The result-set data to handle */
        private $result = array ();
        /** @var int The cursor for the iterator interface */
        private $cursor = 0;
        /** @var string The SQL query ran to obtain this result set */
        private $sql = '';

        public function __construct (?array $data, string $sql = '')
        {
            $this->result = $data;
            $this->sql = $sql;
        }

        /**
         * @return array The information about the request
         */
        public function getInfo (): array
        {
            return $this->result ['info'];
        }

        /**
         * @return array|null The resultset data if any
         */
        public function getData (): ?array
        {
            return $this->result ['data'];
        }
    
        /**
         * @return array|null The metadata result
         */
        public function getMetadata (): ?array
        {
            return $this->result ['meta'];
        }

        /**
         * @return bool Whether the resultset has data or not
         */
        public function hasData (): bool
        {
            return array_key_exists ('data', $this->result);
        }

        /**
         * @return bool Whether the resultset has info or not
         */
        public function hasInfo (): bool
        {
            return array_key_exists ('info', $this->result);
        }

        /**
         * @return int The number of records found
         */
        public function getFoundCount (): int
        {
            return (int) ($this->getInfo () ['foundSetCount'] ?? 0);
        }
    
        /**
         * @return int The number of records returned
         */
        public function getRecordCount (): int
        {
            return (int) $this->getInfo () ['fetchCount'];
        }

        /**
         * @return int The restfm result code
         */
        protected function getResultCode (): int
        {
            return $this->getInfo () ['X-RESTfm-Status'];
        }

        public function getRecord (int $index)
        {
            return $this->getData () [$index];
        }

        /**
         * Checks if a RESTfm's server answer indicates an error or not
         *
         * @return bool
         */
        function isError ()
        {
            if (is_array ($this->result) === false)
                return true;

            if ($this->hasInfo () === false)
                return true;

            $info = $this->getInfo ();

            if (array_key_exists ('X-RESTfm-Status', $info) === false)
                return true;

            if ($this->getResultCode () !== 200 && $this->getResultCode () != 201)
            {
                $ignorableErrors = array (
                    '401'
                );
                
                if (in_array ($info ['X-RESTfm-FM-Status'], $ignorableErrors) == false)
                    return true;
            }

            return false;
        }

        /**
         * Throws an uniformly-created exception with information about the query error
         *
         * @throws \Exception
         */
        function raiseError (): void
        {
            if ($this->isError () == true)
            {
                if ($this->hasInfo () === false)
                {
                    throw new \Exception ("Unknown error performir request");
                }

                $info = $this->getInfo ();

                if (getenv ('APP_ENVIRONMENT') == 'dev')
                {
                    throw new \Exception ($info ['X-RESTfm-Status'] . ' (' . $info ['X-RESTfm-FM-Status'] . '): ' . $info ['X-RESTfm-Reason'] . ". " . $info ['X-RESTfm-FM-Reason'] . ". SQL Query: " . $this->sql);
                }
                else
                {
                    throw new \Exception ($info ['X-RESTfm-Status'] . ': ' . $info ['X-RESTfm-Reason']);
                }
            }
        }

        /**
         * Checks if the resultset did return records
         *
         * @return bool
         */
        function isEmpty ()
        {
            $info = $this->getInfo ();

            if (array_key_exists ('X-RESTfm-FM-Status', $info) === true && $info ['X-RESTfm-FM-Status'] !== '401')
            {
                return true;
            }

            return $this->getFoundCount() === 0;
        }

        /**
         * Return the current element
         * @link https://php.net/manual/en/iterator.current.php
         * @return mixed Can return any type.
         * @since 5.0.0
         */
        public function current ()
        {
            return $this->getData () [$this->cursor];
        }

        /**
         * Move forward to next element
         * @link https://php.net/manual/en/iterator.next.php
         * @return void Any returned value is ignored.
         * @since 5.0.0
         */
        public function next ()
        {
            $this->cursor ++;
        }

        /**
         * Return the key of the current element
         * @link https://php.net/manual/en/iterator.key.php
         * @return mixed scalar on success, or null on failure.
         * @since 5.0.0
         */
        public function key ()
        {
            return $this->cursor;
        }

        /**
         * Checks if current position is valid
         * @link https://php.net/manual/en/iterator.valid.php
         * @return boolean The return value will be casted to boolean and then evaluated.
         * Returns true on success or false on failure.
         * @since 5.0.0
         */
        public function valid ()
        {
            return $this->hasData () == true && array_key_exists ($this->cursor, $this->getData ());
        }

        /**
         * Rewind the Iterator to the first element
         * @link https://php.net/manual/en/iterator.rewind.php
         * @return void Any returned value is ignored.
         * @since 5.0.0
         */
        public function rewind ()
        {
            $this->cursor = 0;
        }
    };
<?php declare (strict_types=1);
    namespace Atatiki\Filemaker;

    class Connection
    {
        /** @var string The base URL for all the request */
        private $baseURL = "";
        /** @var string The auth key for the connection */
        private $key = "";
        
        function __construct (string $server, string $database, string $key)
        {
            $this->baseURL = "http://" . $server . "/RESTfm/" . $database;
            $this->key = $key;
        }
    
        /**
         * Queries a layout and returns the present data
         *
         * @param string $layoutName The layout to query
         * @param string $field      The field to search by
         * @param string $value      The field's value
         * @param int    $skip       The number of records to skip from the beginning
         * @param int    $max        The maximum number of records to return
         *
         * @return ResultSet The data
         * @throws \Exception
         */
        function layout (string $layoutName, string $field = '', string $value = '', int $skip = 0, int $max = 0)
        {
            $url = $this->baseURL . "/layout/" . $layoutName . ".json?RFMkey=" . urlencode ($this->key) . "&RFMcontainer=default";
    
            if ($skip != 0)
            {
                $url .= "&RFMskip=" . ((string) $skip);
            }
    
            if ($max != 0)
            {
                $url .= "&RFMmax=" . ((string) $max);
            }
            
            $curl = curl_init ($url);
            curl_setopt ($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt ($curl, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt ($curl, CURLOPT_TIMEOUT, 8);
            
            $result = curl_exec ($curl);
            
            if ($result === false || curl_error ($curl))
            {
                curl_close ($curl);
                throw new \Exception ("Not returned data");
            }
            
            curl_close ($curl);
            
            return new ResultSet (json_decode ($result, true, 512, JSON_BIGINT_AS_STRING | JSON_OBJECT_AS_ARRAY), '');
        }
    
        /**
         * Queries a layout with an SQL query and returns the present data
         *
         * @param string $layoutName The layout to query
         * @param string $sql        The SQL query to execute
         * @param array  $search     Extra search parameters for RESTfm (if any)
         * @param int    $skip       The number of records to skip from the beginning
         * @param int    $max        The maximum number of records to return
         *
         * @return ResultSet The data
         * @throws \Exception
         */
        function layoutSQL (string $layoutName, string $sql, array $search = array (), int $skip = 0, int $max = 0): ResultSet
        {
            $url = $this->baseURL . "/layout/" . $layoutName . ".json?RFMfind=" . urlencode ($sql) . "&RFMkey=" . urlencode ($this->key) . "&RFMcontainer=default";

            if (count ($search) > 0)
            {
                $i = 0;

                foreach ($search as $key => $value)
                {
                    $i ++;

                    $url .= "&RFMsF" . $key . "=" . urlencode ($value);
                }
            }
            
            if ($skip != 0)
            {
                $url .= "&RFMskip=" . ((string) $skip);
            }
            
            if ($max != 0)
            {
                $url .= "&RFMmax=" . ((string) $max);
            }

            $curl = curl_init ($url);
    
            curl_setopt ($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt ($curl, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt ($curl, CURLOPT_TIMEOUT, 8);
    
            $result = curl_exec ($curl);

            if ($result === false || curl_error ($curl))
            {
                curl_close ($curl);
                throw new \Exception ("Not returned data");
            }
    
            curl_close ($curl);
    
            return new ResultSet (json_decode ($result, true, 512, JSON_BIGINT_AS_STRING | JSON_OBJECT_AS_ARRAY), $sql);
        }
    
        /**
         * Executes the given script on the specified layout
         *
         * @param string $script The script to run
         * @param string $layout The layout to run it in
         * @param array  $data   The data to be used as script parameter
         *
         * @return \Atatiki\Filemaker\ResultSet
         * @throws \Exception
         */
        function script (string $script, string $layout, array $data = array ())
        {
            $url = $this->baseURL . "/layout/" . $layout . ".json?RFMkey=" . urlencode ($this->key) . "&RFMscript=" . urlencode ($script) . "&RFMscriptParam=" . urlencode (json_encode ($data)) . "&RFMcontainer=default";
            
            $curl = curl_init ($url);
            
            curl_setopt ($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt ($curl, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt ($curl, CURLOPT_TIMEOUT, 5); // limit to two minutes should be more than enough
            
            $result = curl_exec ($curl);
            
            if ($result === false || curl_error ($curl))
            {
                curl_close ($curl);
                throw new \Exception ("Not returned data");
            }
            
            curl_close ($curl);
            
            return new ResultSet (json_decode ($result, true, 512, JSON_BIGINT_AS_STRING | JSON_OBJECT_AS_ARRAY));
        }
    
        /**
         * Deletes the given recordID on the specified layout
         *
         * @param string $recordID The record ID to remove
         * @param string $layout   The layout to remove the record from
         *
         * @return \Atatiki\Filemaker\ResultSet
         * @throws \Exception
         */
        function deleteRecord (string $recordID, string $layout)
        {
            $url = $this->baseURL . '/layout/' . $layout . '/' . $recordID . '.json?RFMkey=' . urlencode ($this->key);
            
            $curl = curl_init ($url);
            
            curl_setopt ($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt ($curl, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt ($curl, CURLOPT_TIMEOUT, 8);
            curl_setopt ($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
            
            $result = curl_exec ($curl);
            
            if ($result === false || curl_error ($curl))
            {
                curl_close ($curl);
                throw new \Exception ("Not returned data");
            }
            
            curl_close ($curl);
            
            return new ResultSet (json_decode ($result, true, 512, JSON_BIGINT_AS_STRING | JSON_OBJECT_AS_ARRAY));
        }
    
        /**
         * Updates the specified fields in the given recordID under the layout
         *
         * @param string $recordID The record to update
         * @param string $layout   The layout the record we're updating is
         * @param array  $fields   The fields to update
         *
         * @return \Atatiki\Filemaker\ResultSet
         * @throws \Exception
         */
        function updateRecord (string $recordID, string $layout, array $fields)
        {
            $url = $this->baseURL . '/layout/' . $layout . '/' . $recordID . '.json?RFMkey=' . urlencode ($this->key);
            
            $curl = curl_init ($url);
            
            curl_setopt ($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt ($curl, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt ($curl, CURLOPT_TIMEOUT, 8);
            curl_setopt ($curl, CURLOPT_POSTFIELDS, json_encode (array ('data' => array ($fields))));
            curl_setopt ($curl, CURLOPT_HTTPHEADER, array ('Content-Type: application/json'));
            curl_setopt ($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
            
            $result = curl_exec ($curl);
            
            if ($result === false || curl_error ($curl))
            {
                curl_close ($curl);
                throw new \Exception ("Not returned data");
            }
            
            curl_close ($curl);
            
            return new ResultSet (json_decode ($result, true, 512, JSON_BIGINT_AS_STRING | JSON_OBJECT_AS_ARRAY));
        }
        
        /**
         * Allows the creation of new records in the given layout
         *
         * @param string $layoutName The layout where to create the record
         * @param array  $record     The actual record data to be stored
         *
         * @return \Atatiki\Filemaker\ResultSet
         * @throws \Exception
         */
        function createRecord (string $layoutName, array $record)
        {
            $url = $this->baseURL . '/layout/' . $layoutName . '.json?RFMkey=' . urlencode ($this->key);
            
            $curl = curl_init ($url);
            
            curl_setopt ($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt ($curl, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt ($curl, CURLOPT_TIMEOUT, 8);
            curl_setopt ($curl, CURLOPT_POSTFIELDS, json_encode (array ('data' => array ($record))));
            curl_setopt ($curl, CURLOPT_HTTPHEADER, array ('Content-Type: application/json'));
            
            $result = curl_exec ($curl);
            
            if ($result === false || curl_error ($curl))
            {
                curl_close ($curl);
                throw new \Exception ("Not returned data");
            }
            
            curl_close ($curl);
            
            return new ResultSet (json_decode ($result, true, 512, JSON_BIGINT_AS_STRING | JSON_OBJECT_AS_ARRAY));
        }
    
        /**
         * Similar to PDO prepared statements, returns the SQL query to be run on the FM server
         *
         * @param string $sqlBase The base SQL string, using :variable for values
         * @param array  $values A map of :variable => value to replace in the SQL string
         *
         * @return string
         */
        function prepareSqlQuery (string $sqlBase, array $values): string
        {
            $escapedValues = array ();
            
            foreach ($values as $variable => $value)
            {
                $escapedValues ['/' . preg_quote ($variable) . '/'] = '\'' . preg_replace ('/([@#\\!<>=*"\'?])/', '\\\\${1}', $value) . '\'';
            }
            
            return preg_replace (array_keys ($escapedValues), array_values ($escapedValues), $sqlBase);
        }

        public static function makeConnection ()
        {
            return new \Atatiki\Filemaker\Connection (
                getenv ('RESTFM_SERVER'),
                getenv ('RESTFM_DATABASE'),
                getenv ('RESTFM_KEY')
            );
        }
    };
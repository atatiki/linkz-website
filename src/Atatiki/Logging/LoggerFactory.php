<?php declare(strict_types=1);
    namespace Atatiki\Logging;
    
    class LoggerFactory
    {
        /** @var \Monolog\Logger[] The loggers used by the system */
        private static $systemLogger = array ();
        
        protected static function getChannelLogger (string $channel): \Monolog\Logger
        {
            if (array_key_exists ($channel, self::$systemLogger) === false)
            {
                self::$systemLogger [$channel] = new \Monolog\Logger ($channel);
                self::$systemLogger [$channel]->pushHandler (self::getStreamHandler ('system.log'));
            }
    
            return self::$systemLogger [$channel];
        }
        
        /**
         * @return \Monolog\Logger The performance logger
         * @throws \Exception
         */
        static function getPerformanceLogger (): \Monolog\Logger
        {
            return self::getChannelLogger ('performance');
        }
        
        static function getSystemLogger (): \Monolog\Logger
        {
            return self::getChannelLogger ('system');
        }
        
        static function getStreamHandler (string $filename, $logger = \Monolog\Logger::DEBUG)
        {
            $output = "%datetime% - %channel% [%level_name%]: %message% %context% %extra%\n";
            $formatter = new \Monolog\Formatter\LineFormatter ($output, DATE_ATOM);
            $stream = new \Monolog\Handler\StreamHandler (getenv ('LOGS_PATH') . $filename, $logger);
            $stream->setFormatter ($formatter);
            
            return $stream;
        }
    };
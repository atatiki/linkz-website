<?php declare(strict_types=1);
    namespace Linkz\Model;
    
    use Ramsey\Uuid\Uuid;
    
    class Notifications
    {
        /** @var array The list of notifications for this user */
        private $notifications = array ();
        /** @var \Atatiki\Storage\PermanentStorage The permanent storage where to save the data */
        private $permanentStorage = null;
        
        protected function __construct (array $notifications, \Atatiki\Storage\PermanentStorage $storage)
        {
            $this->notifications = $notifications;
            $this->permanentStorage = $storage;
        }
    
        /**
         * Saves the notification status to the permanent storage
         */
        function persist ()
        {
            $this->cleanup ();
            $this->permanentStorage->set ('notifications', $this->notifications);
        }
    
        /**
         * Performs cleanup on the notification data,
         * removing notifications too old or that have been read more than an hour ago
         */
        function cleanup ()
        {
            $finalNotifications = array ();
            $updateTimeThreshold = time () - (60 * 60);
            
            foreach ($this->notifications as $notification)
            {
                if (
                    ($notification ['status'] == 'new') ||
                    ($notification ['status'] == 'read' && $notification ['updatetime'] > $updateTimeThreshold)
                )
                {
                    $finalNotifications [] = $notification;
                }
            }
            
            $this->notifications = $finalNotifications;
        }
        
        function insertNewNotification (string $message, string $type, $extraInformation = '')
        {
            $this->notifications [] = array (
                'guid' => Uuid::uuid4 ()->getHex (),
                'message' => $message,
                'type' => $type,
                'info' => $extraInformation,
                'status' => 'new',
                'time' => time (),
                'updatetime' => 0
            );
        }
        
        function getNotifications (): array
        {
            return $this->notifications;
        }
        
        function markNotificationAsRead (string $guid)
        {
            foreach ($this->notifications as &$notification)
            {
                if ($notification ['guid'] == $guid)
                {
                    $notification ['status'] = 'read';
                    $notification ['updatetime'] = time ();
                }
            }
        }
        
        static function makeFromStorage (\Atatiki\Storage\PermanentStorage $storage): self
        {
            $notifications = array ();
    
            if ($storage->has ('notifications') === true)
            {
                $notifications = $storage->get ('notifications');
            }
            
            return new self ($notifications, $storage);
        }
    };
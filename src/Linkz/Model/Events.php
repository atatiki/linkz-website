<?php declare(strict_types=1);
    namespace Linkz\Model;

    class Events
    {
        const RECORDS_PER_PAGE = 20;
        const DATE_FORMAT = 'm/d/Y';
        const STATUS_CANCELLED = 'Not confirmed';
        const STATUS_CANCELLED_WITHIN_24HOURS = 'Canceled within 24h';
        
        use \Atatiki\API\Controllers\Base\PaginationTrait;
        
        static function get (string $eventId, &$recordId = null): array
        {
            if ($eventId == '')
                return array ();
            
            $sqlQuery = "SELECT * WHERE _id LIKE :id";
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $sqlQuery = $fm->prepareSqlQuery ($sqlQuery, array (':id' => $eventId));
            $result = $fm->layoutSQL ('Ev__Event', $sqlQuery);

            if ($result->isError () === true)
            {
                $result->raiseError ();
            }

            if ($result->isEmpty () === true)
            {
                return array ();
            }
            
            $recordId = $result->getMetadata () [0] ['recordID'];

            return $result->getRecord (0);
        }
        
        static function getNotes (string $eventId, string $userType): array
        {
            if ($eventId == '')
                return array ();
            
            $sqlQuery = "SELECT * WHERE _id_Event = :id";
    
            switch ($userType)
            {
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT:
                    $sqlQuery .= " AND No301_VisibleToParent = 1";
                    break;
        
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_STUDENT:
                    $sqlQuery .= " AND No300_VisibleToStudent = 1";
                    break;
        
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR:
                default:
                    break;
            }
    
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $sqlQuery = $fm->prepareSqlQuery ($sqlQuery, array (':id' => $eventId));
            $result = $fm->layoutSQL ('No__Notes', $sqlQuery);
    
            if ($result->isError () === true)
            {
                $result->raiseError ();
            }
    
            if ($result->isEmpty () === true)
            {
                return array ();
            }
    
            return $result->getData ();
        }
        
        static function getFiles (string $eventId): array
        {
            if ($eventId == '')
                return array ();
    
            $sqlQuery = "SELECT * WHERE _id_Event LIKE :id";
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $sqlQuery = $fm->prepareSqlQuery ($sqlQuery, array (':id' => $eventId));
            $result = $fm->layoutSQL ('Fi__File', $sqlQuery);
    
            if ($result->isError () === true)
            {
                $result->raiseError ();
            }
    
            if ($result->isEmpty () === true)
            {
                return array ();
            }
    
            return $result->getData ();
        }
        
        static function getFeed (bool $showCancelled, int $page = 1, ?string $tutorId = null, ?string $studentId = null, ?array $studentsToSearch = null)
        {
            // only check these if the user is not a tutor and not a student
            if (is_string ($tutorId) === false && is_string ($studentId) === false && is_array ($studentsToSearch) === false)
            {
                throw new \Linkz\API\Exceptions\NotAllowedException ('Filter parameters are not correct');
            }
            // no students means no data to be returned
            elseif (is_array ($studentsToSearch) === true && count ($studentsToSearch) === 0)
            {
                return self::emptyPaginationResult (self::RECORDS_PER_PAGE);
            }

            // build the SQL query
            $sqlQuery = "SELECT * WHERE ";
            $params = array (
                ':date' => date (self::DATE_FORMAT) . '...' . date (self::DATE_FORMAT, strtotime ('2 weeks'))
            );
            
            if (is_array ($studentsToSearch) === true)
            {
                for ($i = 0; $i < count ($studentsToSearch); $i ++)
                {
                    $sqlQuery .= "('_id_Student' LIKE :student" . ((string) $i) . " AND 'Ev100_Date' LIKE :date)";
    
                    if ($i != (count ($studentsToSearch) - 1))
                    {
                        $sqlQuery .= " OR ";
                    }
    
                    $params [':student' . ((string) $i)] = $studentsToSearch [$i];
                }
            }
            elseif (is_string ($tutorId) === true)
            {
                $sqlQuery .= "_id_Tutor = :id AND 'Ev100_Date' LIKE :date ";
                $params [':id'] = $tutorId;
            }
            else if (is_string ($studentId) === true)
            {
                $sqlQuery .= "_id_Student = :id AND 'Ev100_Date' LIKE :date";
                $params [':id'] = $studentId;
            }
            
            if ($showCancelled == false)
            {
                $sqlQuery .= " OMIT 'Ev105_Status_ac' = :cancelled";
                $params [':cancelled'] = self::STATUS_CANCELLED;
            }

            $sqlQuery .= " ORDER BY 'Ev100_Date' ASC";

            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $sqlQuery = $fm->prepareSqlQuery ($sqlQuery, $params);
            return self::performPaginatedSQL ($page, $fm, 'Ev__Event', $sqlQuery, self::RECORDS_PER_PAGE);
        }
        
        static function getParentFeed (array $students, int $page = 1, bool $showCanceled = false)
        {
            return self::getFeed ($showCanceled, $page, null, null, $students);
        }
        
        static function getTutorFeed (string $tutorId, int $page = 1, bool $showCancelled = false)
        {
            return self::getFeed ($showCancelled, $page, $tutorId);
        }
        
        static function getStudentFeed (string $studentId, int $page = 1, bool $showCancelled = false)
        {
            return self::getFeed ($showCancelled, $page, null, $studentId);
        }
        
        static function getByDate (string $date, bool $showCancelled, int $page = 1, ?string $tutorId = null, ?string $studentId = null, ?array $studentsToSearch = null)
        {
            // only check these if the user is not a tutor and not a student
            if (is_string ($tutorId) === false && is_string ($studentId) === false && is_array ($studentsToSearch) === false)
            {
                throw new \Linkz\API\Exceptions\NotAllowedException ('Filter parameters are not correct');
            }
            // no students means no data to be returned
            elseif (is_array ($studentsToSearch) === true && count ($studentsToSearch) === 0)
            {
                return self::emptyPaginationResult (self::RECORDS_PER_PAGE);
            }
    
            // build the SQL query
            $sqlQuery = "SELECT * WHERE ";
            $params = array (':date' => $date);
    
            if (is_array ($studentsToSearch) === true)
            {
                for ($i = 0; $i < count ($studentsToSearch); $i ++)
                {
                    $sqlQuery .= "(_id_Student = :student" . ((string) $i) . " AND 'Ev100_Date' LIKE :date)";
            
                    if ($i != (count ($studentsToSearch) - 1))
                    {
                        $sqlQuery .= " OR ";
                    }
            
                    $params [':student' . ((string) $i)] = $studentsToSearch [$i];
                }
            }
            elseif (is_string ($tutorId) === true)
            {
                $sqlQuery .= "_id_Tutor = :id AND 'Ev100_Date' LIKE :date";
                $params [':id'] = $tutorId;
            }
            else if (is_string ($studentId) === true)
            {
                $sqlQuery .= '_id_Student = :id AND \'Ev100_Date\' LIKE :date';
                $params [':id'] = $studentId;
            }
    
            if ($showCancelled == false)
            {
                $sqlQuery .= " OMIT 'Ev105_Status_ac' = :cancelled";
                $params [':cancelled'] = self::STATUS_CANCELLED;
            }
    
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $sqlQuery = $fm->prepareSqlQuery ($sqlQuery, $params);
            return self::performPaginatedSQL ($page, $fm, 'Ev__Event', $sqlQuery, self::RECORDS_PER_PAGE);
        }
        
        static function getParentByDate (string $date, array $students, int $page = 1, bool $showCanceled = false)
        {
            return self::getByDate ($date, $showCanceled, $page, null, null, $students);
        }
        
        static function getTutorByDate (string $date, string $tutorId, int $page = 1, bool $showCancelled = false)
        {
            return self::getByDate ($date, $showCancelled, $page, $tutorId);
        }
        
        static function getStudentByDate (string $date, string $studentId, int $page = 1, bool $showCancelled = false)
        {
            return self::getByDate ($date, $showCancelled, $page, null, $studentId);
        }
    
        static function getFullTutorByDate (string $date, string $tutorId)
        {
            // build the SQL query
            $sqlQuery = "SELECT * WHERE _id_Tutor = :id AND 'Ev100_Date' = :date OMIT 'Ev105_Status_ac' = :cancelled";
            $params = array (':date' => $date, ':id' => $tutorId, ':cancelled' => self::STATUS_CANCELLED);
        
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $sqlQuery = $fm->prepareSqlQuery ($sqlQuery, $params);
            $result = $fm->layoutSQL ('Ev__Event', $sqlQuery);
        
            if ($result->isError () == true)
            {
                $result->raiseError ();
            }
        
            if ($result->isEmpty () == true)
            {
                return array ();
            }
        
            return $result->getData ();
        }
        
        static function getFullStudentByDate (string $date, string $studentId)
        {
            // build the SQL query
            $sqlQuery = "SELECT * WHERE _id_Student = :id AND 'Ev100_Date' = :date OMIT 'Ev105_Status_ac' = :cancelled";
            $params = array (':date' => $date, ':id' => $studentId, ':cancelled' => self::STATUS_CANCELLED);
            
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $sqlQuery = $fm->prepareSqlQuery ($sqlQuery, $params);
            $result = $fm->layoutSQL ('Ev__Event', $sqlQuery);
            
            if ($result->isError () == true)
            {
                $result->raiseError ();
            }
            
            if ($result->isEmpty () == true)
            {
                return array ();
            }
            
            return $result->getData ();
        }
        
        static function getFile (string $eventId, string $fileId, &$recordId = null)
        {
            $sqlQuery = "SELECT * WHERE _id_Event = :eventId AND _id = :id";
            $params = array (':eventId' => $eventId, ':id' => $fileId);
            
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $sqlQuery = $fm->prepareSqlQuery ($sqlQuery, $params);
            $result = $fm->layoutSQL ('Fi__File', $sqlQuery);
            
            if ($result->isError () === true)
            {
                $result->raiseError ();
            }
            
            if ($result->isEmpty () === true)
            {
                return array ();
            }
            
            // record ID might be needed here
            $recordId = $result->getMetadata () [0]['recordID'];
            
            return $result->getRecord (0);
        }
    };
<?php declare(strict_types=1);
    namespace Linkz\Model;
    
    class Parents
    {
        static function get (string $parentId, &$recordId = null)
        {
            if ($parentId == '')
                return array ();
                
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $sqlQuery = $fm->prepareSqlQuery ("SELECT * WHERE _id LIKE :id", array (':id' => $parentId));
            $result = $fm->layoutSQL ('Pe__Person', $sqlQuery);
    
            if ($result->isError () === true)
            {
                $result->raiseError ();
            }
    
            if ($result->isEmpty () === true)
            {
                return array ();
            }
    
            $recordId = $result->getMetadata () [0] ['recordID'];
    
            return $result->getRecord (0);
        }
    };
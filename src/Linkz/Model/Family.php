<?php declare (strict_types=1);
    namespace Linkz\Model;
    
    class Family
    {
        static function get (string $familyId): array
        {
            if ($familyId == '')
                return array ();
            
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $sqlQuery = $fm->prepareSqlQuery ('SELECT * WHERE _id LIKE :id', array (':id' => $familyId));
            $result = $fm->layoutSQL ('Fa__Family', $sqlQuery);
    
            if ($result->isError () === true)
            {
                $result->raiseError ();
            }
    
            if ($result->isEmpty () === true)
            {
                return array ();
            }
    
            return $result->getRecord (0);
        }
        
        static function getStudents (string $familyId): array
        {
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $sqlQuery = $fm->prepareSqlQuery ('SELECT * WHERE _id_Family LIKE :family_id', array (':family_id' => $familyId));
            $result = $fm->layoutSQL ('St__Student', $sqlQuery);

            if ($result->isError () === true)
            {
                $result->raiseError ();
            }

            return $result->getData ();
        }
    };
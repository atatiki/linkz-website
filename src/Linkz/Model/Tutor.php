<?php declare (strict_types=1);
    namespace Linkz\Model;
    
    class Tutor
    {
        static function get (string $tutorId, bool $requireInvoiceInformation = false, &$recordId = null): array
        {
            if ($tutorId == '')
                return array ();
            
            $sqlQuery = "SELECT * WHERE _id = :id";
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $sqlQuery = $fm->prepareSqlQuery ($sqlQuery, array (':id' => $tutorId));
            $result = $fm->layoutSQL (( ($requireInvoiceInformation) ? 'Tu__Tutor_Profile' : 'Tu__Tutor_Information'), $sqlQuery);

            if ($result->isError () === true)
            {
                $result->raiseError ();
            }

            if ($result->isEmpty () === true)
            {
                return array ();
            }
    
            $recordId = $result->getMetadata () [0] ['recordID'];

            return $result->getRecord (0);
        }
    };
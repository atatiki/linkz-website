<?php declare(strict_types=1);
    namespace Linkz\Model;

    class Contracts
    {
        const RECORDS_PER_PAGE = 20;
        use \Atatiki\API\Controllers\Base\PaginationTrait;
        
        protected static function get (int $page = 1, ?array $students = null, ?string $familyId = null, ?string $tutorId = null, int $ignoreEmpty = 0): array
        {
            $sqlQuery = "SELECT * WHERE ";
            $params = array ();

            if (is_array ($students) === true)
            {
                for ($i = 0; $i < count ($students); $i ++)
                {
                    $sqlQuery .= "_id_Student LIKE :student" . ((string) $i) . " OR ";
                    $params [':student' . ((string) $i)] = $students [$i];
                }
                
                $sqlQuery .= "_id_Family = :family";
                $params [':family'] = $familyId;
            }
            elseif (is_string ($tutorId) === true)
            {
                $sqlQuery .= '_id_Tutor = :tutor';
                $params [':tutor'] = $tutorId;
            }
            else
            {
                throw new \Linkz\API\Exceptions\NotAllowedException ('Must specify criteria for the search');
            }
            
            if ($ignoreEmpty)
            {
                $sqlQuery .= ' OMIT Co207_CountRows_c LIKE \'=\'';
            }

            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $sqlQuery = $fm->prepareSqlQuery ($sqlQuery, $params);
            return self::performPaginatedSQL ($page, $fm, 'Co__Contract', $sqlQuery, self::RECORDS_PER_PAGE);
        }
        
        static function getByTutor (string $tutorId, int $page = 1, int $ignoreEmpty = 0): array
        {
            return self::get ($page, null, null, $tutorId, $ignoreEmpty);
        }
        
        static function getByFamily (array $students, string $familyId, int $page = 1, int $ignoreEmpty = 0): array
        {
            return self::get ($page, $students, $familyId, null, $ignoreEmpty);
        }
        
        /**
         * Searches for the given contract's basic information
         *
         * @param string $contractId The contract to get the data for
         *
         * @return array The contract's data
         *
         * @throws \Exception
         */
        static function getContract (string $contractId): array
        {
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $query = $fm->prepareSqlQuery ("SELECT * WHERE _id LIKE :id", array (':id' => $contractId));
            $result = $fm->layoutSQL ('Co__Contract', $query);

            if ($result->isError () == true)
            {
                $result->raiseError ();
            }
            
            if ($result->isEmpty () == true)
            {
                return array ();
            }

            return $result->getRecord (0);
        }
        
        static function getRows (string $contractId): array
        {
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $query = $fm->prepareSqlQuery ("SELECT * WHERE _id_Contract LIKE :id", array (':id' => $contractId));
            $result = $fm->layoutSQL ('Cor__ContractRow', $query);
    
            if ($result->isError () == true)
            {
                $result->raiseError ();
            }
            
            if ($result->isEmpty () == true)
            {
                return array ();
            }
    
            return $result->getData ();
        }
        
        /**
         * Searches all the invoices for the given contract
         *
         * @param string $contractId The contract to get the invoices for
         *
         * @return array The list of invoices for this contract
         * @throws \Exception If the contract doesn't belong to the user
         */
        static function getInvoices (string $contractId): array
        {
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $query = $fm->prepareSqlQuery ("SELECT * WHERE _id_Contract LIKE :contract_id", array (':contract_id' => $contractId));
            $result = $fm->layoutSQL ('In__Invoice', $query);

            if ($result->isError () == true)
            {
                $result->raiseError ();
            }
            
            if ($result->isEmpty () == true)
            {
                return array ();
            }
            
            return $result->getData ();
        }
    };
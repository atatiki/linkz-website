<?php declare (strict_types=1);
    namespace Linkz\Model;
    
    class Invoice
    {
        /**
         * Obtains the invoice information for the given invoice ID
         *
         * @param string $invoiceId The invoice to get info
         *
         * @return array The invoice data
         * @throws \Exception
         */
        static function get (string $invoiceId): array
        {
            if ($invoiceId == '')
                return array ();
            
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $sqlQuery = $fm->prepareSqlQuery ('SELECT * WHERE _id = :id', array (':id' => $invoiceId));
            $result = $fm->layoutSQL ('In__Invoice', $sqlQuery);
    
            if ($result->isError () == true)
            {
                $result->raiseError ();
            }
    
            return $result->getRecord (0);
        }
        
        /**
         * Obtains the invoice rows for the given invoice ID
         *
         * @param string $invoiceId The invoice to get info
         *
         * @return array The invoice rows
         * @throws \Exception
         */
        static function getRows (string $invoiceId): array
        {
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $sqlQuery = $fm->prepareSqlQuery ('SELECT * WHERE _id_Invoice LIKE :id', array (':id' => $invoiceId));
            $result = $fm->layoutSQL ('Inr__InvoiceRow', $sqlQuery);

            if ($result->isError () == true)
            {
                $result->raiseError ();
            }
            
            if ($result->isEmpty () == true)
            {
                return array ();
            }

            return $result->getData ();
        }

        /**
         * Obtains the invoice payments for the given invoice ID
         *
         * @param string $invoiceId The invoice to get the payments for
         *
         * @return array The payment rows
         * @throws \Exception
         */
        static function getPayments (string $invoiceId): array
        {
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $sqlQuery = $fm->prepareSqlQuery ('SELECT * WHERE _id_Invoice LIKE :id', array (':id' => $invoiceId));
            $result = $fm->layoutSQL ('Pa__Payment', $sqlQuery);

            if ($result->isError () == true)
            {
                $result->raiseError ();
            }

            return $result->getData () ?? [];
        }
    };
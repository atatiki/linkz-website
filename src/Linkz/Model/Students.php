<?php declare (strict_types=1);
    namespace Linkz\Model;
    
    class Students
    {
        static function get (string $studentId): array
        {
            if ($studentId == '')
                return array ();
    
            $sqlQuery = "SELECT * WHERE _id LIKE :id";
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $sqlQuery = $fm->prepareSqlQuery ($sqlQuery, array (':id' => $studentId));
            $result = $fm->layoutSQL ('St__Student', $sqlQuery);
    
            if ($result->isError () === true)
            {
                $result->raiseError ();
            }
    
            if ($result->isEmpty () === true)
            {
                return array ();
            }
    
            return $result->getRecord (0);
        }
    };
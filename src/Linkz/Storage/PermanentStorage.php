<?php declare(strict_types=1);
    namespace Linkz\Storage;
    
    use \Atatiki\Storage\PermanentStorage as AtatikiPermanentStorage;

    /**
     * Base accessor functions to obtain access to specified permanent storages
     *
     * @package Linkz\Storage
     */
    class PermanentStorage
    {
        /** @var string The prefix used for tutor's storage */
        const TUTOR_STORAGE_PREFIX = 'tutor:';
        /** @var string The prefix used for student's storage */
        const STUDENT_STORAGE_PREFIX = 'student:';
        /** @var string The prefix used for the parent's storage */
        const PARENT_STORAGE_PREFIX = 'parent:';
        /** @var string The prefix used for the family's storage */
        const FAMILY_STORAGE_PREFIX = 'family:';
    
        /**
         * Gives access to the permanent storage of the given tutorId
         *
         * @param string $tutorId
         *
         * @return \Atatiki\Storage\PermanentStorage
         */
        public static function getTutorStorage (string $tutorId)
        {
            return new AtatikiPermanentStorage (self::TUTOR_STORAGE_PREFIX . $tutorId);
        }
    
        /**
         * Gives access to the permanent storage of the given familyId
         *
         * @param string $familyId
         *
         * @return \Atatiki\Storage\PermanentStorage
         */
        public static function getFamilyStorage (string $familyId)
        {
            return new AtatikiPermanentStorage (self::FAMILY_STORAGE_PREFIX . $familyId);
        }
    
        /**
         * Gives access to the permanent storage of the given studentId
         *
         * @param string $studentId
         *
         * @return \Atatiki\Storage\PermanentStorage
         */
        public static function getStudentStorage (string $studentId)
        {
            return new AtatikiPermanentStorage (self::STUDENT_STORAGE_PREFIX . $studentId);
        }
    
        /**
         * Gives access to the permanent storage of the given parentId
         *
         * @param string $parentId
         *
         * @return \Atatiki\Storage\PermanentStorage
         */
        public static function getParentStorage (string $parentId)
        {
            return new AtatikiPermanentStorage (self::PARENT_STORAGE_PREFIX . $parentId);
        }
        
        public static function getUsersPermanentStorage (\Atatiki\Storage\SessionStorage $session): \Atatiki\Storage\PermanentStorage
        {
            switch ($session->get ('type'))
            {
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_STUDENT:
                    return self::getStudentStorage ($session->get ('id'));
                    
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT:
                    return self::getFamilyStorage ($session->get ('family_id'));
                    
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR:
                    return self::getTutorStorage ($session->get ('id'));
                    
                default:
                    throw new \Linkz\API\Exceptions\NotAllowedException ('You are not allowed here');
            }
        }
    };
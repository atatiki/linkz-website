<?php declare (strict_types=1);
    namespace Linkz\API\Exceptions;

    class NotAllowedException extends \Exception
    {
        function __construct(string $message = "")
        {
            parent::__construct($message, 500);
        }
    }
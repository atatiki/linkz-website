<?php declare(strict_types=1);
    namespace Linkz\API\Controllers;
    
    class StatesController extends \Atatiki\API\Controllers\Base\SecuredController implements \Atatiki\API\Controllers\Base\GetHandler
    {
        function get (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $response->setContentType (\Atatiki\HTTP\Response::JSON);
            
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $result = $fm->layout ('Sa__States', '', '', 0, 1500);
            
            if ($result->isError () === true)
            {
                $result->raiseError ();
            }
            
            if ($result->isEmpty () === true)
            {
                $response->setOutput (array ());
            }
            
            $response->setOutput ($result->getData ());
        }
    };
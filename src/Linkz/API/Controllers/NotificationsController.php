<?php declare(strict_types=1);
    namespace Linkz\API\Controllers;

    class NotificationsController
        extends \Atatiki\API\Controllers\Base\SecuredController
        implements \Atatiki\API\Controllers\Base\GetHandler
    {
        function get (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            // check for the user's notifications
            $notifications =
                \Linkz\Model\Notifications::makeFromStorage (
                    \Linkz\Storage\PermanentStorage::getUsersPermanentStorage ($this->storage)
                );
            
            $response->setContentType (\Atatiki\HTTP\Response::JSON);
            $response->setOutput ($notifications->getNotifications ());
        }
    };
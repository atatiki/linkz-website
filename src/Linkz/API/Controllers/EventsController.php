<?php declare(strict_types=1);
    namespace Linkz\API\Controllers;

    use \Linkz\API\Exceptions\NotAllowedException;

    class EventsController
        extends \Atatiki\API\Controllers\Base\SecuredController
        implements \Atatiki\API\Controllers\Base\GetHandler,
                   \Atatiki\API\Controllers\Base\PostHandler,
                   \Atatiki\API\Controllers\Base\PatchHandler
    {
        const DATE_FORMAT = 'm/d/Y';
        const RECORDS_PER_PAGE = 50;
        
        use \Atatiki\API\Controllers\Base\PaginationTrait;

        function get(\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $response->setContentType (\Atatiki\HTTP\Response::JSON);

            switch ($this->storage->get ('type'))
            {
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT:
                    $this->getParentEvents ($request, $response);
                    break;
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_STUDENT:
                    $this->getStudentEvents ($request, $response);
                    break;
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR:
                    $this->getTutorEvents ($request, $response);
                    break;
                default:
                    throw new NotAllowedException ('Not allowed');
            }
        }

        function getParentEvents (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            // check if we have any query string parameters for filters
            $ignoreStudents = array ();
            $showCancelled = ($request->getQueryStringParameter ('showCancelled') ?? 'true') == 'true';
            $datestamp = date (self::DATE_FORMAT);

            if (is_array ($request->getQueryStringParameter ('ignoreStudents')) === true)
            {
                $ignoreStudents = $request->getQueryStringParameter ('ignoreStudents');
            }

            if ($request->hasQueryStringParameter ('date') === true)
            {
                $datetime = new \DateTime ($request->getQueryStringParameter ('date'));
                $datestamp = $datetime->format (self::DATE_FORMAT);
            }

            $students = array_values (array_diff ($this->storage->get ('students'), $ignoreStudents));

            $response->setOutput (
                \Linkz\Model\Events::getParentByDate (
                    $datestamp,
                    $students,
                    (int) ($request->getQueryStringParameter ('page') ?? 1),
                    $showCancelled
                )
            );
        }

        function getStudentEvents (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $datestamp = date (self::DATE_FORMAT);
            $showCancelled = ($request->getQueryStringParameter ('showCancelled') ?? 'true') == 'true';
            
            if ($request->hasQueryStringParameter ('date') === true)
            {
                $datetime = new \DateTime ($request->getQueryStringParameter ('date'));
                $datestamp = $datetime->format (self::DATE_FORMAT);
            }
    
            $response->setOutput (
                \Linkz\Model\Events::getStudentByDate (
                    $datestamp,
                    $this->storage->get ('id'),
                    (int) ($request->getQueryStringParameter ('page') ?? 1),
                    $showCancelled
                )
            );
        }

        function getTutorEvents (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $datestamp = date (self::DATE_FORMAT);
            $showCancelled = ($request->getQueryStringParameter ('showCancelled') ?? 'true') == 'true';
            
            if ($request->hasQueryStringParameter ('date') === true)
            {
                $datetime = new \DateTime ($request->getQueryStringParameter ('date'));
                $datestamp = $datetime->format (self::DATE_FORMAT);
            }
    
            $response->setOutput (
                \Linkz\Model\Events::getTutorByDate (
                    $datestamp,
                    $this->storage->get ('id'),
                    (int) ($request->getQueryStringParameter ('page') ?? 1),
                    $showCancelled
                )
            );
        }
    
        function post (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $response->setContentType (\Atatiki\HTTP\Response::JSON);

            switch ($this->storage->get ('type'))
            {
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT:
                    $this->parentCreateEvent ($request, $response);
                    break;
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR:
                    $this->tutorCreateEvent ($request, $response);
                    break;
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_STUDENT:
                default:
                    throw new NotAllowedException ('Not allowed');
            }
        }
        
        protected function createEvent (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $status = 'Not confirmed';
            $bodyRequest = $request->getBodyData ();
    
            if (array_key_exists ('contractId', $bodyRequest) === false)
            {
                throw new NotAllowedException ('The event booking request must contain a contract');
            }
    
            $contract = \Linkz\Model\Contracts::getContract ($bodyRequest ['contractId']);
            $contractRows = \Linkz\Model\Contracts::getRows ($bodyRequest ['contractId']);
            $tutorId = '';
    
            if ($this->storage->get ('type') == \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR)
            {
                $status = 'Waiting parent\'s confirmation';
                $tutorId = $this->storage->get ('id');
    
                if ($contract ['_id_Tutor'] != $tutorId)
                {
                    throw new NotAllowedException ('Permission error');
                }
            }
            elseif ($this->storage->get ('type') == \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT)
            {
                $status = 'Waiting tutor\'s confirmation';
                $tutorId = $contract ['_id_Tutor'];
                
                if (in_array ($contract ['_id_Student'], $this->storage->get ('students')) === false)
                {
                    throw new NotAllowedException ('The specified contract is not valid');
                }
            }
    
            if ($contract ['_id_Student'] == '')
            {
                throw new NotAllowedException ('The specified contract is not valid');
            }
    
            if (count ($contractRows) === 0)
            {
                throw new NotAllowedException ('The given contract doesn\'t have any rows in it');
            }
    
            // contract found, check if there is any booking at this date
            $eventsForTutor = \Linkz\Model\Events::getFullTutorByDate ($bodyRequest ['date'], $tutorId);
            $eventsForStudent = \Linkz\Model\Events::getFullStudentByDate ($bodyRequest ['date'], $contract ['_id_Student']);
            $eventsList = array_merge ($eventsForTutor, $eventsForStudent);
    
            // check that both have time available first
            $startTime = self::convertTimeToMinutes ($bodyRequest ['starttime']);
            $endTime = self::convertTimeToMinutes ($bodyRequest ['endtime']);
    
            // to make comparison easy, convert times to minutes
            if (count ($eventsList) > 0)
            {
                // check all the events first
                foreach ($eventsList as $event)
                {
                    $eventStartTime = self::convertTimeToMinutes ($event ['Ev101_StartTime']);
                    $eventEndTime = self::convertTimeToMinutes ($event ['Ev102_EndTime']);
            
                    if (
                        ($startTime <= $eventStartTime && $endTime >= $eventStartTime) ||
                        ($startTime <= $eventEndTime && $endTime >= $eventEndTime) ||
                        ($startTime >= $eventStartTime && $endTime <= $eventEndTime)
                    )
                    {
                        throw new NotAllowedException ('There is an event already present at that date for one or more persons specified in the booking');
                    }
                }
            }
    
            $startTimeFormated = self::convertMinutesToTime ($startTime);
            $endTimeFormated = self::convertMinutesToTime ($endTime);
            $recordData = array (
                '_id_Student' => $contract ['_id_Student'],
                '_id_Contract' => $contract ['_id'],
                '_id_ContractRow' => $contractRows [0] ['_id'],
                '_id_Tutor' => $contract ['_id_Tutor'],
                'Ev100_Date' => $bodyRequest ['date'],
                'Ev101_StartTime' => $startTimeFormated,
                'Ev102_EndTime' => $endTimeFormated,
                'Ev103_Subject' => $bodyRequest ['subject'],
                'Ev104_Description' => $bodyRequest ['description'],
                'Ev105_Status_ac' => $status
            );
    
            $creationResult = \Atatiki\Filemaker\Connection::makeConnection ()->createRecord (
                'Ev__Event',
                $recordData
            );
    
            if ($creationResult->isError () == true)
            {
                $creationResult->raiseError ();
            }
            
            \Linkz\Logging\LoggerFactory::getUserLogger ($this->storage->get ('type'), $this->storage->get ('id'))
                ->debug ('User created a new event', array ('data' => $recordData));
            
            // finally send notifications to the involved users
            $this->sendNotifications ($creationResult->getRecord (0), "EVENT_WAITING_CONFIRMATION");
        }
        
        private function sendNotifications (array $event, string $notification)
        {
            if ($this->storage->get ('type') === \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT)
            {
                $tutorNotifications =
                    \Linkz\Model\Notifications::makeFromStorage (
                        \Linkz\Storage\PermanentStorage::getTutorStorage ($event ['_id_Tutor'])
                    );
    
                $tutorNotifications->insertNewNotification (
                    $notification,
                    'event',
                    array (
                        'eventId' => $event ['_id'],
                        'eventSubject' => $event ['Ev103_Subject'],
                        'eventDate' => $event ['Ev100_Date'],
                        'eventStart' => $event ['Ev101_StartTime'],
                        'eventEnd' => $event ['Ev102_EndTime']
                    )
                );
                $tutorNotifications->persist ();
            }
            elseif ($this->storage->get ('type') === \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR)
            {
                $familyNotifications =
                    \Linkz\Model\Notifications::makeFromStorage (
                        \Linkz\Storage\PermanentStorage::getFamilyStorage (
                            \Linkz\Model\Students::get ($event ['_id_Student']) ['_id_Family']
                        )
                    );
        
                $familyNotifications->insertNewNotification (
                    $notification,
                    'event',
                    array (
                        'eventId' => $event ['_id'],
                        'eventSubject' => $event ['Ev103_Subject'],
                        'eventDate' => $event ['Ev100_Date'],
                        'eventStart' => $event ['Ev101_StartTime'],
                        'eventEnd' => $event ['Ev102_EndTime']
                    )
                );
                $familyNotifications->persist ();
            }
    
            $studentNotifications =
                \Linkz\Model\Notifications::makeFromStorage (
                    \Linkz\Storage\PermanentStorage::getStudentStorage (
                        $event ['_id_Student']
                    )
                );
    
            $studentNotifications->insertNewNotification (
                $notification,
                'event',
                array (
                    'eventId' => $event ['_id'],
                    'eventSubject' => $event ['Ev103_Subject'],
                    'eventDate' => $event ['Ev100_Date'],
                    'eventStart' => $event ['Ev101_StartTime'],
                    'eventEnd' => $event ['Ev102_EndTime']
                )
            );
            $studentNotifications->persist ();
        }
        
        function tutorCreateEvent (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $this->createEvent ($request, $response);
        }
        
        function parentCreateEvent (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $this->createEvent ($request, $response);
        }
        
        static function extractHour (string $timestring): int
        {
            return (int) substr ($timestring, 0, strpos ($timestring, ':'));
        }
        
        static function extractMinutes (string $timestring): int
        {
            // WARNING: WE RELY ON A PHP QUIRK, INTEGERS ARE CONVERTED UP UNTIL THE FIRST NON-NUMERAL CHARACTER IN THE STRING
            // THIS IS IMPORTANT FOR THIS FUNCTIONALITY AND ACCORDING TO PHP THIS SHOULDN'T CHANGE IN THE NEAR FUTURE
            // PLEASE UPDATE THIS FUNCTION IN CASE THIS BEHAVIOUR IS CHANGED SOMETIME
            return (int) substr ($timestring, strpos ($timestring, ':'));
        }
        
        static function convertTimeToMinutes (string $timestring)
        {
            $timestring = strtolower ($timestring);
            $minutes = self::extractMinutes ($timestring);
            
            if (strstr ($timestring, 'am') !== false)
            {
                $am = self::extractHour ($timestring);
                
                if ($am == 12)
                {
                    $am = 0;
                }
                
                $minutes += $am * 60;
            }
            elseif (strstr ($timestring, 'pm') !== false)
            {
                $pm = self::extractHour ($timestring);
                
                if ($pm == 12)
                {
                    $pm = 0;
                }
                
                $pm += 12;
                $minutes += $pm * 60;
            }
            else
            {
                $minutes += self::extractHour ($timestring) * 60;
            }
            
            return $minutes;
        }
        
        static function convertMinutesToTime (int $minutes)
        {
            $hours = (int) ($minutes / 60);
            $actualMinutes = $minutes - ($hours * 60);
            $indicator = "";
            
            if ($hours < 12)
            {
                $indicator = "AM";
            }
            else
            {
                $indicator = "PM";
                $hours -= 12;
            }
    
            if ($hours == 0)
            {
                $hours = 12;
            }
            
            return
                str_pad ((string) $hours, 2, '0', STR_PAD_LEFT) . ":" .
                str_pad ((string) $actualMinutes, 2, '0', STR_PAD_LEFT) . " " . $indicator;
        }
    
        function patch (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $response->setContentType (\Atatiki\HTTP\Response::JSON);

            switch ($this->storage->get ('type'))
            {
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT:
                    $this->parentUpdateEvent ($request, $response);
                    break;
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR:
                    $this->tutorUpdateEvent ($request, $response);
                    break;
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_STUDENT:
                default:
                    throw new NotAllowedException ('Not allowed');
            }
        }
        
        protected function updateEvent (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $status = 'Not confirmed';
            $bodyRequest = $request->getBodyData ();
    
            if (array_key_exists ('eventId', $bodyRequest) === false)
            {
                throw new NotAllowedException ('The event booking update request must contain a event');
            }
    
            if ($this->storage->get ('type') == \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR)
            {
                $status = 'Waiting parent\'s confirmation';
            }
            elseif ($this->storage->get ('type') == \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT)
            {
                $status = 'Waiting tutor\'s confirmation';
            }
    
            $eventId = $bodyRequest ['eventId'];
            $recordId = '-1';
            $event = \Linkz\Model\Events::get ($eventId, $recordId);
    
            if ($recordId == '-1')
            {
                throw new NotAllowedException ("Cannot find the given event");
            }
            // contract found, check if there is any booking at this date
            $eventsForTutor = \Linkz\Model\Events::getFullTutorByDate ($bodyRequest ['date'], $event ['_id_Tutor']);
            $eventsForStudent = \Linkz\Model\Events::getFullStudentByDate ($bodyRequest ['date'], $event ['_id_Student']);
            $eventsList = array_merge ($eventsForTutor, $eventsForStudent);
    
            // check that both have time available first
            $startTime = self::convertTimeToMinutes ($bodyRequest ['starttime']);
            $endTime = self::convertTimeToMinutes ($bodyRequest ['endtime']);
    
            // to make comparison easy, convert times to minutes
            if (count ($eventsList) > 0)
            {
                // check all the events first
                foreach ($eventsList as $event)
                {
                    $eventStartTime = self::convertTimeToMinutes ($event ['Ev101_StartTime']);
                    $eventEndTime = self::convertTimeToMinutes ($event ['Ev102_EndTime']);
            
                    if (
                        ($startTime <= $eventStartTime && $endTime >= $eventStartTime) ||
                        ($startTime <= $eventEndTime && $endTime >= $eventEndTime) ||
                        ($startTime >= $eventStartTime && $endTime <= $eventEndTime)
                    )
                    {
                        throw new NotAllowedException ('There is an event already present at that date for one or more persons specified in the booking');
                    }
                }
            }
    
            $startTimeFormated = self::convertMinutesToTime ($startTime);
            $endTimeFormated = self::convertMinutesToTime ($endTime);
            $recordData = array (
                'Ev100_Date' => $bodyRequest ['date'],
                'Ev101_StartTime' => $startTimeFormated,
                'Ev102_EndTime' => $endTimeFormated,
                'Ev105_Status_ac' => $status
            );
    
            $creationResult = \Atatiki\Filemaker\Connection::makeConnection ()->updateRecord (
                $recordId,
                'Ev__Event',
                $recordData
            );
    
            if ($creationResult->isError () == true)
            {
                $creationResult->raiseError ();
            }
            
            $event = \Linkz\Model\Events::get ($eventId);
            
            \Linkz\Logging\LoggerFactory::getUserLogger ($this->storage->get ('type'), $this->storage->get ('id'))
                ->debug ('User updated an event', array ('data' => $recordData, 'recordId' => $recordId, 'eventId' => $eventId));
            
            // finally send notifications to the involved users
            $this->sendNotifications ($event, "EVENT_UPDATE_REQUEST");
        }
        
        function tutorUpdateEvent (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $this->updateEvent ($request, $response);
        }
        
        function parentUpdateEvent (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $this->updateEvent ($request, $response);
        }
    };
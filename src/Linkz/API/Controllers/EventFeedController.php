<?php declare(strict_types=1);
    namespace Linkz\API\Controllers;

    use \Linkz\API\Exceptions\NotAllowedException;

    class EventFeedController extends \Atatiki\API\Controllers\Base\SecuredController implements \Atatiki\API\Controllers\Base\GetHandler
    {
        const RECORDS_PER_PAGE = 20;
        const DATE_FORMAT = 'm/d/Y';
        
        use \Atatiki\API\Controllers\Base\PaginationTrait;
        
        function get(\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $response->setContentType (\Atatiki\HTTP\Response::JSON);

            switch ($this->storage->get ('type'))
            {
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT:
                    $this->getParentEvents ($request, $response);
                    break;
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_STUDENT:
                    $this->getStudentEvents ($request, $response);
                    break;
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR:
                    $this->getTutorEvents ($request, $response);
                    break;
                default:
                    throw new NotAllowedException ('Not allowed');
            }
        }

        function getParentEvents (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            // check if we have any query string parameters for filters
            $ignoreStudents = array ();
            $showCancelled = ($request->getQueryStringParameter ('showCancelled') ?? 'true') == 'true';

            if (is_array ($request->getQueryStringParameter ('ignoreStudents')) === true)
            {
                $ignoreStudents = $request->getQueryStringParameter ('ignoreStudents');
            }

            $students = array_values (array_diff ($this->storage->get ('students'), $ignoreStudents));
            
            $response->setOutput (
                \Linkz\Model\Events::getParentFeed (
                    $students,
                    (int) ($request->getQueryStringParameter ('page') ?? 1),
                    $showCancelled
                )
            );
        }

        function getStudentEvents (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $showCancelled = ($request->getQueryStringParameter ('showCancelled') ?? 'true') == 'true';
            
            $response->setOutput (
                \Linkz\Model\Events::getStudentFeed (
                    $this->storage->get ('id'),
                    (int) ($request->getQueryStringParameter ('page') ?? 1),
                    $showCancelled
                )
            );
        }

        function getTutorEvents (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $showCancelled = ($request->getQueryStringParameter ('showCancelled') ?? 'true') == 'true';
            
            $response->setOutput (
                \Linkz\Model\Events::getTutorFeed (
                    $this->storage->get ('id'),
                    (int) ($request->getQueryStringParameter ('page') ?? 1),
                    $showCancelled
                )
            );
        }
    };
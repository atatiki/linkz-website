<?php declare(strict_types=1);
    namespace Linkz\API\Controllers;
    
    class EventStatusUpdateController extends \Atatiki\API\Controllers\Base\SecuredController implements \Atatiki\API\Controllers\Base\PostHandler
    {
        function post (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $response->setContentType (\Atatiki\HTTP\Response::JSON);
            $recordID = '-1';
            $event = \Linkz\Model\Events::get ($request->getParameter ('id') ?? '', $recordID);
            
            if ($recordID == '-1')
            {
                throw new \Linkz\API\Exceptions\NotAllowedException ('You are not allowed to modify this event');
            }
            
            switch ($request->getParameter ('action'))
            {
                case 'confirm':
                    $this->confirmEvent ($event, $recordID);
                    break;
                case 'cancel':
                    $this->cancelEvent ($event, $recordID, $request->getBodyData () ['reason']);
                    break;
                    
                default:
                    throw new \Linkz\API\Exceptions\NotAllowedException ('Specified action is not valid');
            }
        }
    
        private function checkEventBelongsToUs (array $event)
        {
            // make sure that it we are allowed to change it's status
            switch ($this->storage->get ('type'))
            {
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT:
                    break;
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR:
                    break;
        
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_STUDENT:
                default:
                    throw new \Linkz\API\Exceptions\NotAllowedException ('You are not allowed to modify this event');
                    break;
            }
        }
        
        private function sendNotifications (array $event, string $notification, string $reason = '')
        {
            if ($this->storage->get ('type') === \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT)
            {
                $tutorNotifications =
                    \Linkz\Model\Notifications::makeFromStorage (
                        \Linkz\Storage\PermanentStorage::getTutorStorage ($event ['_id_Tutor'])
                    );
    
                $tutorNotifications->insertNewNotification (
                    $notification,
                    'event',
                    array (
                        'eventId' => $event ['_id'],
                        'eventSubject' => $event ['Ev103_Subject'],
                        'eventDate' => $event ['Ev100_Date'],
                        'eventStart' => $event ['Ev101_StartTime'],
                        'eventEnd' => $event ['Ev102_EndTime'],
                        'reason' => $reason
                    )
                );
                $tutorNotifications->persist ();
            }
            elseif ($this->storage->get ('type') === \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR)
            {
                $familyNotifications =
                    \Linkz\Model\Notifications::makeFromStorage (
                        \Linkz\Storage\PermanentStorage::getFamilyStorage (
                            \Linkz\Model\Students::get ($event ['_id_Student']) ['_id_Family']
                        )
                    );
        
                $familyNotifications->insertNewNotification (
                    $notification,
                    'event',
                    array (
                        'eventId' => $event ['_id'],
                        'eventSubject' => $event ['Ev103_Subject'],
                        'eventDate' => $event ['Ev100_Date'],
                        'eventStart' => $event ['Ev101_StartTime'],
                        'eventEnd' => $event ['Ev102_EndTime'],
                        'reason' => $reason
                    )
                );
                $familyNotifications->persist ();
            }
    
            $studentNotifications =
                \Linkz\Model\Notifications::makeFromStorage (
                    \Linkz\Storage\PermanentStorage::getStudentStorage (
                        $event ['_id_Student']
                    )
                );
    
            $studentNotifications->insertNewNotification (
                $notification,
                'event',
                array (
                    'eventId' => $event ['_id'],
                    'eventSubject' => $event ['Ev103_Subject'],
                    'eventDate' => $event ['Ev100_Date'],
                    'eventStart' => $event ['Ev101_StartTime'],
                    'eventEnd' => $event ['Ev102_EndTime'],
                    'reason' => $reason
                )
            );
            $studentNotifications->persist ();
        }
        
        private function cancelEvent (array $event, string $recordID, string $reason)
        {
            $description = $event ['Ev104_Description'];
            $status = 'Not confirmed';
            $unixtime =
                date_create_from_format (
                    'm/d/Y',
                    $event ['Ev100_Date'],
                    new \DateTimeZone ('UTC')
                )->getTimestamp () + (\Linkz\API\Controllers\EventsController::convertTimeToMinutes ($event ['Ev101_StartTime']) * 60);
            $diff = $unixtime - time ();
            
            if ($diff <= 86400)
            {
                $description = $event ['Ev104_Description'] . "\r\nCanceled withing 24 hours";
                $status = 'Canceled within 24h';
            }
            
            $this->checkEventBelongsToUs ($event);
            $this->sendNotifications ($event, 'EVENT_NOT_CONFIRMED', $reason);
    
            $update = array ('Ev105_Status_ac' => $status, 'Ev107_CancellationReason' => $reason, 'Ev104_Description' => $description);
            
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $result = $fm->updateRecord ($recordID, 'Ev__Event', $update);
            
            if ($result->isError () === true)
            {
                $result->raiseError ();
            }
            
            \Linkz\Logging\LoggerFactory::getUserLogger ($this->storage->get ('type'), $this->storage->get ('id'))
                ->debug ('User cancelled event', array ('eventId' => $event ['_id']));
        }
    
        private function confirmEvent (array $event, string $recordID)
        {
            $this->checkEventBelongsToUs ($event);
            $this->sendNotifications ($event, 'EVENT_CONFIRMED');
            
            $update = array ('Ev105_Status_ac' => 'Confirmed');
            
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $result = $fm->updateRecord ($recordID, 'Ev__Event', $update);
            
            if ($result->isError () === true)
            {
                $result->raiseError ();
            }
            
            \Linkz\Logging\LoggerFactory::getUserLogger ($this->storage->get ('type'), $this->storage->get ('id'))
                ->debug ('User confirmed event', array ('eventId' => $event ['_id']));
        }
    };
<?php declare (strict_types=1);
    namespace Linkz\API\Controllers;

    class EventFileRemoveController extends \Atatiki\API\Controllers\Base\SecuredController implements \Atatiki\API\Controllers\Base\PostHandler
    {
        function post (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $response->setContentType (\Atatiki\HTTP\Response::JSON);
            
            $recordID = '-1';
            $file = \Linkz\Model\Events::getFile (
                $request->getParameter ('id') ?? '',
                $request->getParameter ('fileId') ?? '',
                $recordID
            );
    
            $userId = $this->storage->get ('id');
            
            if (
                (
                    ($this->storage->get ('type') == \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR && $userId == $file ['_id_TutorWeb']) == false &&
                    ($this->storage->get ('type') == \Atatiki\API\Controllers\Base\SecuredController::TYPE_STUDENT && $userId == $file ['_id_StudentWeb']) == false
                ) || $recordID == '-1'
            )
            {
                throw new \Linkz\API\Exceptions\NotAllowedException ("You are not allowed to do this");
            }
            
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $removeResult = $fm->deleteRecord ($recordID, 'Fi__File');
            
            if ($removeResult->isError () == true)
            {
                $removeResult->raiseError ();
            }
            
            \Linkz\Logging\LoggerFactory::getUserLogger ($this->storage->get ('type'), $this->storage->get ('id'))
                ->info ("User removed file", array ('fileId' => $file ['_id']));
        }
    };
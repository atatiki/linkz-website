<?php declare(strict_types=1);
    namespace Linkz\API\Controllers;

    use \Linkz\API\Exceptions\NotAllowedException;

    class InvoiceDetailsController extends \Atatiki\API\Controllers\Base\SecuredController implements \Atatiki\API\Controllers\Base\GetHandler
    {
        /**
         * @inheritdoc
         * @throws \Exception
         */
        function get(\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            if ($this->storage->get ('type') !== \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT)
            {
                throw new NotAllowedException ("Only parent's allowed");
            }
            
            $invoiceId = $request->getParameter ('invoiceId') ?? '';

            $response->setContentType (\Atatiki\HTTP\Response::JSON);
            $response->setOutput (
                array (
                    'data' => $this->getInvoiceData ($invoiceId),
                    'rows' => \Linkz\Model\Invoice::getRows ($invoiceId),
                    'payments' => \Linkz\Model\Invoice::getPayments ($invoiceId)
                )
            );
        }
        
        private function getInvoiceData (string $id)
        {
            $invoice = \Linkz\Model\Invoice::get ($id);
            
            if ($invoice ['_id_Student'] != '' && in_array ($invoice ['_id_Student'], $this->storage->get ('students')) === false)
            {
                throw new NotAllowedException ('Permission denied');
            }
            
            return $invoice;
        }
    };
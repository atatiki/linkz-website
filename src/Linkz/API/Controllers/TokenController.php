<?php declare (strict_types=1);
    namespace Linkz\API\Controllers;

    class TokenController extends \Atatiki\API\Controllers\Base\SecuredController implements \Atatiki\API\Controllers\Base\GetHandler
    {
        function get (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $response->setContentType (\Atatiki\HTTP\Response::JSON);

            // only refresh the token if it's going to expire in 15 minutes or less
            if ($this->storage->get ('expire_time') < strtotime ('+15 minutes'))
            {
                $this->storage->set ('expire_time', strtotime ("+8 hours"));
            }

            \Linkz\Logging\LoggerFactory::getUserLogger ($this->storage->get ('type'), $this->storage->get ('id'))
                ->debug ("User refreshed the token", array ('token' => $this->storage->getAuthKey ()));
            
            if (getenv ('APP_ENVIRONMENT') === 'dev')
            {
                $response->setOutput (
                    array (
                        'expiretime' => $this->storage->get ('expire_time'),
                        'logintime' => $this->storage->get ('login_time', time ()),
                        'username' => $this->storage->get ('username'),
                        'id' => $this->storage->get ('id'),
                        'family_id' => $this->storage->get ('family_id'),
                        'firstname' => $this->storage->get ('firstname'),
                        'lastname' => $this->storage->get ('lastname'),
                        'students' => $this->storage->get ('students'),
                        'type' => $this->storage->get ('type'),
                    )
                );
            }
            else
            {
                $response->setOutput (array ('expiretime' => $this->storage->get ('expire_time')));
            }
        }
    };
<?php declare (strict_types=1);
    namespace Linkz\API\Controllers;

    class CancellationReasonsController extends \Atatiki\API\Controllers\Base\SecuredController implements \Atatiki\API\Controllers\Base\GetHandler
    {
        /**
         * @inheritdoc
         * @throws \Exception
         */
        function get(\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $response->setContentType (\Atatiki\HTTP\Response::JSON);
            
            $connection = \Atatiki\Filemaker\Connection::makeConnection ();
            $result = $connection->layout ('Cnr__CancellationReason');
            
            if ($result->isError () == true)
            {
                $result->raiseError ();
            }
            
            $response->setOutput ($result->getData ());
        }
    };

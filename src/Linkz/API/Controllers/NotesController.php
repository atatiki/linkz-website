<?php declare (strict_types=1);
    namespace Linkz\API\Controllers;

    class NotesController extends \Atatiki\API\Controllers\Base\SecuredController implements \Atatiki\API\Controllers\Base\PostHandler
    {
        const DATE_FORMAT = 'm/d/Y';
        
        function post (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $response->setContentType (\Atatiki\HTTP\Response::JSON);
            
            if ($this->storage->get ('type') !== \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR)
            {
                throw new \Linkz\API\Exceptions\NotAllowedException ('Only tutors can use this functionality');
            }
            
            if ($request->getBodyData () === null)
            {
                throw new \Linkz\API\Exceptions\NotAllowedException ('Not received any content, expected note information in the body data');
            }
            
            $event = \Linkz\Model\Events::get ($request->getParameter ('eventId') ?? '');
            
            // first check that the event exists
            if ($event == array ())
            {
                throw new \Exception ('Cannot create a note for a non-existant event');
            }
            
            $creationResult = \Atatiki\Filemaker\Connection::makeConnection ()->createRecord (
                'No__Notes',
                array (
                    '_id_Event' => $event ['_id'],
                    'No101_Description' => $request->getBodyData () ['note'],
                    'No100_Date' => date (self::DATE_FORMAT),
                    'No300_VisibleToStudent' => (int) $request->getBodyData () ['visibleToStudent'],
                    'No301_VisibleToParent' => (int) $request->getBodyData () ['visibleToParent']
                )
            );
            
            if ($creationResult->isError () == true)
            {
                $creationResult->raiseError ();
            }
            
            \Linkz\Logging\LoggerFactory::getUserLogger ($this->storage->get ('type'), $this->storage->get ('id'))
                ->debug ("User created note", array ('content' => $request->getBodyData ['note'], 'eventId' => $event ['_id']));
        }
        
        function doesEventExist (string $eventId)
        {
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $sqlQuery = $fm->prepareSqlQuery (
                "SELECT * WHERE '_id' LIKE :id",
                array (':id' => $eventId)
            );
            $result = $fm->layoutSQL ('Ev__Event', $sqlQuery);
            
            if ($result->isError () === true)
            {
                $result->raiseError ();
            }
            
            if ($result->isEmpty () === true)
            {
                return false;
            }
            
            return true;
        }
    };
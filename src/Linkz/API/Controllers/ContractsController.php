<?php declare(strict_types=1);
    namespace Linkz\API\Controllers;

    class ContractsController
        extends \Atatiki\API\Controllers\Base\SecuredController
        implements \Atatiki\API\Controllers\Base\GetHandler,
                    \Atatiki\API\Controllers\Base\PostHandler
    {
        const DATE_FORMAT = 'm/d/Y';
        const RECORDS_PER_PAGE = 20;
        use \Atatiki\API\Controllers\Base\PaginationTrait;
        
        /**
         * @inheritdoc
         * @throws \Linkz\API\Exceptions\NotAllowedException
         */
        function get(\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $response->setContentType (\Atatiki\HTTP\Response::JSON);
            
            switch ($this->storage->get ('type'))
            {
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT:
                    $this->getParentContracts ($request, $response);
                    break;
                    
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR:
                    $this->getTutorContracts ($request, $response);
                    break;
                    
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_STUDENT:
                default:
                    throw new \Linkz\API\Exceptions\NotAllowedException ('Only parents and tutors allowed');
                    break;
            }
        }
        
        /**
         * Fetches a paginated list of contracts related to the parent's students
         *
         * @param \Atatiki\HTTP\Request  $request
         * @param \Atatiki\HTTP\Response $response
         *
         * @throws \Exception
         */
        function getParentContracts (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $response->setOutput (
                \Linkz\Model\Contracts::getByFamily (
                    $this->storage->get ('students'),
                    $this->storage->get ('family_id'),
                    (int) ($request->getQueryStringParameter ('page') ?? 1),
                    (int) ($request->getQueryStringParameter ('ignoreempty') ?? 0)
                )
            );
        }
        
        function getTutorContracts (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $response->setOutput (
                \Linkz\Model\Contracts::getByTutor (
                    $this->storage->get ('id'),
                    (int) ($request->getQueryStringParameter ('page') ?? 1),
                    (int) ($request->getQueryStringParameter ('ignoreempty') ?? 0)
                )
            );
        }
        
        function post (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $response->setContentType (\Atatiki\HTTP\Response::JSON);
    
            switch ($this->storage->get ('type'))
            {
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT:
                    $this->parentPlaceBooking ($request, $response);
                    break;
        
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR:
                    $this->tutorPlaceBooking ($request, $response);
                    break;
        
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_STUDENT:
                default:
                    throw new \Linkz\API\Exceptions\NotAllowedException ('Only parents and tutors allowed');
                    break;
            }
        }
    
        function parentPlaceBooking (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
        
        }
    
        function tutorPlaceBooking (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
        
        }
    };
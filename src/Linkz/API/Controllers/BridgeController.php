<?php declare(strict_types=1);
    namespace Linkz\API\Controllers;

    use Linkz\API\Exceptions\NotAllowedException;

    class BridgeController extends \Atatiki\API\Controllers\Base\SecuredController implements \Atatiki\API\Controllers\Base\GetHandler
    {
        function get (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            switch ($this->storage->get ('type'))
            {
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT:
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_STUDENT:
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR:
                    $this->fetchURL ($request, $response);
                    break;

                default:
                    throw new NotAllowedException ('Only logged-in users can use this endpoint');
            }
        }

        function fetchURL (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response)
        {
            $starttime = microtime (true);
            
            if ($request->hasQueryStringParameter ('url') === false)
            {
                throw new NotAllowedException ('This endpoint needs to be supplied an URL');
            }

            $url = $request->getQueryStringParameter ('url');
            
            $userLogger = \Linkz\Logging\LoggerFactory::getUserLogger ($this->storage->get ('type'), $this->storage->get ('id'));

            // some security checks over the URL to make sure it belongs to our server
            if (strstr ($url, '/fmi/xml/cnt/') === false)
            {
                $userLogger->error ("User tried to request a invalid file trough the bridge", array ('url' => $url));
                throw new NotAllowedException ('The specified URL is not valid');
            }
            
            $userLogger->debug ("User requesting file trough the bridge", array ('url' => $url));

            // replace the server's url with our own url with username and password
            $url =
                getenv ("FM_PROTOCOL") . "://" .
                urlencode (getenv ("FM_USERNAME")) . ":" . urlencode (getenv ("FM_PASSWORD")) . "@" .
                getenv ("FM_SERVER") .
                substr ($url, strpos ($url, '/fmi/'));

            $contentTypeHeader = '';
            $curl = curl_init ($url);

            curl_setopt ($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt ($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt ($curl, CURLOPT_HEADERFUNCTION, function ($curl, $header) use (&$contentTypeHeader)
            {
                $exploded = explode (':', $header, 2);

                if (count ($exploded) < 2)
                    return strlen ($header);

                $header_name = strtolower (trim ($exploded [0]));

                if ($header_name == 'content-type')
                {
                    $contentTypeHeader = strtolower (trim ($exploded [1]));
                }

                return strlen ($header);
            });

            $curlResponse = curl_exec ($curl);

            curl_close ($curl);

            if ($curlResponse === false)
            {
                throw new \Exception ('Invalid server response');
            }

            $response->setContentType ($contentTypeHeader);
            $response->setOutput ($curlResponse);
            
            $endtime = microtime (true);
            
            \Linkz\Logging\LoggerFactory::getSystemLogger ()
                                        ->debug (
                                            'Request to Filemaker took ' . floor (($endtime - $starttime) * 1000) . ' ms',
                                            array (
                                                'url' => $request->getQueryStringParameter ('url')
                                            )
                                        );
        }
    };
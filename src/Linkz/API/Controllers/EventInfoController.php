<?php declare (strict_types=1);
    namespace Linkz\API\Controllers;

    use \Linkz\API\Exceptions\NotAllowedException;

    class EventInfoController extends \Atatiki\API\Controllers\Base\SecuredController implements \Atatiki\API\Controllers\Base\GetHandler
    {
        function get(\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $response->setContentType (\Atatiki\HTTP\Response::JSON);

            switch ($this->storage->get ('type'))
            {
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT:
                    $this->getParentEvent ($request, $response);
                    break;
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_STUDENT:
                    $this->getStudentEvent ($request, $response);
                    break;
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR:
                    $this->getTutorEvent ($request, $response);
                    break;
                default:
                    throw new NotAllowedException ('Not allowed');
            }
        }

        function getParentEvent (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $event = \Linkz\Model\Events::get ($request->getParameter ('id') ?? '');

            $response->setOutput (
                array (
                    'tutor' => \Linkz\Model\Tutor::get ($event ['_id_Tutor']),
                    'event' => $event,
                    'files' => \Linkz\Model\Events::getFiles ($event ['_id']),
                    'notes' => \Linkz\Model\Events::getNotes ($event ['_id'], \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT)
                )
            );
        }

        function getStudentEvent (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $event = \Linkz\Model\Events::get ($request->getParameter ('id') ?? '');
    
            $response->setOutput (
                array (
                    'tutor' => \Linkz\Model\Tutor::get ($event ['_id_Tutor']),
                    'event' => $event,
                    'files' => \Linkz\Model\Events::getFiles ($event ['_id']),
                    'notes' => \Linkz\Model\Events::getNotes ($event ['_id'], \Atatiki\API\Controllers\Base\SecuredController::TYPE_STUDENT)
                )
            );
        }

        function getTutorEvent (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $event = \Linkz\Model\Events::get ($request->getParameter ('id') ?? '');

            $response->setOutput (
                array (
                    'event' => $event,
                    'student' => \Linkz\Model\Students::get ($event ['_id_Student']),
                    'files' => \Linkz\Model\Events::getFiles ($event ['_id']),
                    'notes' => \Linkz\Model\Events::getNotes ($event ['_id'], \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR)
                )
            );
        }
    };
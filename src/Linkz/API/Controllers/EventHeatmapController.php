<?php declare(strict_types=1);
    namespace Linkz\API\Controllers;
    
    class EventHeatmapController
        extends \Atatiki\API\Controllers\Base\SecuredController
        implements \Atatiki\API\Controllers\Base\GetHandler
    {
        function get (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $response->setContentType (\Atatiki\HTTP\Response::JSON);
            $timestamp = time ();
            
            if ($request->hasQueryStringParameter ('date') === true)
            {
                $datetime = new \DateTime ($request->getQueryStringParameter ('date'));
                $timestamp = $datetime->getTimestamp ();
            }
            
            $start = strtotime ('first day of this month', $timestamp);
            $end = strtotime ('last day of this month', $timestamp);
            $datefilter = date ('m/d/Y', $start) . '...' . date ('m/d/Y', $end);
            
            switch ($this->storage->get ('type'))
            {
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT:
                    $response->setOutput (
                        \Linkz\Model\Events::getParentByDate (
                            $datefilter,
                            $this->storage->get ('students'),
                            1,
                            false
                        )
                    );
                    break;
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_STUDENT:
                    $response->setOutput (
                        \Linkz\Model\Events::getStudentByDate (
                            $datefilter,
                            $this->storage->get ('id'),
                            1,
                            false
                        )
                    );
                    break;
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR:
                    $response->setOutput (
                        \Linkz\Model\Events::getTutorByDate (
                            $datefilter,
                            $this->storage->get ('id'),
                            1,
                            false
                        )
                    );
                    break;
                default:
                    throw new \NotAllowedException ('Not allowed');
            }
        }
    };
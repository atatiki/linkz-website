<?php declare (strict_types=1);
    namespace Linkz\API\Controllers;
    
    use Atatiki\Storage\SessionStorage;

    class LoginController extends \Atatiki\API\Controllers\Base\BaseController implements \Atatiki\API\Controllers\Base\PostHandler
    {
        /**
         * @inheritdoc
         * @throws \Exception If any error occurred
         */
        function post (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $response->setContentType (\Atatiki\HTTP\Response::JSON);

            if ($request->hasHeader ('Authorization') === false)
            {
                throw new \Exception ("Expected OAuth token", \Atatiki\HTTP\Response::ERROR_AUTH);
            }
            
            $value = $request->getHeader ('Authorization');
            
            if (strncmp ($value, 'Basic ', strlen ('Basic ')) !== 0)
            {
                throw new \Exception ("Expected Basic OAuth token", \Atatiki\HTTP\Response::ERROR_AUTH);
            }
            
            $basic = substr ($value, strlen ('Basic '));
            $basic = base64_decode ($basic);
            $username = urldecode (substr ($basic, 0, strpos ($basic, ':')));
            $password = urldecode (substr ($basic, strpos ($basic, ':') + 1));

            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            
            $sqlQuery = $fm->prepareSqlQuery (
                'SELECT * WHERE Pe150_Username = :username AND Pe151_Password = :password',
                array (
                    ':username' => $username,
                    ':password' => strtoupper (hash ('sha256', $password))
                )
            );
            
            $result = $fm->layoutSQL ('Pe__Person_Login', $sqlQuery);

            if ($result->isError () === false && $result->isEmpty () === false)
            {
                $this->performParentLogin ($response, $result);
                return;
            }
            
            $sqlQuery = $fm->prepareSqlQuery (
                'SELECT * WHERE St150_Username = :username AND St151_Password = :password',
                array (
                    ':username' => $username,
                    ':password' => strtoupper (hash ('sha256', $password))
                )
            );
            
            $result = $fm->layoutSQL ('St__Student_Login', $sqlQuery);
            
            if ($result->isError () === false && $result->isEmpty () === false)
            {
                $this->performStudentLogin ($response, $result);
                return;
            }
            
            $sqlQuery = $fm->prepareSqlQuery (
                'SELECT * WHERE Tu150_Username = :username AND Tu151_Password = :password',
                array (
                    ':username' => $username,
                    ':password' => strtoupper (hash ('sha256', $password))
                )
            );
            
            $result = $fm->layoutSQL ('Tu__Tutor_Login', $sqlQuery);
            
            if ($result->isError () === false && $result->isEmpty () === false)
            {
                $this->performTutorLogin ($response, $result);
                return;
            }
            
            \Linkz\Logging\LoggerFactory::getSystemLogger ()->alert (
                "Login failed",
                array (
                    'username' => $username,
                    'password' => strtoupper (hash ('sha256', $password))
                )
            );
            
            throw new \Exception ('Cannot find any account linked to this username and password');
        }
        
        function performParentLogin (\Atatiki\HTTP\Response $response, \Atatiki\Filemaker\ResultSet $result)
        {
            // username and password found, setup session data
            $authKey = SessionStorage::generateAuthKey ();
            $storage = SessionStorage::makeStorage ($authKey);
            $record = $result->getRecord (0);
            $students = $this->fetchStudentsListForFamily ($record ['_id_Family']);
    
            $storage->set ('login_time', time ());
            $storage->set ('expire_time', strtotime ("+8 hours"));
            $storage->set ('username', $record ['Pe150_Username']);
            $storage->set ('id', $record ['_id']);
            $storage->set ('family_id', $record ['_id_Family']);
            $storage->set ('firstname', $record ['Pe100_FirstName']);
            $storage->set ('lastname', $record ['Pe101_LastName']);
            $storage->set ('students', $students);
            $storage->set ('type', \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT);
    
            $response->setOutput (
                array (
                    'status' => 'ok',
                    'type' => \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT,
                    'token' => $authKey,
                    'expiretime' => $storage->get ('expire_time')
                )
            );
            
            \Linkz\Logging\LoggerFactory::getUserLogger (\Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT, $record ['_id'])
                                        ->info ('User logged in correctly', array ('username' => $record ['Pe150_Username']));
        }
        
        function performStudentLogin (\Atatiki\HTTP\Response $response, \Atatiki\Filemaker\ResultSet $result)
        {
            $authKey = SessionStorage::generateAuthKey ();
            $storage = SessionStorage::makeStorage ($authKey);
            $record = $result->getRecord (0);
            
            $storage->set ('login_time', time ());
            $storage->set ('expire_time', strtotime ("+8 hours"));
            $storage->set ('username', $record ['St150_Username']);
            $storage->set ('id', $record ['_id']);
            $storage->set ('family_id', $record ['_id_Family']);
            $storage->set ('firstname', $record ['St100_FirstName']);
            $storage->set ('lastname', $record ['St101_LastName']);
            $storage->set ('type', \Atatiki\API\Controllers\Base\SecuredController::TYPE_STUDENT);
            
            $response->setOutput (
                array (
                    'status' => 'ok',
                    'type' => \Atatiki\API\Controllers\Base\SecuredController::TYPE_STUDENT,
                    'token' => $authKey,
                    'expiretime' => $storage->get ('expire_time')
                )
            );
            
            \Linkz\Logging\LoggerFactory::getUserLogger (\Atatiki\API\Controllers\Base\SecuredController::TYPE_STUDENT, $record ['_id'])
                                        ->info ('User logged in correctly', array ('username' => $record ['St150_Username']));
        }
        
        function performTutorLogin (\Atatiki\HTTP\Response $response, \Atatiki\Filemaker\ResultSet $result)
        {
            $authKey = SessionStorage::generateAuthKey ();
            $storage = SessionStorage::makeStorage ($authKey);
            $record = $result->getRecord (0);
            
            $storage->set ('login_time', time ());
            $storage->set ('expire_time', strtotime ('+8 hours'));
            $storage->set ('username', $record ['Tu150_Username']);
            $storage->set ('id', $record ['_id']);
            $storage->set ('firstname', $record ['Tu100_FirstName']);
            $storage->set ('lastname', $record ['Tu101_LastName']);
            $storage->set ('type', \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR);
            
            $response->setOutput (
                array (
                    'status' => 'ok',
                    'type' => \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR,
                    'token' => $authKey,
                    'expiretime' => $storage->get ('expire_time')
                )
            );
            
            \Linkz\Logging\LoggerFactory::getUserLogger (\Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR, $record ['_id'])
                                        ->info ('User logged in correctly', array ('username' => $record ['Tu150_Username']));
        }

        /**
         * Returns a list of all the student's id this family has
         *
         * @param string $familyID The family to get the students for
         * @return array The list of id's for students
         * @throws \Exception
         */
        function fetchStudentsListForFamily (string $familyID)
        {
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();

            // fetch all the students that belong to this family
            $sqlQuery = $fm->prepareSqlQuery (
                "SELECT * WHERE _id_Family LIKE :family",
                array (':family' => $familyID)
            );

            $result = $fm->layoutSQL ('St__Student', $sqlQuery);

            if ($result->isError () === true)
            {
                throw new \Exception ('Error when fetching basic user data. Login aborted');
            }

            $studentsList = array ();

            if ($result->isEmpty () === false)
            {
                foreach ($result as $student)
                {
                    $studentsList [] = $student ['_id'];
                }
            }

            return $studentsList;
        }
    };
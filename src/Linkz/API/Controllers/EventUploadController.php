<?php declare(strict_types=1);
    namespace Linkz\API\Controllers;

    class EventUploadController extends \Atatiki\API\Controllers\Base\SecuredController implements \Atatiki\API\Controllers\Base\PostHandler
    {
        function post (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $response->setContentType (\Atatiki\HTTP\Response::JSON);
            
            if (array_key_exists ('file', $_FILES) === false)
            {
                throw new \Exception ("No files given");
            }
            
            $publicFilePath = getenv ('FILE_URL_PATH') . basename ($_FILES ['file']['name']);
            $publicFileUrl = getenv ('FILE_URL_BASE') . basename ($_FILES ['file']['name']);
            
            move_uploaded_file ($_FILES ['file']['tmp_name'], $publicFilePath);
            
            $scriptParam = array (
                'url' => $publicFileUrl,
                'eventId' => $request->getParameter ('id'),
            );
            
            switch ($this->storage->get ('type'))
            {
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR:
                    $scriptParam ['tutorId'] = $this->storage->get ('id');
                    break;
                    
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_STUDENT:
                    $scriptParam ['studentId'] = $this->storage->get ('id');
                    break;
                    
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT:
                default:
                    throw new \Linkz\API\Exceptions\NotAllowedException ("Parents are not allowed");
                    break;
            }
            
            \Linkz\Logging\LoggerFactory::getUserLogger ($this->storage->get ('type'), $this->storage->get ('id'))
                ->debug ('User requested file upload', array ('url' => $publicFileUrl, 'path' => $publicFilePath));
            
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $result = $fm->script ('api.uploadFileToEvent', 'Fi__File', $scriptParam);
            
            if ($result->isError () === true)
            {
                $result->raiseError ();
            }
            
            if ($result->isEmpty () === true)
            {
                throw new \Exception ('Unexpected error when loading the file into the database');
            }
            
            $record = $result->getRecord (0);
            
            if (array_key_exists ('_id_Event', $record) === false)
            {
                \Linkz\Logging\LoggerFactory::getUserLogger ($this->storage->get ('type'), $this->storage->get ('id'))
                    ->debug ('File upload failed', array ('url' => $publicFileUrl, 'path' => $publicFilePath));
                
                throw new \Linkz\API\Exceptions\NotAllowedException ('Error inserting the file: ' . $record ['at.RESTfmResult']);
            }
            
            $response->setOutput ($record);
        }
    };
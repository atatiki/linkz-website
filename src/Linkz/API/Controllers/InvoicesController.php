<?php declare (strict_types=1);
    namespace Linkz\API\Controllers;

    use \Linkz\API\Exceptions\NotAllowedException;

    class InvoicesController extends \Atatiki\API\Controllers\Base\SecuredController implements \Atatiki\API\Controllers\Base\GetHandler
    {
        /**
         * @inheritdoc
         * @throws NotAllowedException
         */
        function get (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
        }
    };
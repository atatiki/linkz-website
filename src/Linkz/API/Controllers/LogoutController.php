<?php declare (strict_types=1);
    namespace Linkz\API\Controllers;

    class LogoutController extends \Atatiki\API\Controllers\Base\SecuredController implements \Atatiki\API\Controllers\Base\GetHandler
    {
        function get (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            \Linkz\Logging\LoggerFactory::getUserLogger ($this->storage->get ('type'), $this->storage->get ('id'))
                ->debug ('User logged out');
            
            $this->storage->delete ();

            $response->setContentType (\Atatiki\HTTP\Response::JSON);
            $response->setOutput (array ());
        }
    };
<?php declare(strict_types=1);
    namespace Linkz\API\Controllers;

    class UserController
        extends \Atatiki\API\Controllers\Base\SecuredController
        implements \Atatiki\API\Controllers\Base\GetHandler,
                    \Atatiki\API\Controllers\Base\PostHandler
    {
        const UPDATEABLE_FIELDS = array (
            '_id_State', '_id_StateVisiting', '_id_StateInvoice',
            // tutor-specific variables
            'Tu100_FirstName', 'Tu101_LastName', 'Tu120_VisitingAddress1', 'Tu121_VisitingAddress2',
            'Tu122_VisitingZipCode', 'Tu123_VisitingCity', 'Tu142_SkypeID', 'Tu145_Email',
            'Tu130_InvoiceAddress1', 'Tu131_InvoiceAddress2', 'Tu132_InvoiceZipCode', 'Tu133_InvoiceCity',
            'Tu140_PhoneHome', 'Tu141_PhoneMobile', 'Tu106_Sex',
            // parent-specific variables
            'Pe100_FirstName', 'Pe101_LastName', 'Pe110_Address1', 'Pe111_Address2', 'Pe112_ZipCode', 'Pe113_City',
            'Pe120_PhoneHome', 'Pe121_PhoneMobile', 'Pe122_SkypeID', 'Pe102_Sex'
        );
        
        function get(\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $response->setContentType (\Atatiki\HTTP\Response::JSON);

            switch ($this->storage->get ('type'))
            {
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT:
                    $this->getParentInformation ($response);
                    break;
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR:
                    $this->getTutorInformation ($response);
                    break;
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_STUDENT:
                    $this->getStudentInformation ($response);
                    break;

                default:
                    throw new \Linkz\API\Exceptions\NotAllowedException ('Only logged in users are allowed');

            }
        }

        function getParentInformation (\Atatiki\HTTP\Response $response)
        {
            $parentInfo = \Linkz\Model\Parents::get ($this->storage->get ('id'));
    
            if (count ($parentInfo) === 0)
            {
                throw new \Exception ('Cannot fetch user information');
            }
    
            $response->setOutput ($parentInfo);
        }

        function getTutorInformation (\Atatiki\HTTP\Response $response)
        {
            $tutorInfo = \Linkz\Model\Tutor::get ($this->storage->get ('id'), true);
            
            if (count ($tutorInfo) === 0)
            {
                throw new \Exception ('Cannot fetch user information');
            }
            
            $response->setOutput ($tutorInfo);
        }

        function getStudentInformation (\Atatiki\HTTP\Response $response)
        {
            $studentInfo = \Linkz\Model\Students::get ($this->storage->get ('id'));
            
            if (count ($studentInfo) === 0)
            {
                throw new \Exception ('Cannot fetch user information');
            }
            
            $response->setOutput ($studentInfo);
        }
    
        function post (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            // first filter the fields we're going to save
            $body = $request->getBodyData ();
            $payload = array ();
            $recordId = '-1';
            $layout = '';
            
            if ($this->storage->get ('type') == \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT)
            {
                $layout = 'Pe__Person';
                \Linkz\Model\Parents::get ($this->storage->get ('id'), $recordId);
                
                foreach ($body as $key => $value)
                {
                    if (in_array ($key, self::UPDATEABLE_FIELDS) === true)
                    {
                        if ($key [0] == '_' || strpos ($key, 'Pe') === 0)
                        {
                            $payload [$key] = $value;
                        }
                    }
                }
            }
            elseif ($this->storage->get ('type') == \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR)
            {
                $layout = 'Tu__Tutor_Profile';
                \Linkz\Model\Tutor::get ($this->storage->get ('id'), false, $recordId);
                
                foreach ($body as $key => $value)
                {
                    if (in_array ($key, self::UPDATEABLE_FIELDS) === true)
                    {
                        if ($key [0] == '_' || strpos ($key, 'Tu') === 0)
                        {
                            $payload [$key] = $value;
                        }
                    }
                }
            }
            else
            {
                throw new \Linkz\API\Exceptions\NotAllowedException ('You are not allowed here');
            }
            
            if ($recordId == '-1')
            {
                throw new \Linkz\API\Exceptions\NotAllowedException ('');
            }
            
            \Linkz\Logging\LoggerFactory::getUserLogger ($this->storage->get ('type'), $this->storage->get ('id'))
                ->debug ('User updated contact information', $payload);
            
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $result = $fm->updateRecord ($recordId, $layout, $payload);
            
            if ($result->isError () === true)
            {
                $result->raiseError ();
            }
        }
    };
<?php declare (strict_types=1);
    namespace Linkz\API\Controllers;

    use \Linkz\API\Exceptions\NotAllowedException;

    class ContractDetailsController extends \Atatiki\API\Controllers\Base\SecuredController implements \Atatiki\API\Controllers\Base\GetHandler
    {
        /**
         * @inheritdoc
         * @throws \Exception
         */
        function get(\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            if ($this->storage->get ('type') !== \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT)
            {
                throw new NotAllowedException ("Only parent's allowed");
            }

            /** @var string $contractId */
            $contractId = $request->getParameter ('id');

            $response->setContentType (\Atatiki\HTTP\Response::JSON);
            $response->setOutput (
                array (
                    'invoices' => $this->getContractInvoices ($contractId),
                    'data' => \Linkz\Model\Contracts::getContract ($contractId)
                )
            );
        }
        
        private function getContractInvoices (string $id)
        {
            $invoices = \Linkz\Model\Contracts::getInvoices ($id);
            $students = $this->storage->get ('students');
            $resultData = array ();

            // check that all the results belong to our user first
            foreach ($invoices as $entry)
            {
                if ($entry ['_id_Student'] == '' || in_array ($entry ['_id_Student'], $students) === true)
                {
                    $resultData [] = $entry;
                }
            }

            return $resultData;
        }
    };

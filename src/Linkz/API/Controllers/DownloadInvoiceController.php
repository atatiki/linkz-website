<?php
    namespace Linkz\API\Controllers;
    
    class DownloadInvoiceController extends \Atatiki\API\Controllers\Base\SecuredController implements \Atatiki\API\Controllers\Base\GetHandler
    {
        function get (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $fm = \Atatiki\Filemaker\Connection::makeConnection ();
            $result = $fm->layout ('Qbp__QuickBookPreferences');
            
            if ($result->isError () === true)
            {
                $result->raiseError ();
            }
            
            if ($result->isEmpty () === true)
            {
                throw new \Exception ('Cannot connect to the server');
            }
            
            $result = $result->getRecord (0);
            $invoice = \Linkz\Model\Invoice::get ($request->getParameter ('id') ?? '');
            
            if (array_key_exists ('_QB_id', $invoice) === false)
            {
                throw new \Linkz\API\Exceptions\NotAllowedException ('Cannot access this invoice yet');
            }
            
            $dataService = \QuickBooksOnline\API\DataService\DataService::Configure (
                array (
                    'auth_mode' => 'oauth2',
                    'ClientID' => $result ['Qbp008_clientID'],
                    'ClientSecret' => $result ['Qbp009_clientSecret'],
                    'QBORealmID' => $result ['Qbp018_realmID'],
                    'scope' => 'com.intuit.quickbooks.accounting com.intuit.quickbooks.payment openid profile email phone address',
                    'baseUrl' => 'development',
                    'accessTokenKey' => $result ['Qbp002_accessToken'],
                    'refreshTokenKey' => $result ['Qbp020_refreshToken']
                )
            );
            
            $qbInvoice = $dataService->FindById ('invoice', $invoice ['_QB_id']);
            $result = $dataService->DownloadPDF ($qbInvoice);
            
            $response->setContentType ('application/pdf');
            
            if (is_string ($result))
            {
                $content = file_get_contents ($result);
                $response->setOutput ($content);
            }
            elseif (is_resource ($result))
            {
                fseek ($result, 0, SEEK_END);
                $size = ftell ($result);
                fseek ($result, 0, SEEK_SET);
                
                $content = fread ($result, $size);
                $response->setOutput ($content);
            }
        }
    };
<?php declare(strict_types=1);
    namespace Linkz\API\Controllers;

    use \Linkz\API\Exceptions\NotAllowedException;

    class FamilyController extends \Atatiki\API\Controllers\Base\SecuredController implements \Atatiki\API\Controllers\Base\GetHandler
    {
        function get(\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            if ($this->storage->get ('type') !== \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT)
            {
                throw new NotAllowedException ("Only parent's allowed");
            }

            $response->setContentType (\Atatiki\HTTP\Response::JSON);
            $response->setOutput (
                array (
                    'info' => \Linkz\Model\Family::get ($this->storage->get ('family_id')),
                    'students' => \Linkz\Model\Family::getStudents ($this->storage->get ('family_id'))
                )
            );
        }
    };
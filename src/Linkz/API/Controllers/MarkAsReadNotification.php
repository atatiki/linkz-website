<?php declare(strict_types=1);
    namespace Linkz\API\Controllers;
    
    class MarkAsReadNotification extends \Atatiki\API\Controllers\Base\SecuredController implements \Atatiki\API\Controllers\Base\PostHandler
    {
        function post (\Atatiki\HTTP\Request $request, \Atatiki\HTTP\Response $response): void
        {
            $notifications =
                \Linkz\Model\Notifications::makeFromStorage (
                    \Linkz\Storage\PermanentStorage::getUsersPermanentStorage ($this->storage)
                );
    
            \Linkz\Logging\LoggerFactory::getUserLogger ($this->storage->get ('type'), $this->storage->get ('id'))
                ->info ('User marked notification as read', array ('uuid' => $request->getParameter ('uuid') ?? ''));
            
            $notifications->markNotificationAsRead ($request->getParameter ('uuid') ?? '');
            $notifications->persist ();
        }
    };
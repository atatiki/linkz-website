<?php declare(strict_types=1);
    namespace Linkz\Logging;
    
    class LoggerFactory extends \Atatiki\Logging\LoggerFactory
    {
        /** @var \Monolog\Logger[] List of loggers registered */
        private static $userLogger = array ();
        
        static function getUserLogger (string $type, string $id): \Monolog\Logger
        {
            switch ($type)
            {
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_TUTOR:
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_PARENT:
                case \Atatiki\API\Controllers\Base\SecuredController::TYPE_STUDENT:
                    if (array_key_exists ($type, self::$userLogger) === false)
                    {
                        self::$userLogger [$type] = new \Monolog\Logger ($type);
                        self::$userLogger [$type]->pushHandler (self::getStreamHandler ('actions.log'));
                        self::$userLogger [$type]->pushProcessor (function ($record) use ($type, $id)
                        {
                            $record ['context'] ['id'] = $id;
                            
                            return $record;
                        });
                    }
                    
                    return self::$userLogger [$type];
                default:
                    throw new \Exception ('Unknown user type ' . $type);
            }
        }
    };
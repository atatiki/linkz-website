<?php declare(strict_types=1);
    namespace Linkz\API;

    final class EventsControllerTest extends \PHPUnit\Framework\TestCase
    {
        public function testTime (): void
        {
            $this->assertEquals (0, \Linkz\API\Controllers\EventsController::convertTimeToMinutes ("00:00"));
            $this->assertEquals (12 * 60, \Linkz\API\Controllers\EventsController::convertTimeToMinutes ("12:00"));
            
            $this->assertEquals ("12:00 AM", \Linkz\API\Controllers\EventsController::convertMinutesToTime (0));
            $this->assertEquals ("12:00 PM", \Linkz\API\Controllers\EventsController::convertMinutesToTime (12 * 60));
        }
    }
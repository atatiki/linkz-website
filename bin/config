#!/usr/bin/env php
<?php declare (strict_types=1);
    /**
     * Script that generates a configuration file for the whole system based on .env files
     *
     * @author Alexis Maiquez Murcia <alexis.murcia@atatiki.es>
     */

    $requiredConfigurations = array (
        "js" => array (
            "API_SERVER" => "api.server",
            "API_PORT" => "api.port",
            "API_PROTOCOL" => "api.protocol",
            "API_ENV" => "api.env",
            "APP_ENVIRONMENT" => "environment"
        ),
        "php" => array (
            "APP_ENVIRONMENT" => "",
            "PROJECT_PATH" => "",
            "REDIS_CLUSTER" => "",
            "REDIS_DATABASE" => "",
            "FM_USERNAME" => "",
            "FM_PASSWORD" => "",
            "FM_PROTOCOL" => "",
            "FM_SERVER" => "",
            "RESTFM_SERVER" => "",
            "RESTFM_DATABASE" => "",
            "RESTFM_KEY" => "",
            "FILE_URL_PATH" => "",
            "FILE_URL_BASE" => "",
            "LOGS_PATH" => ""
        )
    );
    
    include "vendor/autoload.php";
    
    file_exists (".env.default")   or die ("Cannot find file .env.default on current directory, aborting...");
    file_exists (".env")           or die ("Cannot find file .env on current directory, aborting...");
    
    /** @var \Dotenv\Dotenv $defaultValues Loads the default values for the configuration */
    $defaultValues = new \Dotenv\Dotenv (realpath ("."), ".env.default");
    $defaultValues->load ();
    
    /** @var \Dotenv\Dotenv $overridenValues Loads the overriden values for the configuration */
    $overridenValues = new \Dotenv\Dotenv (realpath ("."), ".env");
    $overridenValues->overload ();
    
    // environment files are loaded now, time to check for the correct variables
    // to make our life easier we're loading them from an array
    foreach ($requiredConfigurations as $type => $varlist)
    {
        foreach ($varlist as $variableName => $extrainfo)
        {
            $missingOnDefault = false;
            $missingOnOverriden = false;
            
            try
            {
                $defaultValues->required ($variableName);
            }
            catch (\RuntimeException $ex)
            {
                $missingOnDefault = true;
            }
            
            try
            {
                $overridenValues->required ($variableName);
            }
            catch (\RuntimeException $ex)
            {
                $missingOnOverriden = true;
            }
            
            if ($missingOnDefault == true && $missingOnOverriden == true)
            {
                die ("The required variable '" . $variableName . "' cannot be found in any of the .env files loaded");
            }
        }
    }
    
    $json = new \JsonPath\JsonObject();
    
    // validation is done properly, now we can generate the needed configuration files for javascript
    foreach ($requiredConfigurations ['js'] as $variableName => $path)
    {
        $json->set ('$.' . $path, getenv ($variableName));
    }
    
    $javascriptConfiguration = $json->getJson (JSON_PRETTY_PRINT);
    
    if (file_exists ("public/app/config/") == false)
    {
        mkdir ("public/app/config/", 0700, true);
        
        if (file_exists ("public/app/config") == false)
        {
            die ("Cannot create output folder for javascript configuration (public/app/config)");
        }
    }
    
    file_put_contents ("public/app/config/configuration.json", $javascriptConfiguration);
#!/bin/bash

npm -v || (
    echo "Downloading eslint globally"
    sudo npm install -g eslint
    sudo npm install -g eslint-plugin-angular
)

cd ..
PROJECT_CODENAME=linkz
PROJECT_FOLDER=${PROJECT_CODENAME}-website/public

[[ -d "$PROJECT_FOLDER" ]] || (echo "Main repository is not downloaded" && exit 1)

mkdir -p "$PROJECT_FOLDER/assets/libs/fontawesome/fonts"
mkdir -p "$PROJECT_FOLDER/assets/libs/fontawesome/css"

# download and deploy files
[[ -d "$PROJECT_CODENAME-fontawesome" ]] || git clone https://github.com/FortAwesome/Font-Awesome.git "$PROJECT_CODENAME-fontawesome"
[[ -d "$PROJECT_CODENAME-fontawesome/web-fonts-with-css" ]] || (echo "Cannot find web-fonts-with-css folder" && exit 1)
cp ${PROJECT_CODENAME}-fontawesome/web-fonts-with-css/webfonts/* "$PROJECT_FOLDER/assets/libs/fontawesome/fonts"
cp ${PROJECT_CODENAME}-fontawesome/web-fonts-with-css/css/* "$PROJECT_FOLDER/assets/libs/fontawesome/css"

cd "$PROJECT_CODENAME-bootstrap"
[[ -d ".git" ]] || git clone git@bitbucket.org:atatiki/linkz-bootstrap.git
make
cd ..
cd ${PROJECT_FOLDER}/..

if [[ "$1" = "watch" ]]; then
	screen -c .screenrc
fi
